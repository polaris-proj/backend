﻿using Domain;
using Domain.Connectors;
using Domain.StrategyProcessor;
using ExchangeConnectors.Connectors;
using FluentAssertions;
using Infrastructure.FluentAssertions;
using Microsoft.Extensions.Logging;
using MoreLinq;

namespace ExchangeConnectors.Integrations.Tests;

[Parallelizable(ParallelScope.Fixtures)]
public class BinanceFuturesConnectorTests
{
    [OneTimeTearDown]
    public async Task DisposeConnector() => await _binanceFuturesConnector.DisposeAsync();

    [Test]
    public async Task GetPrices_ReturnSomeValues()
    {
        var priceByTicker = await _binanceFuturesConnector.GetPrices();
        priceByTicker.Should().NotBeEmpty();
        priceByTicker.Keys.ForEach(x =>
            x.ToString().Should().NotBeNullOrWhiteSpace().And.NotBeEmpty().And.Contain("/"));
        priceByTicker.Values.ForEach(x => x.Should().BePositive());
    }

    private static IEnumerable<object[]> FuturesOrderTestData()
    {
        yield return new object[]
        {
            FuturesOrder.Long, new PlacementOrder(10_000, 0.01m), new PlacementOrder(12_000, 0.01m),
            new PlacementOrder(8_000, 0.01m)
        };
        yield return new object[]
        {
            FuturesOrder.Short, new PlacementOrder(100_000, 0.01m), new PlacementOrder(8_000, 0.01m),
            new PlacementOrder(12_000, 0.01m)
        };
    }

    [TestCaseSource(nameof(FuturesOrderTestData))]
    public async Task CreateFuturesOrder_CreateOrderSuccessfully(FuturesOrder orderType, PlacementOrder placementOrder,
        PlacementOrder take, PlacementOrder stop)
    {
        var order = await _binanceFuturesConnector.CreateFuturesOrder(_btcUsdt, orderType, 1, placementOrder, take,
            stop);
        order.Should().NotBeNull();
        order.Id.Should().NotBeNull();
    }

    [TestCaseSource(nameof(FuturesOrderTestData))]
    public async Task GetOrderInfo_ShouldReturnValidDataForFuturesOrder(FuturesOrder orderType,
        PlacementOrder placementOrder, PlacementOrder _, PlacementOrder __)
    {
        var order = await _binanceFuturesConnector.CreateFuturesOrder(_btcUsdt, orderType, 1, placementOrder);

        var orderInfo = await _binanceFuturesConnector.GetOrderInfo(_btcUsdt, order);
        orderInfo.OrderType.Should().Be(orderType.ToOrderType());
        orderInfo.Price.Should().BeInRadius(placementOrder.Price);
        orderInfo.Amount.Should().BeInRadius(placementOrder.Amount);
        orderInfo.CreateTime.Should().BeCloseTo(DateTime.UtcNow, TimeSpan.FromHours(1));
    }

    [TestCase(FuturesOrder.Long)]
    [TestCase(FuturesOrder.Short)]
    public async Task CloseOrder_ShouldReturnValidDataForFuturesOrder(FuturesOrder orderType)
    {
        var price = (double) (await _binanceFuturesConnector.GetPrices()).Single(x => x.Key == _btcUsdt).Value;
        price *= orderType == FuturesOrder.Long ? 0.9 : 1.1;

        var order = await _binanceFuturesConnector.CreateFuturesOrder(_btcUsdt, orderType, 1,
            new PlacementOrder((int) price, 0.01m));

        var orderInfo = await _binanceFuturesConnector.GetOrderInfo(_btcUsdt, order);
        orderInfo.OrderStatus.Should().Be(OrderStatus.Open);

        await _binanceFuturesConnector.CancelOrder(_btcUsdt, order);

        var orderInfoAfterClose = await _binanceFuturesConnector.GetOrderInfo(_btcUsdt, order);
        orderInfoAfterClose.OrderStatus.Should().Be(OrderStatus.Close);
    }

    [Test]
    public async Task GetOrderInfo_UnknownId_ThrowException()
    {
        var getOrderInfo = async () =>
            await _binanceFuturesConnector.GetOrderInfo(_btcUsdt, new OrderId(Guid.NewGuid().ToString()));

        await getOrderInfo.Should().ThrowAsync<Exception>("Order Not Found");
    }

    [Test]
    public async Task GetCoinsAmount()
    {
        var coinsAmount = await _binanceFuturesConnector.GetCoinsAmount();
        coinsAmount.Should().NotBeEmpty();
        coinsAmount.ForEach(x =>
        {
            x.Asset.Name.Should().NotBeNullOrEmpty();
            x.Asset.Name.Length.Should().BeGreaterOrEqualTo(1);
            x.Available.Should().BeGreaterOrEqualTo(0);
            x.Locked.Should().BeGreaterOrEqualTo(0);
            x.Total.Should().BePositive();
        });
    }

    private static IEnumerable<object[]> GetCandlesTestData()
    {
        return new[]
        {
            new object[]
                {new DateTimeRange(DateTime.UtcNow.AddDays(-2), DateTime.UtcNow.AddDays(-1)), TimeFrame.h1, 24},
            new object[] {new DateTimeRange(DateTime.UtcNow.AddDays(-2), DateTime.UtcNow.AddDays(-1)), TimeFrame.h4, 6},
            new object[]
                {new DateTimeRange(DateTime.UtcNow.AddDays(-2), DateTime.UtcNow.AddDays(-1)), TimeFrame.h12, 2},
            new object[] {new DateTimeRange(DateTime.UtcNow.AddDays(-2), DateTime.UtcNow.AddDays(-1)), TimeFrame.D1, 1},
            new object[]
                {new DateTimeRange(DateTime.UtcNow.AddDays(-2), DateTime.UtcNow.AddDays(-1)), TimeFrame.m30, 48},
            new object[]
                {new DateTimeRange(DateTime.UtcNow.AddDays(-2), DateTime.UtcNow.AddDays(-1)), TimeFrame.m15, 96},
        };
    }

    [TestCaseSource(nameof(GetCandlesTestData))]
    public async Task GetCandles_ByPreviousDay_ShouldReturn24H1Candle(DateTimeRange range, TimeFrame timeFrame,
        int candleAmount)
    {
        var candles = await _binanceFuturesConnector.GetCandles(_btcUsdt, timeFrame, range);

        candles.Count.Should().Be(candleAmount);
    }

    [Test]
    public async Task SubscribeOnNewKlines_OnePairOneTimeframe_WhenDataReceivedInvokeAction()
    {
        var isReceived = false;
        await _binanceFuturesConnector.SubscribeOnNewKlines(new[] {_btcUsdt}, new[] {TimeFrame.m1},
            kline =>
            {
                kline.Pair.Should().Be(_btcUsdt);
                kline.TimeFrame.Should().Be(TimeFrame.m1);
                isReceived = true;
            });
        await Task.Delay(TimeSpan.FromSeconds(60 - DateTime.Now.Second + 5));
        isReceived.Should().BeTrue();
    }

    [Test]
    public async Task SubscribeOnNewKlines_ThreePairOneTimeframe_WhenDataReceivedInvokeAction()
    {
        var pairs = new[] {_btcUsdt, new Pair("LTC", UsdtTicker), new Pair("ETH", UsdtTicker)};
        var receivedAmount = 0;
        await _binanceFuturesConnector.SubscribeOnNewKlines(pairs, new[] {TimeFrame.m1},
            kline =>
            {
                pairs.Should().Contain(kline.Pair);
                kline.TimeFrame.Should().Be(TimeFrame.m1);
                Interlocked.Increment(ref receivedAmount);
            });
        await Task.Delay(TimeSpan.FromSeconds(60 - DateTime.Now.Second + 5));
        receivedAmount.Should().Be(pairs.Length);
    }

    [Test]
    [NonParallelizable]
    public async Task GetOrdersPerPair_ShouldReturnTrades()
    {
        var trades = await _binanceFuturesConnector.GetOrdersPerPair(_btcUsdt);
        var amount = trades.Count;

        await _binanceFuturesConnector.CreateFuturesOrder(_btcUsdt, FuturesOrder.Long, 1,
            new PlacementOrder(20_000, 0.01m));

        var newTrades = await _binanceFuturesConnector.GetOrdersPerPair(_btcUsdt);
        newTrades.Count.Should().BeGreaterOrEqualTo(amount + 1);
        var lastOrder = newTrades.Last();
        lastOrder.OrderType.Should().Be(OrderType.Long);
        lastOrder.Amount.Should().Be(0.01m);
    }

    [Test]
    [Explicit("тулза на случай, если начали валиться тесты с Reach max open order limit")]
    [Description("Cancel all opened orders")]
    public async Task PrepareAccount()
    {
        var coinsAmount = await _binanceFuturesConnector.GetPrices();
        foreach (var coins in coinsAmount)
        {
            var tt = await _binanceFuturesConnector.GetOrdersPerPair(coins.Key);
            foreach (var order in tt.Where(x => x.OrderStatus != OrderStatus.Close))
            {
                await _binanceFuturesConnector.CancelOrder(coins.Key, order.OrderId);
            }
        }
    }

    private readonly Pair _btcUsdt = new(BtcTicker, UsdtTicker);
    private static readonly Ticker UsdtTicker = new("USDT");
    private static readonly Ticker BtcTicker = new("BTC");
    private const string ApiKey = "b93b2970037a9936e4a4844f4d467c6504e68e016bfd205e873fd410ff0b9c91";
    private const string SecretKey = "651b3ee739bf4a73584813aaffd5650357dc94c97056d6417049836f80735f4f";
    private readonly BinanceFuturesConnector _binanceFuturesConnector = new(ApiKey, SecretKey, new LoggerFactory(), true);
}