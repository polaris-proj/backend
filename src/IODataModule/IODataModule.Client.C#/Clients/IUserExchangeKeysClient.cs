﻿using IODataModule.Shared.Dto;

namespace IODataModule.Client.Clients;

public interface IUserExchangeKeysClient
{
    Task<ExchangeKeyDto> GetExchangeKey(Guid userId, Exchange exchange);
    Task<ExchangeKeyDto[]> GetAllExchangeKeys(Guid userId);
    Task AddOrUpdateExchangeKey(Guid userId, Exchange exchange, string secret, string key);
    Task RemoveExchangeKey(Guid userId, Exchange exchange);
}