﻿using Domain.StrategyProcessor;

namespace IODataModule.Client.Clients;

public interface ICandleService
{
    Task<IEnumerable<Candle>> GetRange(Guid userId, ExchangePairTimeFrame key, long start, long end);
    IAsyncEnumerable<Candle> GetRangeStream(Guid userId, ExchangePairTimeFrame key, long start, long end);
    Task<IEnumerable<Pair>> GetPairs(Guid userId, Exchange exchange);
}