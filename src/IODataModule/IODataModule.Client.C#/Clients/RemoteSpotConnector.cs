﻿using Domain.Connectors;
using Domain.StrategyProcessor;
using IODataModule.Shared.IRemoteExchangeConnector;
using IODataModule.Shared.IRemoteExchangeConnector.Dto;

namespace IODataModule.Client.Clients;

public class RemoteSpotConnector : BaseRemoteConnector, ISpotConnector
{
    public RemoteSpotConnector(Guid userId, Exchange exchange, IoDataModuleClientChannel channel) : base(userId,
        exchange, channel)
    {
    }

    public async Task<OrderId> CreateMarketOrder(Pair pair, MarketOrder orderType, decimal amount)
    {
        var dto = new MarketOrderInfo
        {
            Exchange = Exchange,
            UserId = UserId,
            Pair = pair,
            OrderType = orderType,
            Amount = amount
        };
        
        var service = Channel.CreateGrpcService<ISpotRemoteExchangeConnectorGrpc>();
        return await service.CreateMarketOrder(dto);
    }

    public async Task<OrderId> CreateLimitOrder(Pair pair, LimitOrder orderType, PlacementOrder entry)
    {
        var dto = new LimitOrderInfo
        {
            Exchange = Exchange,
            UserId = UserId,
            Pair = pair,
            OrderType = orderType,
            Entry = entry
        };

        var service = Channel.CreateGrpcService<ISpotRemoteExchangeConnectorGrpc>();
        return await service.CreateLimitOrder(dto);
    }
}