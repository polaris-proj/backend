﻿using Infrastructure;
using Infrastructure.Grpc;

namespace IODataModule.Client;

public class IoDataModuleGrpcServiceParameters(IVault vault) : IParameters, IGrpcServiceParameters
{
    public string Address { get; } = vault.GetValue("IODataModule:gRPC:Url");
}