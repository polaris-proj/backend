﻿using IODataModule.Shared.Dto;

namespace IODataModule.Shared;

[DataContract]
public class AddOrUpdateExchangeKey
{
    [DataMember(Order = 1)] public required Guid UserId { get; set; }
    [DataMember(Order = 2)] public required ExchangeKeyDto ExchangeKey { get; set; }
}