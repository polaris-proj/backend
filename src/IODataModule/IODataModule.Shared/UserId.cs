﻿namespace IODataModule.Shared;
[DataContract]
public class UserId
{
   [DataMember(Order = 1)] public required Guid Id { get; set; }
}