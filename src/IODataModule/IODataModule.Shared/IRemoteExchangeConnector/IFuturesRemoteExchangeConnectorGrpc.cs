﻿using System.ServiceModel;
using Domain.Connectors;
using IODataModule.Shared.IRemoteExchangeConnector.Dto;
using ProtoBuf.Grpc;

namespace IODataModule.Shared.IRemoteExchangeConnector;

[ServiceContract]
public interface IFuturesRemoteExchangeConnectorGrpc
{
    [OperationContract]
    Task<OrderId> CreateFuturesOrder(FuturesOrderInfo orderInfo, CallContext context = default);
}