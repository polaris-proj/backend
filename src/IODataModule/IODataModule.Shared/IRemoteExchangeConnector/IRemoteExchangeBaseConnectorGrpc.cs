﻿using System.ServiceModel;
using Domain.Connectors;
using Domain.StrategyProcessor;
using IODataModule.Shared.Dto;
using ProtoBuf.Grpc;

namespace IODataModule.Shared.IRemoteExchangeConnector;

[ServiceContract]
public interface IRemoteExchangeBaseConnectorGrpc
{
    [OperationContract]
    Task<IReadOnlyList<Balance>> GetCoinsAmount(ExchangeInfo exchangeInfo, CallContext context = default);

    [OperationContract]
    Task<Dictionary<Pair, decimal>> GetPrices(ExchangeInfo exchangeInfo, CallContext ctx = default);

    [OperationContract]
    Task<IReadOnlyList<Candle>> GetCandles(CandleInfo candleInfo, CallContext context = default);

    [OperationContract]
    Task<IReadOnlyList<Order>> GetCurrentOrdersPerPair(PairInfo pairInfo, CallContext context = default);

    [OperationContract]
    Task<Order> GetOrderInfo(OrderInfo orderInfo, CallContext context = default);

    [OperationContract]
    Task CancelOrder(OrderInfo orderInfo, CallContext context = default);
}