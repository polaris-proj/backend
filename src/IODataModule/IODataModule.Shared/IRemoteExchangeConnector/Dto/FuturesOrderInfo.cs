﻿using Domain.Connectors;
using Domain.StrategyProcessor;

namespace IODataModule.Shared.IRemoteExchangeConnector.Dto;

[DataContract]
public class FuturesOrderInfo
{
    [DataMember(Order = 1)] public required Exchange Exchange { get; set; }
    [DataMember(Order = 2)] public required Guid UserId { get; set; }
    [DataMember(Order = 3)] public required Pair Pair { get; set; }
    [DataMember(Order = 4)] public required FuturesOrder OrderType { get; set; }
    [DataMember(Order = 5)] public required int Leverage { get; set; }
    [DataMember(Order = 6)] public required PlacementOrder Entry { get; set; }
    [DataMember(Order = 7)] public PlacementOrder? Take { get; set; }
    [DataMember(Order = 8)] public PlacementOrder? Stop { get; set; }
}