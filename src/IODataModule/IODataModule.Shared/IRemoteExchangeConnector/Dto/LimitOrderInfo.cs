﻿using Domain.Connectors;
using Domain.StrategyProcessor;

namespace IODataModule.Shared.IRemoteExchangeConnector.Dto;

[DataContract]
public class LimitOrderInfo
{
    [DataMember(Order = 1)] public required Exchange Exchange { get; set; }
    [DataMember(Order = 2)] public required Guid UserId { get; set; }
    [DataMember(Order = 3)] public required Pair Pair { get; set; }
    [DataMember(Order = 4)] public required LimitOrder OrderType { get; set; }
    [DataMember(Order = 5)] public required PlacementOrder Entry { get; set; }
}