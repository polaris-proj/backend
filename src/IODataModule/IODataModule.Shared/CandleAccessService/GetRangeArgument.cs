﻿using Domain.StrategyProcessor;

namespace IODataModule.Shared.CandleAccessService;

[DataContract]
public class GetRangeArgument
{
    public GetRangeArgument(Guid userId, ExchangePairTimeFrame key, long start, long end)
    {
        (Exchange, Pair, TimeFrame) = (key.Exchange, key.Pair, key.TimeFrame);
        Start = start;
        End = end;
        UserId = userId;
    }

    [DataMember(Order = 1)] public Exchange Exchange { get; set; }
    [DataMember(Order = 2)] public Pair Pair { get; set; }
    [DataMember(Order = 3)] public TimeFrame TimeFrame { get; set; }
    [DataMember(Order = 4)] public long Start { get; set; }
    [DataMember(Order = 5)] public long End { get; set; }
    [DataMember(Order = 6)] public Guid UserId { get; set; }
    
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    private GetRangeArgument(){}//for protobuf don't delete
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
}