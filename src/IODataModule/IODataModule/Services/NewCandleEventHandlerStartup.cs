﻿using ExchangeConnectors.Connectors;
using Infrastructure;
using Infrastructure.RTQConnector;
using IODataModule.Services.ExchangeConnectors;
using ReadOnlyCollectionsExtensions;

namespace IODataModule.Services;

internal class NewCandleEventHandlerStartup(
    IConnectorFactory connectorFactory,
    ILoggerFactory loggerFactory,
    KafkaParameters kafkaParameters
)
    : BackgroundService
{
    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        var userId = Guid.Parse("9b257c73-241c-438e-9d2d-46834afcb244"); // todo fix it

        var listOfExchanges = new List<(IInternalConnector, Exchange)>
        {
            (await connectorFactory.GetSpotConnectorAsync(userId, Exchange.BinanceSpot), Exchange.BinanceSpot),
            //  (new BinanceFuturesConnector(key, secret),Exchange.BinanceFutures)
        };

        foreach (var (connector, exchange) in listOfExchanges)
        {
            var pairs = (await connector.GetPrices()).Keys.ToReadOnlyList();
            var timeFrames = EnumEnumerator.GetEnums<TimeFrame>().Where(x => x > 0).ToReadOnlyList();

            _logger.LogTrace($"Starting candleHandler for {exchange.ToString()}");
            var newCandleEventHandler = new NewCandleEventHandler(connector, exchange, pairs, timeFrames, kafkaParameters, loggerFactory);
            _newCandleEventHandlers.Add(newCandleEventHandler);
            await newCandleEventHandler.StartAsync();

            stoppingToken.ThrowIfCancellationRequested();
        }
    }

    public override Task StopAsync(CancellationToken cancellationToken)
    {
        base.StopAsync(cancellationToken);
        foreach (var handler in _newCandleEventHandlers)
        {
            handler.Stop();
            cancellationToken.ThrowIfCancellationRequested();
        }

        return Task.CompletedTask;
    }
    
    private readonly List<NewCandleEventHandler> _newCandleEventHandlers = new();
    private readonly ILogger<NewCandleEventHandlerStartup> _logger = loggerFactory.CreateLogger<NewCandleEventHandlerStartup>();
}