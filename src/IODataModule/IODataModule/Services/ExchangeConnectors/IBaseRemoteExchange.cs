﻿using Domain.Connectors;
using Domain.StrategyProcessor;

namespace IODataModule.Services.ExchangeConnectors;

public interface IBaseRemoteExchange
{
    Task<IReadOnlyList<Balance>> GetCoinsAmount(Exchange exchange, Guid userId);
    Task<Dictionary<Pair, decimal>> GetPrices(Exchange exchange, Guid userId);
    Task<IReadOnlyList<Candle>> GetCandles(Exchange exchange, Guid userId, Pair pair, TimeFrame timeFrame,DateTimeRange range, CancellationToken ct = default);
    Task<IReadOnlyList<Order>> GetCurrentOrdersPerPair(Exchange exchange, Guid userId, Pair pair);
    Task<Order> GetOrderInfo(Exchange exchange, Guid userId, Pair pair, OrderId orderId);
    Task CancelOrder(Exchange exchange, Guid userId, Pair pair, OrderId orderId);
}