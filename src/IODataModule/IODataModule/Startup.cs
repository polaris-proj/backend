﻿using Infrastructure;
using Infrastructure.DAL.Dapper;
using Infrastructure.Metric;
using Infrastructure.Metric.Grpc;
using Infrastructure.RTQConnector;
using IODataModule.Controllers;
using IODataModule.Controllers.ExchangeConnectors;
using IODataModule.Infrastructure.DAL;
using IODataModule.Infrastructure.DAL.Repositories;
using IODataModule.Services;
using IODataModule.Services.ExchangeConnectors;
using IODataModule.Shared;
using IODataModule.Shared.CandleAccessService;
using IODataModule.Shared.IRemoteExchangeConnector;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Prometheus;
using ProtoBuf.Grpc.Server;

namespace IODataModule;

public static class Startup
{
    public static void ConfigureServices(this IServiceCollection services)
    {
        services.AddHealthChecks();
        services.AddLogging(x => { x.AddConsole(); });

        services.AddSingleton<IVault, Vault>();

        ConfigureDatabase(services);

        services.Scan(i =>
            {
                i.FromCallingAssembly()
                    .AddClasses(cl => cl.AssignableTo<StartUpService>()).AsSelf().As<StartUpService>()
                    .WithScopedLifetime();
                i.FromAssemblyOf<InfrastructureAssembly>()
                    .AddClasses(c => c.AssignableTo<IParameters>()).AsSelf().WithSingletonLifetime();
            })
            .AddSingleton<IMemoryCache, MemoryCache>()
            .AddSingleton(typeof(IProducer<>), typeof(Producer<>))
            .AddScoped<ICandleAccessService, CandleAccessService>()
            .AddSingleton<ICandleRepository, CandleRepository>()
            .AddSingleton<IUserExchangeKeysRepository, UserExchangeKeysRepository>()
            .AddSingleton<IConnectorFactory, ConnectorFactory>()
            .AddScoped<SpotRemoteExchange>()
            .AddScoped<FuturesRemoteExchange>();

        services.AddCodeFirstGrpc(options =>
        {
            options.EnableDetailedErrors = true;
            options.MaxReceiveMessageSize = 200 * 1024 * 1024; // 200 MB
            options.MaxSendMessageSize = 500 * 1024 * 1024; // 500 MB
            options.Interceptors.Add<GrpcErrorInterceptor>();
        });

        AddGrpcControllers(services);

        services.AddHostedService<NewCandleEventHandlerStartup>();

        var port = services.BuildServiceProvider(validateScopes: true).GetRequiredService<MetricSettings>().Port;
        services.AddMetricServer(x => x.Port = port);


        services.AddScoped<PatternEventLoader>();

    }

    private static void AddGrpcControllers(this IServiceCollection services)
    {
        services.AddScoped<IRemoteExchangeBaseConnectorGrpc, BaseRemoteExchangeController>()
            .AddScoped<ISpotRemoteExchangeConnectorGrpc, SpotRemoteExchangeController>()
            .AddScoped<IFuturesRemoteExchangeConnectorGrpc, FuturesRemoteExchangeController>()
            .AddScoped<IUserAccessTokenServiceGrpc, UserAccessTokenServiceGrpc>()
            .AddScoped<ICandleAccessServiceGrpc, CandleAccessServiceGrpc>();
    }

    public static void ConfigureApp(this IApplicationBuilder app)
    {
        app.UseRouting();

        app.UseGrpcMetrics();
        app.UseHttpMetrics();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapGrpcService<IUserAccessTokenServiceGrpc>();
            endpoints.MapGrpcService<ICandleAccessServiceGrpc>();

            endpoints.MapGrpcService<IRemoteExchangeBaseConnectorGrpc>();
            endpoints.MapGrpcService<ISpotRemoteExchangeConnectorGrpc>();
            endpoints.MapGrpcService<IFuturesRemoteExchangeConnectorGrpc>();
            //endpoints.MapGrpcService<BaseRemoteExchangeServiceGrpc>();
                
            endpoints.MapHealthChecks("health");
        });
    }

    private static void ConfigureDatabase(IServiceCollection services)
    {
        services.ConfigureNpgSql().AddPooledDbContextFactory<IoDataModuleContext>(ConfigureNpgsql);
        return;

        void ConfigureNpgsql(IServiceProvider sp, DbContextOptionsBuilder contextOptionsBuilder)
        {
            var connectionString = sp.GetRequiredService<PostgresParameters>().FullConnectionString;
            contextOptionsBuilder.UseNpgsql(connectionString);
            // .EnableSensitiveDataLogging()
        }
    }
}