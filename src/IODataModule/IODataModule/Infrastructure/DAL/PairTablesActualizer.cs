﻿/*using Domain.Connectors;
using Infrastructure;
using IODataModule.Infrastructure.DAL.Repositories;
using IODataModule.Services.ExchangeConnectors;

namespace IODataModule.Infrastructure.DAL;

internal class PairTablesActualizer(ICandleRepository candleRepository, ConnectorFactory connectorFactory) : StartUpService
{
    public override async Task Start()
    {
        var userId = Guid.Parse("9b257c73-241c-438e-9d2d-46834afcb244");

        var listOfExchanges = new List<(IConnector, Exchange)>
        {
            (await connectorFactory.GetSpotConnector(userId, Exchange.BinanceSpot), Exchange.BinanceSpot),
            //  (new BinanceFuturesConnector(key, secret),Exchange.BinanceFutures)
        };
        foreach (var (connector, exchange) in listOfExchanges)
        {
            var pairs = (await connector.GetPrices()).Keys.ToList();
            foreach (var pair in pairs)
            {
                await candleRepository.CreateTableIfNotExist(exchange, pair);
            }
        }
    }

    public override int Priority => 10000;
}*/