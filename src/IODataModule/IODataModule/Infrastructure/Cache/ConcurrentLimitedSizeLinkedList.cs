﻿/*namespace IODataModule.Infrastructure.Cache;

public class ConcurrentLimitedSizeLinkedList<T>
{
    private readonly LimitedSizeLinkedList<T> _list;
    private readonly object _locker = new();

    public ConcurrentLimitedSizeLinkedList(int limit)
    {
        _list = new LimitedSizeLinkedList<T>(limit);
    }

    public void AddLast(T item)
    {
        lock (_locker)
        {
            _list.AddLast(item);
        }
    }

    public T GetLast()
    {
        return _list.GetLast();
    }

    public T GetFirst()
    {
        return _list.GetFirst();
    }

    public IEnumerable<T> GetAllValues()
    {
        return _list.GetAllValues();
    }

    public int Count => _list.Count;
}*/