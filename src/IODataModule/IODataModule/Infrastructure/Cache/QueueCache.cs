﻿/*using IODataModule.Infrastructure.DAL.Models;

namespace IODataModule.Infrastructure.Cache;

public class QueueCache<T> where T : ExchangeCommonDbo
{
    private readonly ConcurrentLimitedSizeLinkedList<T> _objects;

    public QueueCache(int size)
    {
        _objects = new ConcurrentLimitedSizeLinkedList<T>(size);
    }

    public IEnumerable<T> GetElementsRange(long start, long end)
    {
        if (_objects.GetFirst().TimeStamp <= start && _objects.GetLast().TimeStamp >= end)
            return _objects.GetAllValues()
                .Where(x => x.TimeStamp >= start)
                .Where(x => x.TimeStamp <= end);

        return new List<T>();
    }

    public T? GetElement(long timeStamp)
    {
        if (_objects.GetFirst().TimeStamp <= timeStamp && _objects.GetLast().TimeStamp >= timeStamp)
            return _objects.GetAllValues()
                .First(x => x.TimeStamp == timeStamp);

        return null;
    }

    public void Add(T candle)
    {
        //todo тут нужна потокобезопасность?
        _objects.AddLast(candle);
    }
}*/