﻿using System;
using System.Threading.Tasks;
using Binance.Net;
using Binance.Net.Clients;
using Binance.Net.Enums;
using CryptoExchange.Net.Authentication;
using Domain.Connectors;
using Domain.StrategyProcessor;
using MethodTimer;
using Microsoft.Extensions.Logging;

namespace ExchangeConnectors.Connectors;

public class BinanceFuturesConnector : BinanceBaseConnector, IInternalFuturesConnector
{
    public BinanceFuturesConnector(string key, string secret, ILoggerFactory loggerFactory, bool isTestApi = false)
        : base(key, secret, loggerFactory.CreateLogger<BinanceFuturesConnector>(), isTestApi)
    {
        var credentials = new ApiCredentials(key, secret);

        _socketClient = new BinanceSocketClient(x =>
        {
            x.ApiCredentials = credentials;
            x.AutoReconnect = true;
            x.Environment = BinanceEnvironment.Testnet;
        });
    }

    #region Candles and klines

    public async Task<IReadOnlyList<Balance>> GetCoinsAmount()
    {
        var accountIndo = await RestClient.UsdFuturesApi.Account.GetAccountInfoAsync();

        return accountIndo.Data.Assets.Where(x => x.AvailableBalance > 0).Select(x =>
            new Balance(x.Asset, available: x.AvailableBalance, locked: 0)).ToList(); //todo проверить правильность
    }


    public async Task<IReadOnlyList<Candle>> GetCandles(Pair pair, TimeFrame timeFrame, DateTimeRange range)
    {
        return await base.GetCandles(pair, timeFrame, range,
            (pairStr, interval, startTime, endTime, limit) =>
                RestClient.UsdFuturesApi.ExchangeData.GetKlinesAsync(pairStr, interval, startTime, endTime, limit));
    }

    [Time]
    public async Task SubscribeOnNewKlines(IReadOnlyList<Pair> ticker, IReadOnlyList<TimeFrame> tf, Action<Kline> eventHandler)
    {
        await base.SubscribeOnNewKlines(ticker, tf, eventHandler,
            (pairs, timeFrames, candleAction, ct) =>
                _socketClient.UsdFuturesApi.SubscribeToKlineUpdatesAsync(pairs, timeFrames, candleAction, ct));
    }

    #endregion

    [Time]
    public async Task<IReadOnlyList<Order>> GetOrdersPerPair(Pair pair)
    {
        var userTradesResult = await RestClient.UsdFuturesApi.Trading.GetOrdersAsync(pair.ToStringWithOutSlash());
        var trades = new List<Order>();

        if (!userTradesResult.Success)
            throw new Exception($"Smth went wrong {userTradesResult.Error}");

        foreach (var trade in userTradesResult.Data)
        {
            var orderType =
                StatusConverter.GetFuturesOrderType(trade.Side.ToCommonOrderSide(), trade.Type.ToCommonOrderType());

            var status = trade.Status.GetOrderStatus();

            var order = new Order(trade.Price, trade.Quantity, trade.QuantityFilled, orderType, status)
            {
                OrderId = new OrderId(trade.Id.ToString())
            };
            trades.Add(order);
        }

        return trades;
    }

    [Time]
    public async Task<Dictionary<Pair, decimal>> GetPrices()
    {
        return await base.GetPrices(() => RestClient.UsdFuturesApi.ExchangeData.GetPricesAsync());
    }

    public async Task<OrderId> CreateFuturesOrder(Pair pair, FuturesOrder orderType, int leverage, PlacementOrder entry,
        PlacementOrder? take = null, PlacementOrder? stop = null)
    {
        var orderSide = orderType.GetOrderSide();
        var invertedOrderSide = orderSide == OrderSide.Buy ? OrderSide.Sell : OrderSide.Buy;

        var openPositionResult = await RestClient.UsdFuturesApi.Trading.PlaceOrderAsync(pair.ToStringWithOutSlash(),
            orderSide,
            FuturesOrderType.Limit, entry.Amount, entry.Price, timeInForce: TimeInForce.GoodTillCanceled);
        if (stop is not null)
        {
            var stopLossResult = await RestClient.UsdFuturesApi.Trading.PlaceOrderAsync(pair.ToStringWithOutSlash(),
                invertedOrderSide,
                FuturesOrderType.StopMarket, quantity: null, closePosition: true, stopPrice: stop.Price,
                timeInForce: TimeInForce.GoodTillCanceled);
        }

        if (take is not null)
        {
            var takeProfitResult = await RestClient.UsdFuturesApi.Trading.PlaceOrderAsync(pair.ToStringWithOutSlash(),
                invertedOrderSide,
                FuturesOrderType.TakeProfitMarket, quantity: null, closePosition: true, stopPrice: take.Price,
                timeInForce: TimeInForce.GoodTillCanceled);
        }

        if (!openPositionResult.Success)
            throw new Exception($"Can't create order: {openPositionResult.Error} {pair.ToStringWithOutSlash()}");

        return new OrderId(openPositionResult.Data.Id.ToString());
    }

    [Time]
    public async Task<Order> GetOrderInfo(Pair pair, OrderId id)
    {
        var orderData =
            await RestClient.UsdFuturesApi.CommonFuturesClient.GetOrderAsync(id.Id, pair.ToStringWithOutSlash());

        if (orderData.Data is null)
            throw new Exception($"Order Not Found {orderData.Error}");

        var orderType = StatusConverter.GetFuturesOrderType(orderData.Data.Side, orderData.Data.Type);
        var orderStatus = orderData.Data.Status.GetOrderStatus();

        return new Order(orderData.Data.Timestamp, null, orderData.Data.Price.Value, orderData.Data.Quantity.Value,
            orderData.Data.QuantityFilled.Value, orderType, orderStatus);
    }

    [Time]
    public async Task CancelOrder(Pair pair, OrderId orderId)
    {
        await RestClient.UsdFuturesApi.CommonFuturesClient.CancelOrderAsync(orderId.Id, pair.ToStringWithOutSlash());
    }

    public override async ValueTask DisposeAsync()
    {
        await base.DisposeAsync();
        _socketClient.Dispose();
    }

    private readonly BinanceSocketClient _socketClient;
    // private readonly ResponseQueue _responseQueue = new();
}