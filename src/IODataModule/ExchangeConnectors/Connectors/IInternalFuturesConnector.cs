﻿using Domain.Connectors;

namespace ExchangeConnectors.Connectors;

public interface IInternalFuturesConnector : IFuturesConnector, IInternalConnector;