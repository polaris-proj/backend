﻿using System;
using System.Threading.Tasks;
using Binance.Net;
using Binance.Net.Clients;
using Binance.Net.Objects.Models.Spot;
using CryptoExchange.Net.Authentication;
using CryptoExchange.Net.CommonObjects;
using CryptoExchange.Net.Objects;
using Domain.Connectors;
using Domain.StrategyProcessor;
using MethodTimer;
using Microsoft.Extensions.Logging;
using Balance = Domain.Connectors.Balance;
using Kline = Domain.StrategyProcessor.Kline;
using Order = Domain.StrategyProcessor.Order;
using OrderId = Domain.Connectors.OrderId;

namespace ExchangeConnectors.Connectors;

public class BinanceSpotConnector : BinanceBaseConnector, IInternalSpotConnector
{
    public BinanceSpotConnector(string key, string secret, ILoggerFactory loggerFactory, bool isTestApi = false)
        : base(key, secret, loggerFactory.CreateLogger<BinanceSpotConnector>(), isTestApi)
    {
        var credentials = new ApiCredentials(key, secret);

        _socketClient = new BinanceSocketClient(x =>
        {
            x.ApiCredentials = credentials;
            x.AutoReconnect = true;
            x.RequestTimeout = TimeSpan.FromSeconds(90);
            x.SocketSubscriptionsCombineTarget = 1;
            x.Environment = isTestApi ? BinanceEnvironment.Testnet : BinanceEnvironment.Live;
        });
    }

    #region Candles and klines

    public async Task<IReadOnlyList<Balance>> GetCoinsAmount()
    {
        var accountIndo = await RestClient.SpotApi.Account.GetAccountInfoAsync();

        return accountIndo.Data.Balances.Where(x => x.Total > 0)
            .Select(x => new Balance(asset: x.Asset, available: x.Available, locked: x.Locked))
            .ToList();
    }

    public async Task<IReadOnlyList<Candle>> GetCandles(Pair pair, TimeFrame timeFrame, DateTimeRange range)
    {
        return await base.GetCandles(pair, timeFrame, range,
            (pairStr, interval, startTime, endTime, limit) =>
                RestClient.SpotApi.ExchangeData.GetKlinesAsync(pairStr, interval, startTime, endTime, limit));
    }

    [Time]
    public async Task SubscribeOnNewKlines(IReadOnlyList<Pair> ticker, IReadOnlyList<TimeFrame> tf, Action<Kline> eventHandler)
    {
        await base.SubscribeOnNewKlines(
            ticker,
            tf,
            eventHandler,
            (pairs, timeFrames, candleAction, ct) =>
                _socketClient.SpotApi.ExchangeData.SubscribeToKlineUpdatesAsync(pairs, timeFrames, candleAction, ct));
    }

    #endregion

    [Time]
    public async Task<IReadOnlyList<Order>> GetOrdersPerPair(Pair pair)
    {
        var openOrders = RestClient.SpotApi.CommonSpotClient.GetOpenOrdersAsync(pair.ToStringWithOutSlash());
        var closedOrders = RestClient.SpotApi.CommonSpotClient.GetClosedOrdersAsync(pair.ToStringWithOutSlash());
        var trades = new List<Order>();

        foreach (var trade in (await Task.WhenAll(openOrders, closedOrders)).SelectMany(x => x.Data))
        {
            var orderType = StatusConverter.GetOrderType(trade.Side, trade.Type);

            var status = trade.Status.GetOrderStatus();

            var order = new Order(trade.Price.Value, trade.Quantity.Value, trade.QuantityFilled.Value, orderType,
                status)
            {
                CreateTime = trade.Timestamp,
                OrderId = new OrderId(trade.Id)
            };
            trades.Add(order);
        }

        return trades.OrderBy(x => x.CreateTime).ToList();
    }

    [Time]
    public async Task<Dictionary<Pair, decimal>> GetPrices()
    {
        return await base.GetPrices(() => RestClient.SpotApi.ExchangeData.GetPricesAsync());
    }

    public async Task<OrderId> CreateMarketOrder(Pair pair, MarketOrder orderType, decimal amount)
    {
        var orderSide = orderType == MarketOrder.Buy ? CommonOrderSide.Buy : CommonOrderSide.Sell;

        var orderData = await RestClient.SpotApi.CommonSpotClient.PlaceOrderAsync(pair.ToStringWithOutSlash(),
            orderSide, CommonOrderType.Market, amount);

        if (!orderData.Success)
            throw new Exception($"Can't create order: {orderData.Error} {pair.ToStringWithOutSlash()}");
        return new OrderId(orderData.Data.Id);
    }

    public async Task<OrderId> CreateLimitOrder(Pair pair, LimitOrder orderType, PlacementOrder entry)
    {
        var orderSide = orderType == LimitOrder.BuyLimit ? CommonOrderSide.Buy : CommonOrderSide.Sell;

        var orderData = await RestClient.SpotApi.CommonSpotClient.PlaceOrderAsync(
            pair.ToStringWithOutSlash(), orderSide, CommonOrderType.Limit, entry.Amount, price: entry.Price);

        if (!orderData.Success)
            throw new Exception($"Can't create order: {orderData.Error} {pair.ToStringWithOutSlash()}");

        return new OrderId(orderData.Data.Id);
    }

    [Time]
    public async Task<Order> GetOrderInfo(Pair pair, OrderId id)
    {
        var orderData = await RestClient.SpotApi.CommonSpotClient.GetOrderAsync(id.Id ?? throw new InvalidOperationException(), pair.ToStringWithOutSlash());

        if (orderData.Data is null)
            throw new Exception("Order Not Found");

        var orderType = StatusConverter.GetOrderType(orderData.Data.Side, orderData.Data.Type);
        var orderStatus = orderData.Data.Status.GetOrderStatus();
        var binanceObj = (WebCallResult<BinanceOrder>) orderData.Data.SourceObject;
        return new Order(binanceObj.Data.CreateTime, binanceObj.Data.UpdateTime, orderData.Data.Price.Value,
            orderData.Data.Quantity.Value, orderData.Data.QuantityFilled.Value, orderType, orderStatus);
    }

    [Time]
    public async Task CancelOrder(Pair pair, OrderId orderId)
    {
        await RestClient.SpotApi.CommonSpotClient.CancelOrderAsync(orderId.Id ?? throw new InvalidOperationException(), pair.ToStringWithOutSlash());
    }

    public override async ValueTask DisposeAsync()
    {
        await base.DisposeAsync();
        _socketClient.Dispose();
    }


    private readonly BinanceSocketClient _socketClient;
}