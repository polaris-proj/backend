﻿using Domain.Connectors;

namespace ExchangeConnectors.Connectors;

public interface IInternalSpotConnector : ISpotConnector, IInternalConnector;