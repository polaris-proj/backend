﻿using System;
using System.Threading.Tasks;
using Domain.Connectors;
using Domain.StrategyProcessor;

namespace ExchangeConnectors.Connectors;

public interface IInternalConnector : IConnector
{
    Task SubscribeOnNewKlines(IReadOnlyList<Pair> pair, IReadOnlyList<TimeFrame> tf, Action<Kline> eventHandler);
    Task UnsubscribeOnNewKlines(Pair pair, TimeFrame tf, Action<Kline> deleg);//todo подписываем на список а отписываемся на 1, странно
}