from datetime import datetime

from sqlalchemy import Column, DateTime, String
from sqlalchemy.dialects.postgresql import UUID

from .base import Base


class InviteUserKey(Base):
    __tablename__ = "InviteUserKey"

    user_id = Column(UUID(as_uuid=True), primary_key=True)
    invite_key = Column(String, nullable=True)
    created_at = Column(DateTime, default=datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __str__(self):
        return f"{self.__class__.__name__}(id={self.user_id}, invite_key={self.invite_key})"
