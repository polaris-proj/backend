# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: protobuf-net/bcl.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\x16protobuf-net/bcl.proto\x12\x03\x62\x63l\"\xae\x01\n\x08TimeSpan\x12\r\n\x05value\x18\x01 \x01(\x12\x12*\n\x05scale\x18\x02 \x01(\x0e\x32\x1b.bcl.TimeSpan.TimeSpanScale\"g\n\rTimeSpanScale\x12\x08\n\x04\x44\x41YS\x10\x00\x12\t\n\x05HOURS\x10\x01\x12\x0b\n\x07MINUTES\x10\x02\x12\x0b\n\x07SECONDS\x10\x03\x12\x10\n\x0cMILLISECONDS\x10\x04\x12\t\n\x05TICKS\x10\x05\x12\n\n\x06MINMAX\x10\x0f\"\x8d\x02\n\x08\x44\x61teTime\x12\r\n\x05value\x18\x01 \x01(\x12\x12*\n\x05scale\x18\x02 \x01(\x0e\x32\x1b.bcl.DateTime.TimeSpanScale\x12(\n\x04kind\x18\x03 \x01(\x0e\x32\x1a.bcl.DateTime.DateTimeKind\"g\n\rTimeSpanScale\x12\x08\n\x04\x44\x41YS\x10\x00\x12\t\n\x05HOURS\x10\x01\x12\x0b\n\x07MINUTES\x10\x02\x12\x0b\n\x07SECONDS\x10\x03\x12\x10\n\x0cMILLISECONDS\x10\x04\x12\t\n\x05TICKS\x10\x05\x12\n\n\x06MINMAX\x10\x0f\"3\n\x0c\x44\x61teTimeKind\x12\x0f\n\x0bUNSPECIFIED\x10\x00\x12\x07\n\x03UTC\x10\x01\x12\t\n\x05LOCAL\x10\x02\"\x91\x01\n\x0eNetObjectProxy\x12\x19\n\x11\x65xistingObjectKey\x18\x01 \x01(\x05\x12\x14\n\x0cnewObjectKey\x18\x02 \x01(\x05\x12\x17\n\x0f\x65xistingTypeKey\x18\x03 \x01(\x05\x12\x12\n\nnewTypeKey\x18\x04 \x01(\x05\x12\x10\n\x08typeName\x18\x08 \x01(\t\x12\x0f\n\x07payload\x18\n \x01(\x0c\"\x1e\n\x04Guid\x12\n\n\x02lo\x18\x01 \x01(\x06\x12\n\n\x02hi\x18\x02 \x01(\x06\"4\n\x07\x44\x65\x63imal\x12\n\n\x02lo\x18\x01 \x01(\x04\x12\n\n\x02hi\x18\x02 \x01(\r\x12\x11\n\tsignScale\x18\x03 \x01(\rB\x0f\xaa\x02\x0cProtoBuf.Bclb\x06proto3')



_TIMESPAN = DESCRIPTOR.message_types_by_name['TimeSpan']
_DATETIME = DESCRIPTOR.message_types_by_name['DateTime']
_NETOBJECTPROXY = DESCRIPTOR.message_types_by_name['NetObjectProxy']
_GUID = DESCRIPTOR.message_types_by_name['Guid']
_DECIMAL = DESCRIPTOR.message_types_by_name['Decimal']
_TIMESPAN_TIMESPANSCALE = _TIMESPAN.enum_types_by_name['TimeSpanScale']
_DATETIME_TIMESPANSCALE = _DATETIME.enum_types_by_name['TimeSpanScale']
_DATETIME_DATETIMEKIND = _DATETIME.enum_types_by_name['DateTimeKind']
TimeSpan = _reflection.GeneratedProtocolMessageType('TimeSpan', (_message.Message,), {
  'DESCRIPTOR' : _TIMESPAN,
  '__module__' : 'protobuf_net.bcl_pb2'
  # @@protoc_insertion_point(class_scope:bcl.TimeSpan)
  })
_sym_db.RegisterMessage(TimeSpan)

DateTime = _reflection.GeneratedProtocolMessageType('DateTime', (_message.Message,), {
  'DESCRIPTOR' : _DATETIME,
  '__module__' : 'protobuf_net.bcl_pb2'
  # @@protoc_insertion_point(class_scope:bcl.DateTime)
  })
_sym_db.RegisterMessage(DateTime)

NetObjectProxy = _reflection.GeneratedProtocolMessageType('NetObjectProxy', (_message.Message,), {
  'DESCRIPTOR' : _NETOBJECTPROXY,
  '__module__' : 'protobuf_net.bcl_pb2'
  # @@protoc_insertion_point(class_scope:bcl.NetObjectProxy)
  })
_sym_db.RegisterMessage(NetObjectProxy)

Guid = _reflection.GeneratedProtocolMessageType('Guid', (_message.Message,), {
  'DESCRIPTOR' : _GUID,
  '__module__' : 'protobuf_net.bcl_pb2'
  # @@protoc_insertion_point(class_scope:bcl.Guid)
  })
_sym_db.RegisterMessage(Guid)

Decimal = _reflection.GeneratedProtocolMessageType('Decimal', (_message.Message,), {
  'DESCRIPTOR' : _DECIMAL,
  '__module__' : 'protobuf_net.bcl_pb2'
  # @@protoc_insertion_point(class_scope:bcl.Decimal)
  })
_sym_db.RegisterMessage(Decimal)

if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  DESCRIPTOR._serialized_options = b'\252\002\014ProtoBuf.Bcl'
  _TIMESPAN._serialized_start=32
  _TIMESPAN._serialized_end=206
  _TIMESPAN_TIMESPANSCALE._serialized_start=103
  _TIMESPAN_TIMESPANSCALE._serialized_end=206
  _DATETIME._serialized_start=209
  _DATETIME._serialized_end=478
  _DATETIME_TIMESPANSCALE._serialized_start=103
  _DATETIME_TIMESPANSCALE._serialized_end=206
  _DATETIME_DATETIMEKIND._serialized_start=427
  _DATETIME_DATETIMEKIND._serialized_end=478
  _NETOBJECTPROXY._serialized_start=481
  _NETOBJECTPROXY._serialized_end=626
  _GUID._serialized_start=628
  _GUID._serialized_end=658
  _DECIMAL._serialized_start=660
  _DECIMAL._serialized_end=712
# @@protoc_insertion_point(module_scope)
