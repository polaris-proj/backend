import logging
import uuid
from typing import List, Optional

from sqlalchemy import select, update
from sqlalchemy.ext.asyncio import AsyncSession

from src.OrderConfirmation.models.telegram_user import TelegramUser

from src.OrderConfirmation.utils.misc import save_commit

logger = logging.getLogger(__name__)


async def get_telegram_user_by_user_id(session: AsyncSession, user_id: uuid.UUID) -> Optional[TelegramUser]:
    stmt = select(TelegramUser).where(TelegramUser.user_id == user_id)
    result = await session.execute(stmt)

    return result.scalar_one_or_none()


async def get_telegram_user(session: AsyncSession, telegram_id: int) -> Optional[TelegramUser]:
    stmt = select(TelegramUser).where(TelegramUser.telegram_id == telegram_id)
    result = await session.execute(stmt)

    return result.scalar_one_or_none()


async def create_telegram_user(
    session: AsyncSession,
    telegram_id: str,
    user_id: Optional[uuid.UUID] = None,
    username: str = None,
    fullname: str = None,
) -> TelegramUser:
    tg_user = TelegramUser(telegram_id=telegram_id, user_id=user_id, username=username, fullname=fullname)

    session.add(tg_user)
    await save_commit(session)

    await session.flush()
    await session.refresh(tg_user)
    return tg_user


async def update_telegram_user(
    session: AsyncSession,
    tg_user: TelegramUser,
    user_id: Optional[uuid.UUID] = None,
    username: str = None,
    fullname: str = None,
) -> TelegramUser:
    logger.debug(f"Update tg_user {tg_user.telegram_id=}")

    stmt = update(TelegramUser).where(TelegramUser.telegram_id == tg_user.telegram_id)

    if user_id:
        stmt = stmt.values(user_id=user_id)
    if username:
        stmt = stmt.values(username=username)
    if fullname:
        stmt = stmt.values(fullname=fullname)

    stmt = stmt.returning(TelegramUser)
    u = await session.execute(stmt)
    await session.commit()

    return u.scalars().one_or_none()


async def get_or_create_telegram_user(
    session: AsyncSession,
    telegram_id: int,
    user_id: Optional[uuid.UUID] = None,
    username: str = None,
    fullname: str = None,
) -> TelegramUser:
    tg_user = await get_telegram_user(session, telegram_id)

    if tg_user:
        return tg_user

    return await create_telegram_user(session, telegram_id, user_id, username, fullname)


async def get_telegram_users(session: AsyncSession, offset: int = 10, limit: int = 10) -> List[TelegramUser]:
    """
    Fetches users
    :param session:
    :param offset:
    :param limit:
    :return:
    """

    results = await session.execute(select(TelegramUser).offset(offset).limit(limit))

    # noinspection PyTypeChecker
    return results.scalars().fetchall()


async def get_user_by_tg_id(
    session: AsyncSession,
    telegram_id: int,
) -> Optional[TelegramUser]:
    stmt = select(TelegramUser).where(TelegramUser.telegram_id == telegram_id)

    result = await session.execute(stmt)

    return result.scalars().first()


async def get_user_by_id(
    session: AsyncSession,
    user_id: int,
) -> Optional[TelegramUser]:
    stmt = select(TelegramUser).where(TelegramUser.user_id == user_id)

    result = await session.execute(stmt)

    return result.scalars().first()
