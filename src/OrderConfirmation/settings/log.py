from .base import AdvancedBaseSettings


class LoggingSettings(AdvancedBaseSettings):
    debug: bool
    format: str

    class Config:
        env_prefix = "LOGGING_"


logging_settings = LoggingSettings()
