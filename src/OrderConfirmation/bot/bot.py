from typing import Tuple

from aiogram import Bot, Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage

from src.OrderConfirmation.settings.telegram import telegram_settings

from . import handlers, middlewares


def get_app() -> Dispatcher:
    memory_storage = MemoryStorage()

    bot = Bot(telegram_settings.token, parse_mode="HTML")

    dp = Dispatcher(bot, storage=memory_storage)

    handlers.setup(dp)
    middlewares.setup(dp)

    return dp
