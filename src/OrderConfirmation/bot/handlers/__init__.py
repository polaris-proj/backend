from aiogram import Dispatcher

from . import commands, startup


def setup(dp: Dispatcher):
    commands.setup(dp)
