import logging

from aiogram import types
from aiogram.dispatcher.middlewares import BaseMiddleware
from sqlalchemy.ext.asyncio import AsyncSession

from src.OrderConfirmation.database import engine


class ResourcesMiddleware(BaseMiddleware):
    """
    Middleware for providing resources like db-connection and UrFU-client
    """

    def __init__(self):
        """
        Initializes self
        """

        self._logger = logging.getLogger("resources_middleware")

        super().__init__()

    @staticmethod
    async def _provide_db_session() -> AsyncSession:
        """
        Provides `AsyncSession` object
        :return: Initialized session
        """

        session = AsyncSession(engine)

        return session

    async def _provide_resources(self) -> dict:
        """
        Initializes & provides needed resources, such as `AsyncSession`
        :return:
        """
        self._logger.debug("Providing resources")
        db_session = await self._provide_db_session()

        resources = {
            "db_session": db_session,
        }

        return resources

    async def _cleanup(self, data: dict):
        """
        Closes connections & etc.
        :param data:
        :return:
        """

        self._logger.debug("Cleaning resources")

        if "db_session" in data:
            self._logger.debug("SQLAlchemy session detected, closing connection.")
            session: AsyncSession = data["db_session"]
            await session.commit()
            await session.close()

    async def on_pre_process_message(self, update: types.Message, data: dict):
        """
        For pre-processing `types.Update`
        :param data: Data from other middlewares
        :param update: A telegram-update
        :return:
        """
        await update.answer_chat_action("typing")
        resources = await self._provide_resources()

        data.update(resources)

        return data

    async def on_pre_process_callback_query(self, query: types.CallbackQuery, data: dict):
        """
        Method for preprocessing callback-queries
        :param query: A callback-query
        :param data: A data from another middleware
        :return:
        """

        resources = await self._provide_resources()

        data.update(resources)

        return data

    async def on_post_process_callback_query(self, query: types.CallbackQuery, data_from_handler: list, data: dict):
        """
        Method for post-processing callback query
        :param data_from_handler: Data from handler
        :param query: A callback query
        :param data: A data from another middleware
        :return:
        """

        await self._cleanup(data)

    async def on_post_process_message(self, message: types.Message, data_from_handler: list, data: dict):
        """
        For post-processing message
        :param data_from_handler:
        :param message:
        :param data:
        :return:
        """
        await self._cleanup(data)
