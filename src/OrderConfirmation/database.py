from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import sessionmaker

from src.OrderConfirmation.settings.db import service_database_settings

engine = create_async_engine(service_database_settings.async_postgresql_url)

async_session_maker = sessionmaker(bind=engine, class_=AsyncSession)
