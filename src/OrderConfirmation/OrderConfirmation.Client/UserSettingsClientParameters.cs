﻿using Infrastructure;

namespace OrderConfirmation.Client;

public class UserSettingsClientParameters : IParameters
{
    public UserSettingsClientParameters(IVault vault)
    {
        Address = vault.GetValue("OrderConfirmation:gRPC:Url");
    }

    public string Address { get; }
}