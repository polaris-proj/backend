﻿namespace OrderConfirmation.Client;

public interface IUserSettingsClient
{
    Task<string> SendMessage(string message);
    Task<string> GetLinkToTelegramBot(Guid userId);
}