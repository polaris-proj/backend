﻿using Domain;
using Domain.Connectors;
using Domain.StrategyProcessor;
using Grpc.Net.Client;

namespace OrderConfirmation.Client;

public class UserSettingsClient : IUserSettingsClient
{
    public UserSettingsClient(UserSettingsClientParameters clientParameters)
    {
        _address = clientParameters.Address;
    }

    public async Task<string> SendMessage(string message)
    {
        using (var channel = GrpcChannel.ForAddress(_address))
        {
            var client = new OrderConfirmation.OrderConfirmationClient(channel);
            var answer = await client.SendMessageAsync(new SendMessageArgument {Message = message});

            if (answer.Status == Status.Error)
                throw new Exception(answer.Message);

            return answer.Message;
        }
    }

    public Task<string> GetLinkToTelegramBot(Guid userId)
    {
        using (var channel = GrpcChannel.ForAddress(_address))
        {
            var client = new OrderConfirmation.OrderConfirmationClient(channel);
            var answer = client.GetLinkToTelegramBot(new GetLinkToTelegramBotArgument {UserId = userId.ToString()});

            if (answer.Status == Status.Error)
                throw new Exception(answer.Message);

            return Task.FromResult(answer.Message);
        }
    }

    public Task<OrderId> CreateMarketOrder(Pair pair, MarketOrder orderType, decimal amount)
    {
        using (var channel = GrpcChannel.ForAddress(_address))
        {
            var client = new OrderConfirmation.OrderConfirmationClient(channel);
            var answer = client.SendMessage(new SendMessageArgument());

            if (answer.Status == Status.Error)
                throw new Exception(answer.Message);

            //return answer.Message;
        }

        throw new NotImplementedException();
    }

    public Task<OrderId> CreateLimitOrder(Pair pair, LimitOrder orderType, PlacementOrder entry)
    {
        using (var channel = GrpcChannel.ForAddress(_address))
        {
            var client = new OrderConfirmation.OrderConfirmationClient(channel);
            var answer = client.SendMessage(new SendMessageArgument());

            if (answer.Status == Status.Error)
                throw new Exception(answer.Message);

            //return answer.Message;
        }

        throw new NotImplementedException();
    }

    public Task<OrderId> CreateFuturesOrder(Pair pair, FuturesOrder orderType, int leverage, PlacementOrder entry,
        PlacementOrder? take = null, PlacementOrder? stop = null)
    {
        using (var channel = GrpcChannel.ForAddress(_address))
        {
            var client = new OrderConfirmation.OrderConfirmationClient(channel);
            var answer = client.SendMessage(new SendMessageArgument());

            if (answer.Status == Status.Error)
                throw new Exception(answer.Message);

            //return answer.Message;
        }

        throw new NotImplementedException();
    }

    private readonly string _address;
}