﻿using Domain;
using Domain.Connectors;
using Domain.StrategyProcessor;

namespace OrderConfirmation.Client;

public interface IOrderSenderClient
{
    Task<OrderId> CreateMarketOrder(Pair pair, MarketOrder orderType, decimal amount);

    Task<OrderId> CreateLimitOrder(Pair pair, LimitOrder orderType, PlacementOrder entry);

    Task<OrderId> CreateFuturesOrder(Pair pair, FuturesOrder orderType, int leverage, PlacementOrder entry,
        PlacementOrder? take = null, PlacementOrder? stop = null);
}