﻿using System.Reflection;
using System.ServiceModel;
using ProtoBuf.Grpc.Reflection;
using ProtoBuf.Meta;

namespace ProtoBufSchemeGenerator;

internal static class Program
{
    public static void Main(string[] args)
    {
        const string protosFolderName = "Protos";
        var pathToBinaryAssembly = args[0];
        var binaryFolderPath = Path.GetDirectoryName(pathToBinaryAssembly);
        if (binaryFolderPath is null)
            throw new Exception("Can't get current folder");
        
        var projectDirectory = Path.Combine(
            pathToBinaryAssembly.Split(Path.DirectorySeparatorChar).SkipLast(4).ToArray()); //skip bin , (debug/release), net6.0, binaryName
        var pathToProtosDirectory = Path.Combine(projectDirectory, protosFolderName);

        if (!Directory.Exists(pathToProtosDirectory))
            Directory.CreateDirectory(pathToProtosDirectory);

        var assembly = Assembly.LoadFile(pathToBinaryAssembly) ?? throw new Exception("Can't load Assembly");
        AppDomain.CurrentDomain.AssemblyResolve += (x, y) => LoadDependency(x, y, binaryFolderPath);
        AppDomain.CurrentDomain.TypeResolve += (x, y) => LoadDependency(x, y, binaryFolderPath);

        var contracts = assembly.GetTypes()
            .Where(x => x.IsInterface &&
                        x.CustomAttributes.Any(atr => atr.AttributeType == typeof(ServiceContractAttribute)));

        
        var generator = new SchemaGenerator
        {
            ProtoSyntax = ProtoSyntax.Proto3
        };
        foreach (var typeOfInterface in contracts)
        {
            Console.WriteLine($"Creating schema for {typeOfInterface}");
            var schema = generator.GetSchema(typeOfInterface);
            var filePath = Path.Combine(pathToProtosDirectory, $"{typeOfInterface.Name}.proto");
            for (var _ = 0; _ < 10; _++)
            {
                try
                {
                    File.WriteAllText(filePath, schema);
                    break;
                }
                catch (Exception e)
                {
                    // ignored
                }
            }
        }
    }

    private static Assembly LoadDependency(object? _, ResolveEventArgs args, string dirPath)
    {
        var pathToDependency = Path.Combine(dirPath, args.Name.Split(',')[0] + ".dll");
        return Assembly.LoadFile(pathToDependency);
    }
}