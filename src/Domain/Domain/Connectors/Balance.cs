﻿using System.Runtime.Serialization;

namespace Domain.Connectors;

[DataContract]
public class Balance
{
    public Balance(Ticker asset, decimal available, decimal locked)
    {
        Asset = asset;
        Available = available;
        Locked = locked;
    }

    /// <summary>
    /// The asset this balance is for
    /// </summary>
    [DataMember(Order = 1)]
    public Ticker Asset { get; set; }

    /// <summary>
    /// The quantity that isn't locked in a trade
    /// </summary>
    [DataMember(Order = 2)]
    public decimal Available { get; set; }

    /// <summary>
    /// The quantity that is currently locked in a trade
    /// </summary>
    [DataMember(Order = 3)]
    public decimal Locked { get; set; }

    /// <summary>
    /// The total balance of this asset (Free + Locked)
    /// </summary>
    public decimal Total => Available + Locked;
    
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    private Balance(){}//for protobuf dont remove
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
}