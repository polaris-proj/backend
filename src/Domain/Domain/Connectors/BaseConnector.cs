﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.StrategyProcessor;

namespace Domain.Connectors;

public abstract class BaseConnector<TConnector> : IConnector
    where TConnector : IConnector
{
    protected readonly TConnector Connector;

    protected BaseConnector(TConnector connector)
    {
        Connector = connector;
    }

    public virtual ValueTask DisposeAsync() => Connector.DisposeAsync();

    public virtual Task<Dictionary<Pair, decimal>> GetPrices() => Connector.GetPrices();

    public virtual Task<IReadOnlyList<Balance>> GetCoinsAmount() => Connector.GetCoinsAmount();

    public virtual Task<IReadOnlyList<Candle>> GetCandles(Pair pair, TimeFrame timeFrame, DateTimeRange range)
        => Connector.GetCandles(pair, timeFrame, range);

    public virtual Task<IReadOnlyList<Order>> GetOrdersPerPair(Pair pair) => Connector.GetOrdersPerPair(pair);

    public virtual Task<Order> GetOrderInfo(Pair pair, OrderId id) => Connector.GetOrderInfo(pair, id);

    public virtual Task CancelOrder(Pair pair, OrderId orderId) => Connector.CancelOrder(pair, orderId);
}