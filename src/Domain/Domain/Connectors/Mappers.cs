﻿using System;
using Domain.StrategyProcessor;

namespace Domain.Connectors;

public static class Mappers
{
    public static OrderType ToOrderType(this MarketOrder order)
    {
        return order switch
        {
            MarketOrder.Buy => OrderType.Buy,
            MarketOrder.Sell => OrderType.Sell,
            _ => throw new ArgumentOutOfRangeException(nameof(order), order, null)
        };
    }

    public static OrderType ToOrderType(this LimitOrder order)
    {
        return order switch
        {
            LimitOrder.BuyLimit => OrderType.BuyLimit,
            LimitOrder.SellLimit => OrderType.SellLimit,
            _ => throw new ArgumentOutOfRangeException(nameof(order), order, null)
        };
    }

    public static OrderType ToOrderType(this FuturesOrder order)
    {
        return order switch
        {
            FuturesOrder.Long => OrderType.Long,
            FuturesOrder.Short => OrderType.Short,
            _ => throw new ArgumentOutOfRangeException(nameof(order), order, null)
        };
    }

    public static MarketOrder ToMarketOrder(this OrderType orderType)
    {
        return orderType switch
        {
            OrderType.Buy => MarketOrder.Buy,
            OrderType.Sell => MarketOrder.Sell,
            _ => throw new ArgumentOutOfRangeException(nameof(orderType), orderType, null)
        };
    }

    public static LimitOrder ToLimitOrder(this OrderType orderType)
    {
        return orderType switch
        {
            OrderType.BuyLimit => LimitOrder.BuyLimit,
            OrderType.SellLimit => LimitOrder.SellLimit,
            _ => throw new ArgumentOutOfRangeException(nameof(orderType), orderType, null)
        };
    }

    public static FuturesOrder ToFuturesOrder(this OrderType orderType)
    {
        return orderType switch
        {
            OrderType.Long => FuturesOrder.Long,
            OrderType.Short => FuturesOrder.Short,
            _ => throw new ArgumentOutOfRangeException(nameof(orderType), orderType, null)
        };
    }
}