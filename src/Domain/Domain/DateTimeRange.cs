using System;
using System.Runtime.Serialization;

namespace Domain;

[DataContract]
public struct DateTimeRange
{
    [DataMember(Order = 1)] public DateTime Begin;
    [DataMember(Order = 2)] public DateTime End;

    public DateTimeRange(DateTime begin, DateTime end)
    {
        if (end < begin)
        {
            throw new ArgumentOutOfRangeException($"{end} should be more {begin}");
        }

        Begin = begin;
        End = end;
    }
}