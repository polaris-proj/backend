﻿namespace Domain.Patterns.Objects;

public class NewMovingAverageEvent : ITradeEvent
{
    public decimal Value { get; set; }
    public MovingAverageType Type { get; set; }

    public required Pair Pair { get; set; }
    public TimeFrame TimeFrame { get; set; }
    public Exchange Exchange { get; set; }
    public long TimeStamp { get; set; }
}

public enum MovingAverageType
{
    Open,
    High,
    Low,
    Close
}