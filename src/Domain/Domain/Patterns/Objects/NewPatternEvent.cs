﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Domain.Patterns.Objects;

[DataContract]
public class NewPatternEvent : ITradeEvent
{
    [DataMember(Order = 1)] public required Exchange Exchange { get; set; }
    [DataMember(Order = 2)] public required Pair Pair { get; set; }
    [DataMember(Order = 3)] public required TimeFrame TimeFrame { get; set; }
    [DataMember(Order = 4)] public required long TimeStamp { get; set; }
    [DataMember(Order = 5)] public required RecognitionResult RecognitionResult { get; set; }
}

[DataContract]
public class RecognitionResult
{
    [DataMember(Order = 1)] public required Candle[] UsedCandles { get; set; }
    [DataMember(Order = 2)] public required Dictionary<PatternType, float> PatternToProba { get; set; }
    [DataMember(Order = 3)] public required PatternType MostProbablePattern { get; set; }
    [DataMember(Order = 4)] public required bool IsSuccessful { get; set; }
    [DataMember(Order = 5)] public string? FailReason { get; set; }
}

public enum PatternType
{
    NoPattern = 0,
    UpImpulseOrDiagExpand = 1,
    UpDiagShrink = 2,
    DownImpulseOrDiagExpand = 3,
    DownDiagShrink = 4,
}