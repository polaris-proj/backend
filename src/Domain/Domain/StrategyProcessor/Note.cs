﻿using System.Runtime.Serialization;

namespace Domain.StrategyProcessor;

[DataContract]
public class Note
{
    [DataMember(Order = 1)] public required long TimeStamp { get; set; }
    [DataMember(Order = 2)] public required decimal Price { get; set; }
    [DataMember(Order = 3)] public string? Text { get; set; }
    [DataMember(Order = 4)] public required Icon Icon { get; set; }
    [DataMember(Order = 5)] public required Pair Pair { get; set; }
    [DataMember(Order = 6)] public required TimeFrame TimeFrame { get; set; }
}