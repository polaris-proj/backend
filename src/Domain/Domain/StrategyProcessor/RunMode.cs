﻿namespace Domain.StrategyProcessor;

public enum RunMode
{
    Real = 0,
    HistoryTest = 1,
    RealDataTest = 2,
}

public static class RunModeExtensions
{
    public static bool IsTest(this RunMode runMode) => runMode is RunMode.HistoryTest or RunMode.RealDataTest;
    public static bool IsRealTime(this RunMode runMode) => runMode is RunMode.RealDataTest or RunMode.Real;
}