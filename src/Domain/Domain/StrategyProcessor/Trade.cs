﻿using System.Runtime.Serialization;
using System.Text.Json.Serialization;

// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable PropertyCanBeMadeInitOnly.Global
#pragma warning disable CS8618

namespace Domain.StrategyProcessor;

[DataContract]
public class Trade
{
    public Trade()
    {
    }

    [JsonConstructor]
    public Trade(decimal price, decimal amount)
    {
        Amount = amount;
        Price = price;
    }

    public decimal Price { get; set; }
    public decimal Amount { get; set; }
}