﻿using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Domain.Connectors;
using Domain.PatternRecognition;
using Domain.Patterns.Objects;

namespace Domain.StrategyProcessor;

public abstract record BaseStrategy<TConnector> where TConnector : IConnector
{
    [JsonInclude] public StrategyStatus Status { get; protected set; } = StrategyStatus.Created;
    [JsonIgnore] public TConnector Connector { get; set; } = default!;
    [JsonIgnore] public IChartDataStorage ChartDataStorage { get; set; } = null!;
    [JsonIgnore] public virtual LifeTime LifeTime => LifeTime.SingleInstance;

    public virtual void Stop() { }

    public virtual void Start() { }

    protected async Task<AutoSyncOrder> GetAutoSyncOrder(Pair pair, OrderId id,
        UpdatePolicy updatePolicy = UpdatePolicy.LimitRps)
    {
        var order = await Connector.GetOrderInfo(pair, id);
        return new AutoSyncOrder(Connector, id, pair, order.OrderType, order.Price, order.Amount, updatePolicy);
    }

    protected void CreateNote(Pair pair, TimeFrame timeFrame, long timeStamp, decimal price, string? text = null,
        Icon icon = 0)
    {
        ChartDataStorage.CreateNote(new Note
        {
            Pair = pair,
            TimeFrame = timeFrame,
            TimeStamp = timeStamp,
            Price = price,
            Text = text,
            Icon = icon
        });
    }

    #region EventHandlers

    [TradeEventHandler]
    public virtual Task EventsCatalog_ReboundFromTheLevel(Point obj) => Task.CompletedTask;

    [TradeEventHandler]
    public virtual Task EventsCatalog_Slom(Point obj) => Task.CompletedTask;

    [TradeEventHandler]
    public virtual Task EventsCatalog_PP(Point obj) => Task.CompletedTask;

    [TradeEventHandler]
    public virtual Task EventsCatalog_NewZigZag(NewZigZagEvent obj) => Task.CompletedTask;

    [TradeEventHandler]
    public virtual Task EventsCatalog_NewCandle(NewCandleEvent obj) => Task.CompletedTask;
    
    [TradeEventHandler]
    public virtual Task EventsCatalog_NewPattern(NewPatternEvent obj) => Task.CompletedTask;

    [TradeEventHandler]
    public virtual Task EventsCatalog_NewAccumulation(Accumulation obj) => Task.CompletedTask;

    [TradeEventHandler]
    public virtual Task EventsCatalog_MovingAverage(NewMovingAverageEvent obj) => Task.CompletedTask;

    #endregion
}

public enum LifeTime
{
    SingleInstance = 1,
    SingleInstancePerExchangePairTimeframe = 2,
    MultipleInstancesControlledByStatus = 3,
}