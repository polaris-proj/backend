﻿using System;

namespace Domain.StrategyProcessor;

public interface IChartDataStorage : IAsyncDisposable
{
    void AddChartDot(string name, string description, Point point, Pair pair, TimeFrame timeFrame);
    void CreateNote(Note note);
}