﻿using System;

namespace Domain.StrategyProcessor;

public class StrategyParameter
{
    public StrategyParameter(string name, object value)
    {
        Name = name;
        Value = value;
    }

    public string Name { get; set; }
    public object Value { get; set; }
}

public class StrategyParameter<T> : StrategyParameter
{
    public StrategyParameter(string name, T value) :
        base(name, value ?? throw new ArgumentNullException(nameof(value)))
    {
    }
}