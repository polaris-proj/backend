﻿using System;
using Domain.Connectors;
using Domain.StrategyProcessor;

namespace WebApi.Models.ViewModel;

public class OrderViewModel
{
    public required Pair CurrencyPair { get; set; }
    public OrderType Type { get; set; }
    public OrderStatus Status { get; set; }
    public required decimal Price { get; set; }
    public decimal Amount { get; set; }
    public string? OrderId { get; set; }
    public long CreatedAt { get; set; }
    public DateTime? UpdatedAt { get; set; }
    public int? Leverage { get; set; }
    public decimal? TakePrice { get; set; }
    public decimal? StopPrice { get; set; }
}