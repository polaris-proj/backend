﻿namespace WebApi.Models.ViewModel;

public class CandleOutModel
{
    public CandleOutModel(long timeStamp, decimal open, decimal high, decimal low, decimal close, decimal volume)
    {
        TimeStamp = timeStamp;
        Open = open;
        High = high;
        Low = low;
        Close = close;
        Volume = volume;
    }

    public long TimeStamp { get; set; }
    public decimal Open { get; set; }
    public decimal High { get; set; }
    public decimal Low { get; set; }
    public decimal Close { get; set; }
    public decimal Volume { get; set; }
}