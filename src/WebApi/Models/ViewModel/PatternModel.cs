﻿using System.Collections.Generic;
using Domain.StrategyProcessor;

namespace WebApi.Models.ViewModel;

public class PatternModel
{
    public required string Name { get; set; }
     public required List<Point> PatternPoints { get; set; }
}