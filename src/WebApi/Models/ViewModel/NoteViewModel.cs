﻿using Domain.StrategyProcessor;

namespace WebApi.Models.ViewModel;

public class NoteViewModel
{
    public required long TimeStamp { get; set; }
    public required decimal Price { get; set; }
    public string? Text { get; set; }
    public required Icon Icon { get; set; }
}