﻿using System.Collections.Generic;
using Domain.StrategyProcessor;

namespace WebApi.Models.ViewModel;

public class ChartViewModel
{
    public required string Name { get; set; }
    public string? Description { get; set; }
    public required TimeFrame TimeFrame { get; set; }
    public required Pair Pair { get; set; }
    public required List<Point> Chart { get; set; }
}