﻿using System.ComponentModel.DataAnnotations;
using WebApi.Infrastructure.Mappers;

namespace WebApi.Models;

public class ParameterModel
{
    [Required] public string Name { get; set; }
    [Required] public ParameterMapper.ParameterType TypeOfValue { get; set; }
    [Required] public string Value { get; set; }
}