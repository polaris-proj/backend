﻿using WebApi.Models.ViewModel;

namespace WebApi.Models;

public class PairTimeFrame
{
    public required Pair Pair { get; set; }
    public TimeFrame TimeFrame { get; set; }
}