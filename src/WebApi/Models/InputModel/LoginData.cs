﻿using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.InputModel;

public class LoginData
{
    [Required] public required string Login { get; set; }
    [Required] public required string Password { get; set; }
}