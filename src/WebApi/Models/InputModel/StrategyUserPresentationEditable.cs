﻿using System;


namespace WebApi.Models.InputModel;

public class StrategyUserPresentationEditable
{
    public Guid Id { get; set; }
    public string Name { get; set; }
    public Description[] Descriptions { get; set; }
}