﻿namespace WebApi.Models.InputModel;

public class RegisterData
{
    public required string Login { get; set; }
    public required string Password { get; set; }
    public required string Email { get; set; }
}