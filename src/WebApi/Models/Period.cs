﻿namespace WebApi.Models;

public class Period
{
    public int Year { get; set; }
    public int Month { get; set; }
    public int Week { get; set; }
    public int Day { get; set; }
    public int Hours12 { get; set; }
    public int Hour { get; set; }
}