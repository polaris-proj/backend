﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Tokens;
using WebApi.Infrastructure.DAL.Dbo;
using WebApi.Infrastructure.DAL.Repositories;

namespace WebApi.Infrastructure.Auth;

public class JwtAuthManager : IJwtAuthManager
{
    public JwtAuthManager(UserRepository userStorage, RefreshTokenRepository refreshTokenRepository)
    {
        _userStorage = userStorage;
        _refreshTokenRepository = refreshTokenRepository;
        _secret = Encoding.ASCII.GetBytes(JwtTokenConfig.Secret);
    }


    // optional: clean up expired refresh tokens

    public async Task RemoveExpiredRefreshTokens(DateTime now)
    {
        await _refreshTokenRepository.RemoveExpired(now);
    }


    // can be more specific to ip, user agent, device name, etc.

    public async Task RemoveRefreshTokenByUserName(string userName)
    {
        await _refreshTokenRepository.RemoveByLogin(userName);
    }

    public async Task<JwtAuthResult> GenerateTokens(string username, Claim[] claims, DateTime now)
    {
        var shouldAddAudienceClaim =
            string.IsNullOrWhiteSpace(claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Aud)?.Value);

        var jwtToken = new JwtSecurityToken(
            JwtTokenConfig.Issuer,
            shouldAddAudienceClaim ? JwtTokenConfig.Audience : string.Empty,
            claims,
            expires: now.AddMinutes(JwtTokenConfig.AccessTokenExpiration),
            signingCredentials: new SigningCredentials(new SymmetricSecurityKey(_secret),
                SecurityAlgorithms.HmacSha256Signature));
        var accessToken = new JwtSecurityTokenHandler().WriteToken(jwtToken);

        var refreshToken = new RefreshToken
        {
            Login = username,
            TokenString = GenerateRefreshTokenString(),
            ExpireAt = now.AddMinutes(JwtTokenConfig.RefreshTokenExpiration)
        };
        await _refreshTokenRepository.AddOrUpdateAsync(RefreshTokenDbo.FromDto(refreshToken));

        return new JwtAuthResult
        {
            AccessToken = accessToken,
            RefreshToken = refreshToken
        };
    }

    public async Task<JwtAuthResult> Refresh(string refreshToken, string accessToken, DateTime now)
    {
        if (string.IsNullOrWhiteSpace(accessToken)) throw new SecurityTokenException("Invalid token");

        var existingRefreshToken = await _refreshTokenRepository.ReadAsync(refreshToken);
        if (existingRefreshToken == null)
            throw new SecurityTokenException("Invalid token");

        var login = existingRefreshToken.Login;
        if (existingRefreshToken.Login != login || existingRefreshToken.ExpireAt < now)
            throw new SecurityTokenException("Invalid token");

        var user = await _userStorage.GetByLoginAsync(login);
        if (user == default)
            throw new SecurityException($"User with login: {login} not found");


        var claims = new List<Claim>
        {
            new(ClaimTypes.Name, login),
            new(ClaimTypes.NameIdentifier, user.Id.ToString())
        };

        claims.AddRange(user.Roles.Select(role => new Claim(ClaimTypes.Role, role.Role.ToString())));

        return await GenerateTokens(login, claims.ToArray(), now); // need to recover the original claims
    }

    private static string GenerateRefreshTokenString()
    {
        var randomNumber = new byte[32];
        using var randomNumberGenerator = RandomNumberGenerator.Create();
        randomNumberGenerator.GetBytes(randomNumber);
        return Convert.ToBase64String(randomNumber);
    }


    private readonly byte[] _secret;
    private readonly UserRepository _userStorage;
    private readonly RefreshTokenRepository _refreshTokenRepository;
}

public class JwtAuthResult
{
    [JsonPropertyName("accessToken")] public string AccessToken { get; set; } = null!;

    [JsonPropertyName("refreshToken")] public RefreshToken RefreshToken { get; set; } = null!;
}

public class RefreshToken
{
    [JsonPropertyName("login")] public string Login { get; set; } = null!; // can be used for usage tracking
    // can optionally include other metadata, such as user agent, ip address, device name, and so on

    [JsonPropertyName("tokenString")] public string TokenString { get; set; } = null!;

    [JsonPropertyName("expireAt")] public DateTime ExpireAt { get; set; }
}