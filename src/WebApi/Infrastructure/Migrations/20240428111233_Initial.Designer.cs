﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;
using WebApi.Infrastructure.DAL;

#nullable disable

namespace WebApi.Infrastructure.Migrations
{
    [DbContext(typeof(WebApiApplicationContext))]
    [Migration("20240428111233_Initial")]
    partial class Initial
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "8.0.0")
                .HasAnnotation("Proxies:ChangeTracking", false)
                .HasAnnotation("Proxies:CheckEquality", false)
                .HasAnnotation("Proxies:LazyLoading", true)
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            NpgsqlModelBuilderExtensions.UseIdentityByDefaultColumns(modelBuilder);

            modelBuilder.Entity("WebApi.Infrastructure.DAL.Dbo.DescriptionDbo", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<Guid?>("StrategyDboId")
                        .HasColumnType("uuid");

                    b.Property<Guid>("StrategyUserPresentationDboId")
                        .HasColumnType("uuid");

                    b.Property<string>("Text")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasColumnType("text");

                    b.HasKey("Id");

                    b.HasIndex("StrategyDboId");

                    b.ToTable("DescriptionDbo");
                });

            modelBuilder.Entity("WebApi.Infrastructure.DAL.Dbo.RefreshTokenDbo", b =>
                {
                    b.Property<string>("TokenString")
                        .HasMaxLength(100)
                        .HasColumnType("character varying(100)");

                    b.Property<DateTime>("ExpireAt")
                        .HasColumnType("timestamp with time zone");

                    b.Property<string>("Login")
                        .IsRequired()
                        .HasMaxLength(100)
                        .HasColumnType("character varying(100)");

                    b.HasKey("TokenString");

                    b.HasIndex("Login");

                    b.ToTable("RefreshTokenDbos");
                });

            modelBuilder.Entity("WebApi.Infrastructure.DAL.Dbo.RoleDbo", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<int>("Role")
                        .HasColumnType("integer");

                    b.HasKey("Id");

                    b.ToTable("RoleDbo");
                });

            modelBuilder.Entity("WebApi.Infrastructure.DAL.Dbo.StartTestOfStrategyParametersDbo", b =>
                {
                    b.Property<Guid>("RunId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<bool>("IsTest")
                        .HasColumnType("boolean");

                    b.Property<string>("ModelJson")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<Guid>("StrategyId")
                        .HasColumnType("uuid");

                    b.Property<Guid>("UserId")
                        .HasColumnType("uuid");

                    b.HasKey("RunId");

                    b.HasIndex("UserId", "StrategyId");

                    b.ToTable("StartTestOfStrategyParametersDbos");
                });

            modelBuilder.Entity("WebApi.Infrastructure.DAL.Dbo.StrategyDbo", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<string>("Name")
                        .HasMaxLength(150)
                        .HasColumnType("character varying(150)");

                    b.HasKey("Id");

                    b.ToTable("StrategyUserPresentation");
                });

            modelBuilder.Entity("WebApi.Infrastructure.DAL.Dbo.StrategyUserRunsDbo", b =>
                {
                    b.Property<Guid>("UserId")
                        .HasColumnType("uuid");

                    b.Property<Guid>("StrategyId")
                        .HasColumnType("uuid");

                    b.HasKey("UserId", "StrategyId");

                    b.ToTable("StrategyUserRunsDbos");
                });

            modelBuilder.Entity("WebApi.Infrastructure.DAL.Dbo.UserDbo", b =>
                {
                    b.Property<Guid>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uuid");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasMaxLength(150)
                        .HasColumnType("character varying(150)");

                    b.Property<string>("Login")
                        .IsRequired()
                        .HasMaxLength(150)
                        .HasColumnType("character varying(150)");

                    b.Property<string>("PasswordHash")
                        .IsRequired()
                        .HasMaxLength(150)
                        .HasColumnType("character varying(150)");

                    b.HasKey("Id");

                    b.ToTable("UserDbo");
                });

            modelBuilder.Entity("WebApi.Infrastructure.DAL.Dbo.UserRoleDbo", b =>
                {
                    b.Property<Guid>("RoleId")
                        .HasColumnType("uuid");

                    b.Property<Guid>("UserId")
                        .HasColumnType("uuid");

                    b.Property<DateTime>("AssignmentDateTime")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("timestamp with time zone")
                        .HasDefaultValueSql("CURRENT_TIMESTAMP");

                    b.HasKey("RoleId", "UserId");

                    b.HasIndex("UserId");

                    b.ToTable("UserRole", (string)null);
                });

            modelBuilder.Entity("WebApi.Infrastructure.DAL.Dbo.DescriptionDbo", b =>
                {
                    b.HasOne("WebApi.Infrastructure.DAL.Dbo.StrategyDbo", null)
                        .WithMany("Descriptions")
                        .HasForeignKey("StrategyDboId");
                });

            modelBuilder.Entity("WebApi.Infrastructure.DAL.Dbo.StartTestOfStrategyParametersDbo", b =>
                {
                    b.HasOne("WebApi.Infrastructure.DAL.Dbo.StrategyUserRunsDbo", null)
                        .WithMany("RunsId")
                        .HasForeignKey("UserId", "StrategyId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("WebApi.Infrastructure.DAL.Dbo.UserDbo", b =>
                {
                    b.OwnsOne("WebApi.Infrastructure.DAL.Dbo.UserExtraFields", "ExtraFields", b1 =>
                        {
                            b1.Property<Guid>("UserDboId")
                                .HasColumnType("uuid");

                            b1.Property<string>("Photo")
                                .HasColumnType("text");

                            b1.HasKey("UserDboId");

                            b1.ToTable("UserDbo");

                            b1.WithOwner()
                                .HasForeignKey("UserDboId");
                        });

                    b.Navigation("ExtraFields")
                        .IsRequired();
                });

            modelBuilder.Entity("WebApi.Infrastructure.DAL.Dbo.UserRoleDbo", b =>
                {
                    b.HasOne("WebApi.Infrastructure.DAL.Dbo.RoleDbo", "Role")
                        .WithMany("UserRole")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("WebApi.Infrastructure.DAL.Dbo.UserDbo", "UserDbo")
                        .WithMany("UserRole")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Role");

                    b.Navigation("UserDbo");
                });

            modelBuilder.Entity("WebApi.Infrastructure.DAL.Dbo.RoleDbo", b =>
                {
                    b.Navigation("UserRole");
                });

            modelBuilder.Entity("WebApi.Infrastructure.DAL.Dbo.StrategyDbo", b =>
                {
                    b.Navigation("Descriptions");
                });

            modelBuilder.Entity("WebApi.Infrastructure.DAL.Dbo.StrategyUserRunsDbo", b =>
                {
                    b.Navigation("RunsId");
                });

            modelBuilder.Entity("WebApi.Infrastructure.DAL.Dbo.UserDbo", b =>
                {
                    b.Navigation("UserRole");
                });
#pragma warning restore 612, 618
        }
    }
}
