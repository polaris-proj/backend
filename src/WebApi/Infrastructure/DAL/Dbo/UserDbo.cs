﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Infrastructure.DAL.Dbo;
using Microsoft.EntityFrameworkCore;

namespace WebApi.Infrastructure.DAL.Dbo;

public class UserDbo : IEntity<Guid>
{
    public Guid Id { get; set; }
    [MaxLength(150)] public required string Login { get; set; }
    [MaxLength(150)] public required string Email { get; set; }
    [MaxLength(150)] public required string PasswordHash { get; set; }
    public required UserExtraFields ExtraFields { get; set; }
    public ICollection<RoleDbo> Roles { get; set; } = new List<RoleDbo>();
}

[Owned]
public class UserExtraFields
{
    public string? Photo { get; set; }
}