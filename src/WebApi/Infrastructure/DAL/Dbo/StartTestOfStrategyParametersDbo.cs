﻿using System;
using System.Text.Json;

namespace WebApi.Infrastructure.DAL.Dbo;

public class StartTestOfStrategyParametersDbo
{
    public required Guid RunId { get; set; }
    public required DateTimeOffset TimeOfRun { get; set; }
    public required Guid StrategyId { get; set; }
    public required bool IsTest { get; set; }
    public required JsonDocument ModelJson { get; set; }
    public required Guid UserId { get; set; }
}