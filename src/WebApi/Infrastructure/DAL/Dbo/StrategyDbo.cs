﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Infrastructure.DAL.Dbo;

namespace WebApi.Infrastructure.DAL.Dbo;

public class StrategyDbo : IEntity<Guid>
{
    public Guid Id { get; set; }
    [MaxLength(150)] public string? Name { get; set; }
    public virtual ICollection<DescriptionDbo>? Descriptions { get; set; }
}