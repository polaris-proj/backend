﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain;


namespace WebApi.Infrastructure.DAL.Dbo;

public class UserApiKeysDbo
{
    public Guid UserApiKeysDboId { get; set; }
    public Guid UserDboId { get; set; }
    public virtual required UserDbo User { get; set; }
    public virtual required ICollection<ApiKeyDbo> Keys { get; set; }
}

public class ApiKeyDbo
{
    public Guid ApiKeyDboId { get; set; }
    public Guid UserApiKeysId { get; set; }
    public virtual required UserApiKeysDbo UserApiKeysDbo { get; set; }
    public Exchange Exchange { get; set; }
    [MaxLength(150)] public required string Key { get; set; }
    [MaxLength(150)] public required string Token { get; set; }
}