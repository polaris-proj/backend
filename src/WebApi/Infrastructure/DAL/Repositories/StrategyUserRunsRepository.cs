﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Infrastructure.Json;
using Microsoft.EntityFrameworkCore;
using WebApi.Infrastructure.DAL.Dbo;
using WebApi.Infrastructure.Mappers;
using WebApi.Models.InputModel;
using WebApi.Models.ViewModel;

namespace WebApi.Infrastructure.DAL.Repositories;

public class StrategyUserRunsRepository(WebApiApplicationContext context)
{
    public async Task AddStrategyRealRun(StartStrategyModel model, Guid userId, Guid runId)
    {
        await AddStrategyRunId(model, userId, runId, false);
    }

    public async Task AddStrategyTestRun(StartTestOfStrategyModel model, Guid userId, Guid runId)
    {
        await AddStrategyRunId(model, userId, runId, true);
    }

    public async Task<IEnumerable<TestResult>> GetStrategyRealRuns(Guid userId, Guid strategyId)
    {
        return await GetStrategyRunsId(userId, strategyId, false);
    }

    public async Task<IEnumerable<TestResult>> GetStrategyTestRuns(Guid userId, Guid strategyId)
    {
        return await GetStrategyRunsId(userId, strategyId, true);
    }

    private async Task AddStrategyRunId<TModel>(TModel model, Guid userId, Guid runId, bool isTest)
        where TModel : StartStrategyModel
    {
        var dbo = await context.StrategyUserRunsDbos
            .Include(x => x.RunsId)
            .FirstOrDefaultAsync(x => x.UserId == userId && x.StrategyId == model.StrategyId);

        var runOfStrategyParametersDbo = new StartTestOfStrategyParametersDbo
        {
            RunId = runId,
            TimeOfRun = DateTimeOffset.UtcNow,
            StrategyId = model.StrategyId,
            IsTest = isTest,
            UserId = userId,
            ModelJson = JsonSerializer.SerializeToDocument(model, JsonOptions.JsonSerializerOptions),
        };

        if (dbo != null)
        {
            context.StartTestOfStrategyParametersDbos.Add(runOfStrategyParametersDbo);
        }
        else
        {
            dbo = new StrategyUserRunsDbo
            {
                UserId = userId,
                StrategyId = model.StrategyId,
                RunsId = [runOfStrategyParametersDbo]
            };
            await context.StrategyUserRunsDbos.AddAsync(dbo);
        }

        await context.SaveChangesAsync();
    }

    private async Task<IEnumerable<TestResult>> GetStrategyRunsId(Guid userId, Guid strategyId, bool isTest)
    {
        var dbo = await context.StrategyUserRunsDbos
            .Include(x => x.RunsId)
            .AsNoTracking()
            .SingleOrDefaultAsync(x => x.UserId == userId && x.StrategyId == strategyId);

        return dbo?.RunsId
                   .Where(x => x.IsTest == isTest)
                   .Select(x => x.ToView())
                   .OrderByDescending(x => x.TimeOfRun)
                   .ToArray()
               ?? [];
    }
}