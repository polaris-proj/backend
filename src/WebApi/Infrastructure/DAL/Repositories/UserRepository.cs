﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using WebApi.Infrastructure.DAL.Dbo;

namespace WebApi.Infrastructure.DAL.Repositories;

public class UserRepository(IDbContextFactory<WebApiApplicationContext> contextFactory)
{
    public async Task<UserDbo?> GetByLoginAsync(string login)
    {
        await using var context = await contextFactory.CreateDbContextAsync();
        return await context.UserDbo.Include(x => x.Roles)
            .AsNoTracking()
            .SingleOrDefaultAsync(x => x.Login == login);
    }

    public async Task<UserDbo?> ReadAsync(Guid id)
    {
        await using var context = await contextFactory.CreateDbContextAsync();
        return await context.UserDbo.Include(x => x.Roles)
            .AsNoTracking()
            .SingleOrDefaultAsync(x => x.Id == id);
    }

    public async Task AddAsync(UserDbo user)
    {
        await using var context = await contextFactory.CreateDbContextAsync();
        await context.UserDbo.AddAsync(user);
        await context.SaveChangesAsync();
    }
}