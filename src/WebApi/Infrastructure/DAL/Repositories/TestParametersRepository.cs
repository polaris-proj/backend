﻿using System;
using System.Text.Json;
using System.Threading.Tasks;
using Infrastructure.Json;
using Microsoft.EntityFrameworkCore;
using WebApi.Infrastructure.DAL.Dbo;
using WebApi.Models.InputModel;

namespace WebApi.Infrastructure.DAL.Repositories;

public class TestParametersRepository(WebApiApplicationContext context)
{
    public async Task WriteAsync(StartTestOfStrategyModel model, Guid runId, Guid userId, bool isTest)
    {
        var dbo = new StartTestOfStrategyParametersDbo
        {
            RunId = runId,
            StrategyId = model.StrategyId,
            TimeOfRun = DateTimeOffset.UtcNow,
            UserId = userId,
            IsTest = isTest,
            ModelJson = JsonSerializer.SerializeToDocument(model, JsonOptions.JsonSerializerOptions),
        };

        await context.StartTestOfStrategyParametersDbos.AddAsync(dbo);
        await context.SaveChangesAsync();
    }

    public async Task<StartTestOfStrategyModel?> ReadAsync(Guid runId)
    {
        var dbo = await context.StartTestOfStrategyParametersDbos.FirstOrDefaultAsync(x => x.RunId == runId);
        return dbo?.ModelJson.Deserialize<StartTestOfStrategyModel>(JsonOptions.JsonSerializerOptions);
    }
}