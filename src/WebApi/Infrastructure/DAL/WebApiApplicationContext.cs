﻿using System.Text.Json;
using Infrastructure;
using Infrastructure.DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using WebApi.Infrastructure.DAL.Dbo;

namespace WebApi.Infrastructure.DAL;

public class WebApiApplicationContext : DbContext
{
    public DbSet<StrategyDbo> StrategyUserPresentation { get; set; } = null!;
    public DbSet<UserDbo> UserDbo { get; set; } = null!;
    public DbSet<StartTestOfStrategyParametersDbo> StartTestOfStrategyParametersDbos { get; set; } = null!;
    public DbSet<StrategyUserRunsDbo> StrategyUserRunsDbos { get; set; } = null!;
    public DbSet<RefreshTokenDbo> RefreshTokenDbos { get; set; } = null!;

    public WebApiApplicationContext(PostgresParameters postgresParameters, ILogger<WebApiApplicationContext> logger)
    {
        _postgresParameters = postgresParameters;
        _logger = logger;
        Database.EnsureCreated();
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder
            .Entity<RoleDbo>()
            .HasMany(c => c.Users)
            .WithMany(s => s.Roles);

        modelBuilder.Entity<StartTestOfStrategyParametersDbo>()
            .HasKey(x => x.RunId);

        modelBuilder.Entity<StrategyUserRunsDbo>()
            .HasKey(x => new {x.UserId, x.StrategyId});
        modelBuilder.Entity<StrategyUserRunsDbo>()
            .HasMany(x => x.RunsId)
            .WithOne()
            .HasForeignKey(x => new {x.UserId, x.StrategyId});

        modelBuilder.Entity<RefreshTokenDbo>().HasKey(x => x.TokenString);
        modelBuilder.Entity<RefreshTokenDbo>().HasIndex(x => x.Login);
    }

    protected override void ConfigureConventions(ModelConfigurationBuilder configurationBuilder)
    {
        //configurationBuilder.Properties<Pair>().HaveConversion<PairConverter>();
        configurationBuilder.Properties<JsonDocument>().HaveConversion<JsonConverter>();
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder
            .UseNpgsql(_postgresParameters.FullConnectionString)
            .LogTo(s => _logger.LogTrace(s));
    }

    // private class PairConverter : ValueConverter<Pair, string>
    // {
    //     public PairConverter() : base(
    //         v => v,
    //         v => new Pair(v))
    //     {
    //     }
    // }
    private readonly PostgresParameters _postgresParameters;
    private readonly ILogger<WebApiApplicationContext> _logger;
}