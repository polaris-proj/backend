﻿using WebApi.Models.ViewModel;

namespace WebApi.Infrastructure.Mappers;

public static class PairMapper
{
    public static Pair ToViewModel(this Domain.Pair pair)
    {
        return new Pair
        {
            First = pair.First.Name,
            Second = pair.Second.Name,
        };
    }
    
    public static Domain.Pair ToDomain(this Pair pair)
    {
        return new Domain.Pair(pair.First, pair.Second);
    }
}