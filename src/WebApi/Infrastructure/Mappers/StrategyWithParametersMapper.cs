﻿using System.Linq;
using StrategyProcessor.Shared;
using WebApi.Models.ViewModel;

namespace WebApi.Infrastructure.Mappers;

public static class StrategyWithParametersMapper
{
    public static StrategyWithParameters ToModel(this StrategyDto parameterDbo)
    {
        return new StrategyWithParameters
        {
            Id = parameterDbo.Id,
            InternalName = parameterDbo.InternalName,
            ConnectorType = parameterDbo.StrategyConnectorType,
            Properties = parameterDbo.Parameters.Select(x => x.ToModel()).ToArray()
        };
    }
}