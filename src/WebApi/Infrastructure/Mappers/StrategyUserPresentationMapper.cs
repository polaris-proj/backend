﻿using System.Linq;
using WebApi.Infrastructure.DAL.Dbo;
using WebApi.Models;

namespace WebApi.Infrastructure.Mappers;

public static class StrategyUserPresentationMapper
{

    public static StrategyUserPresentation ToModel(this StrategyDbo parameterDbo)
    {
        return new StrategyUserPresentation
        {
            Id = parameterDbo.Id,
            Name = parameterDbo.Name,
            Descriptions = parameterDbo.Descriptions?.Select(x=>x.ToModel()).ToArray()
        };
    }
}