﻿using Domain;
using Grpc.Net.Client;
using Infrastructure.Grpc;
using ProtoBuf.Grpc.Client;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Client;

public class StrategyProcessorClient(StrategyProcessorClientChannel channel) : IStrategyProcessorClient
{
    public async Task<Answer<Guid>> StartStrategy(StrategyStartDto strategyStartDto)
    {
        var calculator = _channel.CreateGrpcService<IStrategyServiceGrpc>();
        return await calculator.Start(strategyStartDto);
    }

    public async Task StopStrategy(StrategyStopDto strategyStopDto)
    {
        var calculator = _channel.CreateGrpcService<IStrategyServiceGrpc>();
        await calculator.StopStrategy(strategyStopDto);
    }

    public async Task<StrategyDto[]> GetAllStrategies()
    {
        var getStrategiesService = _channel.CreateGrpcService<IGetStrategiesService>();
        return await getStrategiesService.GetStrategies();
    }

    public async Task<TestOfStrategyDto> GetTestResult(Guid testId, Pair pair, TimeFrame timeFrame, DateTimeRange dateTimeRange)
    {
        var testOfStrategyService = _channel.CreateGrpcService<IRunOfStrategyService>();
        var result = await testOfStrategyService.GetRunResult(new TestOfStrategyArgument {RunId = testId, Pair = pair, TimeFrame = timeFrame, DateTimeRange = dateTimeRange });
        return result;
    }

    private readonly GrpcChannel _channel = channel.Channel;
}