﻿using Infrastructure;
using Infrastructure.Grpc;

namespace StrategyProcessor.Client;

public class StrategyProcessorClientParameters(IVault vault) : IParameters, IGrpcServiceParameters
{
    public string Address { get; } = vault.GetValue("StrategyProcessor:gRPC:Url");
}