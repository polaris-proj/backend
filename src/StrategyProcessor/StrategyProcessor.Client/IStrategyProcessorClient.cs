﻿using Domain;
using Infrastructure.Grpc;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Client;

public interface IStrategyProcessorClient
{
    Task<Answer<Guid>> StartStrategy(StrategyStartDto strategyStartDto);
    Task StopStrategy(StrategyStopDto strategyStopDto);
    Task<StrategyDto[]> GetAllStrategies();
    Task<TestOfStrategyDto> GetTestResult(Guid testId, Pair pair, TimeFrame timeFrame, DateTimeRange dateTimeRange);
}