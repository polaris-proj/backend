﻿using Dapper;
using Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Npgsql;

namespace StrategyProcessor.Integrations.Tests;

public abstract class BaseTests
{
    [SetUp]
    public virtual void Setup()
    {
        Scope = App.Services.CreateScope();
        //ClearAllTables();
        var migrations = Scope.ServiceProvider.GetServices<StartUpService>()
            .First(x => x is Infrastructure.DAL.Migrations);
        migrations.Start().GetAwaiter().GetResult();
    }

    [TearDown]
    public virtual void TearDown()
    {
        ClearAllTables();
        Scope.Dispose();
    }

    private void ClearAllTables()
    {
        var dataSource = Scope.ServiceProvider.GetRequiredService<NpgsqlDataSource>();
        var connection = dataSource.OpenConnection();
        connection.Execute("""select 'drop table \"' || tablename || '\" cascade;' from pg_tables;""");
    }

    [OneTimeTearDown]
    public virtual void OneTimeTearDown()
    {
        App.Dispose();
    }

    protected readonly IHost App = Program.SetUp([], false);
    protected IServiceScope Scope = null!;
}