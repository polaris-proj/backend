﻿// Global using directives

global using FluentAssertions;
global using Moq;
global using static Domain.TimeFrames;