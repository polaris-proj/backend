﻿using Domain.Connectors;
using StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Infrastructure.Connectors.ConnectorWithOrderLogger;

//Todo в тестах пишем в кэш всегда, при диспозе в бд. в реалтайме пишем в бд сразу
internal class BaseConnectorWithOrderLogger<TConnector> : BaseConnector<TConnector>
    where TConnector : IConnector
{
    public BaseConnectorWithOrderLogger(TConnector connector, IOrderRepository orderRepository, Guid runId) : base(connector)
    {
        _orderRepository = orderRepository;
        RunId = runId;
    }

    public async Task LogOrder(OrderDto order)
    {
        await _orderRepository.AddOrdersAsync(RunId, new List<OrderDto> {order});
    }

    public override async Task CancelOrder(Pair pair, OrderId orderId) //Todo точно все норм? закрытие это же ещё одна точка
    {
        await Connector.CancelOrder(pair, orderId);
        await _orderRepository.CloseOrderAsync(orderId);
    }


    private readonly IOrderRepository _orderRepository;
    protected readonly Guid RunId;
}