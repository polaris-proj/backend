﻿using Domain.Connectors;
using Infrastructure.EventDriven;
using OrderConfirmation.Client;
using StrategyProcessor.Infrastructure.Connectors.ConnectorWithOrderConfirmation;
using StrategyProcessor.Infrastructure.Connectors.ConnectorWithOrderLogger;
using StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;

namespace StrategyProcessor.Infrastructure.Connectors.Configurators;

internal static class ExchangeConnectorBuilder
{
    public static ISpotConnector WithEmulatingTrading(this ISpotConnector exchange, IDataSource inputDataSource, IEnumerable<Balance> initialBalance)
    {
        var virtualExchange = new VirtualExchange.VirtualExchange(exchange, inputDataSource);
        return virtualExchange.CreateSpotConnector(Guid.NewGuid().ToString(), initialBalance);
    }

    public static IFuturesConnector WithEmulatingTrading(this IFuturesConnector exchange, IDataSource inputDataSource, IEnumerable<Balance> initialBalance)
    {
        var virtualExchange = new VirtualExchange.VirtualExchange(exchange, inputDataSource);
        return virtualExchange.CreateFuturesConnector(Guid.NewGuid().ToString(), initialBalance);
    }

    public static TConnector WithEmulatingTrading<TConnector>(this TConnector exchange, IDataSource inputDataSource, IEnumerable<Balance> initialBalance)
    {
        return typeof(TConnector) switch
        {
            var t when t == typeof(ISpotConnector) => (TConnector) ((ISpotConnector) exchange!).WithEmulatingTrading(
                inputDataSource, initialBalance),
            var t when t == typeof(IFuturesConnector) => (TConnector) ((IFuturesConnector) exchange!)
                .WithEmulatingTrading(inputDataSource, initialBalance),
            _ => throw new NotSupportedException()
        };
    }

    public static ISpotConnector WithTradeLogging(this ISpotConnector connector,
        IOrderRepository orderRepository, Guid runId, bool isRealTime)
    {
        if (isRealTime)
            return new SpotConnectorWithOrderLogger(connector, orderRepository, runId);

        return new DeferredSpotConnectorWithOrderLogger(connector, orderRepository, runId);
    }

    public static IFuturesConnector WithTradeLogging(this IFuturesConnector connector,
        IOrderRepository orderRepository, Guid runId, bool isRealTime)
    {
        if (isRealTime)
            return new FuturesConnectorWithOrderLogger(connector, orderRepository, runId);

        return new DeferredFuturesConnectorWithOrderLogger(connector, orderRepository, runId);
    }

    public static TConnector WithTradeLogging<TConnector>(this TConnector connector,
        IOrderRepository orderRepository, Guid runId, bool isRealTime)
    {
        return typeof(TConnector) switch
        {
            var t when t == typeof(ISpotConnector) => (TConnector) ((ISpotConnector) connector!).WithTradeLogging(
                orderRepository, runId, isRealTime),
            var t when t == typeof(IFuturesConnector) => (TConnector) ((IFuturesConnector) connector!)
                .WithTradeLogging(orderRepository, runId, isRealTime),
            _ => throw new NotSupportedException()
        };
    }

    public static ISpotConnector WithOrderConfirmation(this ISpotConnector connector, IOrderSenderClient orderSenderClient)
    {
        return new SpotConnectorWithOrderConfirmation(connector, orderSenderClient);
    }

    public static IFuturesConnector WithOrderConfirmation(this IFuturesConnector connector, IOrderSenderClient orderSenderClient)
    {
        return new FuturesConnectorWithOrderConfirmation(connector, orderSenderClient);
    }

    public static TConnector WithOrderConfirmation<TConnector>(this TConnector connector, IOrderSenderClient orderSenderClient)
    {
        return typeof(TConnector) switch
        {
            var t when t == typeof(ISpotConnector) => (TConnector) ((ISpotConnector) connector!).WithOrderConfirmation(
                orderSenderClient),
            var t when t == typeof(IFuturesConnector) => (TConnector) ((IFuturesConnector) connector!)
                .WithOrderConfirmation(orderSenderClient),
            _ => throw new NotSupportedException()
        };
    }
}