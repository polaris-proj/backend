﻿using Domain.Connectors;
using Domain.StrategyProcessor;
using OrderConfirmation.Client;

namespace StrategyProcessor.Infrastructure.Connectors.ConnectorWithOrderConfirmation;

internal class SpotConnectorWithOrderConfirmation : BaseConnector<ISpotConnector>, ISpotConnector
{
    private readonly IOrderSenderClient _orderSenderClient;

    public SpotConnectorWithOrderConfirmation(ISpotConnector spotConnector,
        IOrderSenderClient orderConfirmationClient) : base(spotConnector)
    {
        _orderSenderClient = orderConfirmationClient;
    }

    public Task<OrderId> CreateMarketOrder(Pair pair, MarketOrder orderType, decimal amount)
    {
        return _orderSenderClient.CreateMarketOrder(pair, orderType, amount);
    }

    public Task<OrderId> CreateLimitOrder(Pair pair, LimitOrder orderType, PlacementOrder entry)
    {
        return _orderSenderClient.CreateLimitOrder(pair, orderType, entry);
    }
}