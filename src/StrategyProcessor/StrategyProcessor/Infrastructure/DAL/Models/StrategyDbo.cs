﻿using System.ComponentModel.DataAnnotations;
using Domain.StrategyProcessor;

namespace StrategyProcessor.Infrastructure.DAL.Models;

public class StrategyDbo
{
    public Guid Id { get; set; }
    [StringLength(200)] public required string InternalName { get; set; }
    public required StrategyConnectorType StrategyConnectorType { get; set; }
    public ICollection<ParameterDbo> Properties { get; set; } = new List<ParameterDbo>();
}