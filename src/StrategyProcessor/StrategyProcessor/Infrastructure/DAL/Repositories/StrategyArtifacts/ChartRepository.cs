﻿using System.Data;
using System.Linq;
using Dapper;
using Domain.StrategyProcessor;
using Infrastructure.Extensions;
using Npgsql;
using PostgreSQLCopyHelper;
using StrategyProcessor.Infrastructure.DAL.Models;
using StrategyProcessor.Infrastructure.Mappers;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;

public class ChartRepository : IChartRepository
{
    public ChartRepository(NpgsqlDataSource dataSource)
    {
        _dataSource = dataSource;

        _pointsDboCopyHelper = new PostgreSQLCopyHelper<PointDbo>("PointDbo")
            .UsePostgresQuoting()
            .MapBigInt("TimeStamp", x => x.TimeStamp)
            .MapNumeric("Value", x => x.Value)
            .MapBigInt("ChartDboId", x => (long) x.ChartDboId);
    }

    public async Task AddChartsAsync(Guid runId, IEnumerable<ChartDto> chartDtos)
    {
        await using var connection = _dataSource.CreateConnection();
        await connection.OpenAsync();

        foreach (var chart in chartDtos)
        {
            var dbChart = await ReadChartDboAsync(connection, runId, chart.Name, chart.Pair,
                chart.TimeFrame);

            if (dbChart is null)
            {
                var id = await WriteChartDboAsync(connection, runId, chart);

                await _pointsDboCopyHelper.SaveAllAsync(connection,
                    chart.Chart.Select(x => new PointDbo
                    {
                        TimeStamp = x.TimeStamp,
                        Value = x.Value,
                        ChartDboId = id,
                    }));
                continue;
            }

            dbChart.Chart = chart.Chart.Select(x => new PointDbo
            {
                TimeStamp = x.TimeStamp,
                Value = x.Value,
                ChartDboId = dbChart.Id,
            }).ToList();
            await _pointsDboCopyHelper.SaveAllAsync(connection, dbChart.Chart);
        }
    }

    public async Task AddDotToChartAsync(Guid runId, string name, string? description, Pair pair, TimeFrame timeFrame,
        Point point)
    {
        await using var connection = _dataSource.CreateConnection();
        await connection.OpenAsync();

        var dbChart = await ReadChartDboAsync(connection, runId, name, pair, timeFrame);
        var chartDboId = dbChart?.Id ?? 0;
        if (dbChart is null)
        {
            var chart = new ChartDto
            {
                Description = description,
                Name = name,
                Pair = pair,
                TimeFrame = timeFrame,
                Chart = new List<Point>()
            };
            chartDboId = await WriteChartDboAsync(connection, runId, chart);
        }

        await _pointsDboCopyHelper.SaveAllAsync(connection, new[]
        {
            new PointDbo
            {
                TimeStamp = point.TimeStamp,
                Value = point.Value,
                ChartDboId = chartDboId
            }
        });
    }

    public async Task<IEnumerable<ChartDto>> ReadAsync(Guid runId, Pair pair, TimeFrame timeFrame, DateTimeRange dateTimeRange)
    {
        await using var connection = _dataSource.CreateConnection();
        await connection.OpenAsync();

        const string sql = """
                           SELECT c."Id", c."Name", c."Description", c."Pair", c."TimeFrame", p."Value", p."TimeStamp"
                           FROM "ChartDbo" c
                           LEFT JOIN "PointDbo" p ON c."Id" = p."ChartDboId"
                           WHERE c."RunOfStrategyDboId" = @RunId
                             AND c."Pair" = @Pair 
                             AND c."TimeFrame" = @TimeFrame
                             AND p."TimeStamp" >= @TimeStampRangeBegin
                             AND p."TimeStamp" <= @TimeStampRangeEnd
                           """;

        var chartDictionary = new Dictionary<ulong, ChartDto>();

        var charts = await connection.QueryAsync<ChartDbo, PointDbo, ChartDto>(
            sql,
            (chart, point) =>
            {
                if (!chartDictionary.TryGetValue(chart.Id, out var chartEntry))
                {
                    chartEntry = chart.ToDto();
                    chartEntry.Chart = new List<Point>();
                    chartDictionary.Add(chart.Id, chartEntry);
                }

                if (point != null)
                {
                    chartEntry.Chart.Add(point.ToModel());
                }

                return chartEntry;
            },
            new
            {
                RunId = runId,
                Pair = pair.ToString(),
                TimeFrame = timeFrame,
                TimeStampRangeBegin = dateTimeRange.Begin.ToMilliseconds(),
                TimeStampRangeEnd = dateTimeRange.End.ToMilliseconds()
            },
            splitOn: "Value");

        return charts.DistinctBy(x => x.Name).ToList();
    }

    private async Task<ChartDbo?> ReadChartDboAsync(IDbConnection connection, Guid runId, string name, string pair,
        TimeFrame timeFrame)
    {
        const string sql = """
                           SELECT * FROM "ChartDbo"
                           WHERE "RunOfStrategyDboId" = @RunId AND "Pair" = @Pair AND "TimeFrame" = @TimeFrame AND "Name" = @Name
                           Limit 1
                           """;
        return await connection.QuerySingleOrDefaultAsync<ChartDbo>(sql,
            new {RunId = runId, Name = name, Pair = pair, TimeFrame = timeFrame});
    }

    private async Task<ulong> WriteChartDboAsync(IDbConnection connection, Guid runId, ChartDto chartDto)
    {
        const string sql = """
                           INSERT INTO "ChartDbo" ("Description", "Name", "Pair", "TimeFrame", "RunOfStrategyDboId")
                           VALUES (@Description, @Name, @Pair, @TimeFrame, @RunOfStrategyDboId)
                           RETURNING "Id"
                           """;
        return await connection.ExecuteScalarAsync<ulong>(sql, new
        {
            Description = chartDto.Description,
            Name = chartDto.Name,
            Pair = chartDto.Pair.ToString(),
            TimeFrame = chartDto.TimeFrame,
            RunOfStrategyDboId = runId
        });
    }

    private readonly NpgsqlDataSource _dataSource;
    private readonly PostgreSQLCopyHelper<PointDbo> _pointsDboCopyHelper;
}