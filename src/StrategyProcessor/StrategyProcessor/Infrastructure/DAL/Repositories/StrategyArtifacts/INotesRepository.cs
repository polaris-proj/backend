﻿using Domain.StrategyProcessor;

namespace StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;

public interface INotesRepository
{
    Task AddNotesAsync(Guid runId, IEnumerable<Note> noteDbos);
    Task<IEnumerable<Note>> ReadAsync(Guid runId, Pair pair, TimeFrame timeFrame, DateTimeRange dateTimeRange);
}