﻿using System.Linq;
using Dapper;
using Domain.StrategyProcessor;
using Infrastructure.Extensions;
using Npgsql;
using StrategyProcessor.Infrastructure.DAL.Models;
using PostgreSQLCopyHelper;
using StrategyProcessor.Infrastructure.Mappers;

namespace StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;

public class NotesRepository : INotesRepository
{
    public NotesRepository(NpgsqlDataSource dataSource)
    {
        _dataSource = dataSource;

        _notesDboCopyHelper = new PostgreSQLCopyHelper<NoteDbo>(nameof(NoteDbo))
            .UsePostgresQuoting()
            .MapText(nameof(NoteDbo.Pair), x => x.Pair)
            .MapInteger(nameof(NoteDbo.TimeFrame), x => (int) x.TimeFrame)
            .MapBigInt(nameof(NoteDbo.TimeStamp), x => x.TimeStamp)
            .MapNumeric(nameof(NoteDbo.Price), x => x.Price)
            .MapText(nameof(NoteDbo.Text), x => x.Text)
            .MapInteger(nameof(NoteDbo.Icon), x => (int) x.Icon)
            .MapUUID(nameof(NoteDbo.RunOfStrategyId), x => x.RunOfStrategyId);
        SqlMapper.AddTypeHandler(new PairTypeHandler());
    }

    public async Task AddNotesAsync(Guid runId, IEnumerable<Note> noteDbos)
    {
        await using var connection = _dataSource.CreateConnection();
        await connection.OpenAsync();
        await _notesDboCopyHelper.SaveAllAsync(connection, noteDbos.Select(x => x.ToDbo(runId)));
    }

    public async Task<IEnumerable<Note>> ReadAsync(Guid runId, Pair pair, TimeFrame timeFrame, DateTimeRange dateTimeRange)
    {
        const string sql = """
                           SELECT "Pair", "TimeFrame", "TimeStamp", "Price", "Text", "Icon"
                           FROM "NoteDbo"
                           WHERE "RunOfStrategyId" = @RunId
                             AND "Pair" = @Pair
                             AND "TimeFrame" = @TimeFrame
                             AND "TimeStamp" >= @TimeStampRangeBegin
                             AND "TimeStamp" <= @TimeStampRangeEnd
                           """;

        await using var connection = _dataSource.CreateConnection();
        await connection.OpenAsync();

        var result = await connection.QueryAsync<Note>(
            sql,
            new
            {
                RunId = runId, Pair = pair.ToString(),
                TimeFrame = (int)timeFrame,
                TimeStampRangeBegin = dateTimeRange.Begin.ToMilliseconds(),
                TimeStampRangeEnd = dateTimeRange.End.ToMilliseconds()
            }
        );

        return result;
    }

    private readonly PostgreSQLCopyHelper<NoteDbo> _notesDboCopyHelper;
    private readonly NpgsqlDataSource _dataSource;
}