﻿using System.Linq;
using Dapper;
using Domain.Connectors;
using Npgsql;
using StrategyProcessor.Infrastructure.DAL.Models;
using PostgreSQLCopyHelper;
using StrategyProcessor.Infrastructure.Mappers;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;

public class OrderRepository : IOrderRepository
{
    public OrderRepository(NpgsqlDataSource dataSource)
    {
        _dataSource = dataSource;
        _orderDboCopyHelper = new PostgreSQLCopyHelper<OrderDbo>(nameof(OrderDbo))
            .UsePostgresQuoting()
            .MapText(nameof(OrderDbo.Pair), x => x.Pair)
            .MapInteger(nameof(OrderDbo.Type), x => (int) x.Type)
            .MapInteger(nameof(OrderDbo.Status), x => (int) x.Status)
            .MapNumeric(nameof(OrderDbo.Price), x => x.Price)
            .MapNumeric(nameof(OrderDbo.Amount), x => x.Amount)
            .MapInteger(nameof(OrderDbo.Leverage), x => x.Leverage)
            .MapNumeric(nameof(OrderDbo.TakePrice), x => x.TakePrice)
            .MapNumeric(nameof(OrderDbo.StopPrice), x => x.StopPrice)
            .MapText(nameof(OrderDbo.OrderId), x => x.OrderId)
            .MapTimeStampTz(nameof(OrderDbo.CreatedAt), x => x.CreatedAt)
            .MapTimeStampTz(nameof(OrderDbo.UpdatedAt), x => x.UpdatedAt)
            .MapUUID(nameof(OrderDbo.RunOfStrategyId), x => x.RunOfStrategyId);
        SqlMapper.AddTypeHandler(new PairTypeHandler());
    }

    public async Task AddOrdersAsync(Guid runId, IEnumerable<OrderDto> orders)
    {
        await using var connection = _dataSource.CreateConnection();
        await connection.OpenAsync();
        await _orderDboCopyHelper.SaveAllAsync(connection, orders.Select(x => x.ToDbo(runId)));
    }

    public async Task CloseOrderAsync(OrderId orderId)
    {
        await using var connection = _dataSource.CreateConnection();
        await connection.OpenAsync();

        const string sql = """
                           UPDATE "OrderDbo"
                           SET "Status" = @Status
                           WHERE "OrderId" = @OrderId
                           """;

        var updatedRows = await connection.ExecuteAsync(sql, new {Status = OrderStatus.Close, OrderId = orderId.Id});

        if (updatedRows == 0)
        {
            throw new ArgumentNullException($"Order with id: {orderId} not found");
        }

        if (updatedRows > 1)
        {
            throw new Exception("More than one order with the same id");
        }
    }


    public async Task<IEnumerable<OrderDto>> ReadAsync(Guid runId, Pair pair, TimeFrame timeFrame, DateTimeRange dateTimeRange)
    {
        await using var connection = _dataSource.CreateConnection();
        await connection.OpenAsync();

        const string sql = """
                           SELECT *
                           FROM "OrderDbo"
                           WHERE "RunOfStrategyId" = @RunId
                             AND "Pair" = @Pair
                             AND "CreatedAt" >= @DateTimeRangeBegin
                             AND "CreatedAt" <= @DateTimeRangeEnd
                           """; //todo add timeFrame

        return await connection.QueryAsync<OrderDto>(
            sql, new
            {
                RunId = runId,
                Pair = pair,
                DateTimeRangeBegin = dateTimeRange.Begin,
                DateTimeRangeEnd = dateTimeRange.End
            }
        );
    }

    private readonly PostgreSQLCopyHelper<OrderDbo> _orderDboCopyHelper;
    private readonly NpgsqlDataSource _dataSource;
}