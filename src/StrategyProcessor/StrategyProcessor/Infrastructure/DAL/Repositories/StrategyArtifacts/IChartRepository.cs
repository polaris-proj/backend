﻿using Domain.StrategyProcessor;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;

public interface IChartRepository
{
    Task AddChartsAsync(Guid runId, IEnumerable<ChartDto> chartDbos);
    Task AddDotToChartAsync(Guid runId, string name, string? description, Pair pair, TimeFrame timeFrame, Point point);
    Task<IEnumerable<ChartDto>> ReadAsync(Guid runId, Pair pair, TimeFrame timeFrame, DateTimeRange dateTimeRange);
}