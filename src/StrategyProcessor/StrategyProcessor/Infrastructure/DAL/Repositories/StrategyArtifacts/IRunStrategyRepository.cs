﻿using Domain.StrategyProcessor;
using StrategyProcessor.Infrastructure.DAL.Models;

namespace StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;

public interface IRunStrategyRepository
{
    Task<Guid> CreateAsync(RunOfStrategyDbo item);
    Task<RunOfStrategyDbo?> ReadAsync(Guid runId);
    Task UpdateStatusAsync(Guid id, StatusOfRun statusOfRun);
}