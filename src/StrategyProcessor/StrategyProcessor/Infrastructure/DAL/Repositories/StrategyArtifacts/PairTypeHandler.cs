﻿using System.Data;
using Dapper;

namespace StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;

internal class PairTypeHandler : SqlMapper.TypeHandler<Pair>
{
    public override void SetValue(IDbDataParameter parameter, Pair value)
    {
        parameter.Value = value.ToString();
    }

    public override Pair Parse(object value)
    {
        return new Pair((string) value);
    }
}