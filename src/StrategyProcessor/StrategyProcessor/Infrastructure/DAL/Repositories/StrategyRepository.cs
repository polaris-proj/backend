﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using StrategyProcessor.Infrastructure.DAL.Models;

namespace StrategyProcessor.Infrastructure.DAL.Repositories;

public class StrategyRepository(StrategyProcessorContext context) : IStrategyRepository
{
    public async Task<StrategyDbo?> ReadByIdAsync(Guid id)
    {
        return await context.Set<StrategyDbo>().Include(x => x.Properties)
            .FirstOrDefaultAsync(x => x.Id == id);
    }

    public async Task<StrategyDbo?> ReadByNameAsync(string name)
    {
        return await context.Set<StrategyDbo>()
            .Include(x => x.Properties)
            .Where(x => x.InternalName == name)
            .FirstOrDefaultAsync();
    }

    public void Update(StrategyDbo item)
    {
        context.Set<StrategyDbo>().Update(item);
    }

    public Task SaveAsync()
    {
        return context.SaveChangesAsync();
    }

    public async Task<Guid> CreateAsync(StrategyDbo item)
    {
        await context.Set<StrategyDbo>().AddAsync(item);
        return item.Id;
    }

    public async Task<List<StrategyDbo>> ReadAllAsync()
    {
        return await context.Set<StrategyDbo>()
            .Include(x => x.Properties)
            .ToListAsync();
    }
}