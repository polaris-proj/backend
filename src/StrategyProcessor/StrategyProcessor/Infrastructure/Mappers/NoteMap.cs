﻿using Domain.StrategyProcessor;
using StrategyProcessor.Infrastructure.DAL.Models;

namespace StrategyProcessor.Infrastructure.Mappers;

public static class NoteMap
{
    public static Note ToModel(this NoteDbo noteDbo)
    {
        return new Note
        {
            TimeStamp = noteDbo.TimeStamp,
            Price = noteDbo.Price,
            Icon = noteDbo.Icon,
            Text = noteDbo.Text,
            Pair = new Pair(noteDbo.Pair),
            TimeFrame = noteDbo.TimeFrame
        };
    }
    
    public static NoteDbo ToDbo(this Note note, Guid runId)
    {
        return new NoteDbo
        {
            Pair = note.Pair.ToString(),
            TimeFrame = note.TimeFrame,
            TimeStamp = note.TimeStamp,
            Price = note.Price,
            Text = note.Text,
            Icon = note.Icon,
            RunOfStrategyId = runId
        };
    }
}