﻿using System.Linq;
using StrategyProcessor.Infrastructure.DAL.Models;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Infrastructure.Mappers;

internal static class StrategyMap
{
    public static StrategyDto ToDto(this StrategyDbo strategyDbo)
    {
        return new StrategyDto
        {
            Id = strategyDbo.Id,
            InternalName = strategyDbo.InternalName,
            StrategyConnectorType = strategyDbo.StrategyConnectorType,
            Parameters = strategyDbo.Properties.Select(x => x.ToDto()).ToArray()
        };
    }
}