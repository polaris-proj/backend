﻿using System.Linq;
using StrategyProcessor.Infrastructure.DAL.Models;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Infrastructure.Mappers;

public static class TestOfStrategyMap
{
    public static TestOfStrategyDto ToDto(this RunOfStrategyDbo parameterDbo)
    {
        return new TestOfStrategyDto
        {
            Notes = parameterDbo.Notes.Select(x => x.ToModel()).ToList(),
            Orders = parameterDbo.Orders.Select(x => x.ToDto()).ToList(),
            Charts = parameterDbo.Charts.Select(x => x.ToDto()).ToList(),
        };
    }
}