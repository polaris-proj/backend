﻿using Grpc.Net.Client;
using OrderConfirmation.Client;

namespace StrategyProcessor.Services;

public class OrderSenderClientFactory : IOrderSenderClientFactory
{
    public OrderSenderClientFactory(OrderSenderClientParameters clientParameters)
    {
        _clientParameters = clientParameters;
    }

    public OrderSenderClient Create(Guid userId, Exchange exchange, Guid runId, GrpcChannel? channel)
    {
        return new OrderSenderClient(userId, exchange, runId, _clientParameters, channel);
    }

    private readonly OrderSenderClientParameters _clientParameters;
}