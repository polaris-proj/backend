﻿using Infrastructure;
using StrategyProcessor.Services.Consumers;

namespace StrategyProcessor.Services;

internal class StartConsumers(CandleHandler _, NewPatternHandler __) : StartUpService
{
    public override Task Start() => Task.CompletedTask;
}