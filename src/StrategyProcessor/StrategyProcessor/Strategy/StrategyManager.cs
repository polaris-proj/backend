﻿using System.Linq;
using System.Reflection;
using Domain.Connectors;
using Domain.Patterns;
using Domain.Patterns.Objects;
using Domain.StrategyProcessor;

namespace StrategyProcessor.Strategy;

internal class StrategyManager<TStrategy, TConnector> : IStrategyManager
    where TStrategy : BaseStrategy<TConnector>, new()
    where TConnector : IConnector
{
    public Guid RunId { get; }

    public StrategyManager(StrategyManagerParameters<TConnector> p)
    {
        var (parameters, allowedSources, exchangesConnectors) = (p.Parameters, p.AllowedSources, p.ExchangesConnectors);
        var strategyType = typeof(TStrategy);

        _editableProperties = p.DarkMagic.GetEditableProps(strategyType);
        if (_editableProperties.Count != parameters.Count)
        {
            throw new ArgumentException("Length of parameters < length of class parameters");
        }

        if (_editableProperties.Select(x => x.Key)
                .Intersect(parameters.Select(x => x.Name))
                .Count() != parameters.Count
            )
        {
            throw new ArgumentException("Parameters do not match the strategy");
        }

        _parameters = parameters.ToDictionary(x => x.Name, x => x);

        var uniqueExchanges = allowedSources.Select(x => x.Exchange).Distinct().ToList();
        if (uniqueExchanges.Intersect(exchangesConnectors.Keys)
                .Count() != uniqueExchanges.Count
            )
        {
            throw new ArgumentException("Not enough connectors:");
        }

        _exchangesConnectors = exchangesConnectors;
        AllowedSources = allowedSources.ToHashSet();
        _chartDataStorage = p.ChartDataStorage;
        RunId = p.RunId;
        State = new StrategyManagerState<TStrategy>();

        _lifeTime = GetCopyOfStrategy(uniqueExchanges.First()).LifeTime;
        Console.WriteLine($"Запускаю {strategyType.Name}");
    }

    public void SetState(StrategyManagerState<TStrategy> state)
    {
        State = state;
    }

    public async Task HandleCandleEvent(NewCandleEvent candleEvent)
    {
        await SendEvent((strategy, @event) => strategy.EventsCatalog_NewCandle(@event), candleEvent);
    }

    public async Task HandlePatternEvent(NewPatternEvent patternEvent)
    {
        await SendEvent((strategy, @event) => strategy.EventsCatalog_NewPattern(@event), patternEvent);
    }

    protected virtual async Task SendEvent<T>(Func<BaseStrategy<TConnector>, T, Task> send, T arg)
        where T : ITradeEvent
    {
        if (!AllowedSources.Contains(arg.ToExchangePairTimeframe()))
            return; //todo если перестроить систему распространения эвентов, то не придется заниматься вот этим на уровне менеджера стратегии

        await SendEventToStrategiesInMemory(send, arg);
    }

    protected TStrategy GetCopyOfStrategy(Exchange exchange)
    {
        var newStrategy = new TStrategy();
        UpdateDeps(newStrategy, exchange);

        foreach (var param in _parameters.Keys)
        {
            _editableProperties[param].SetValue(newStrategy, _parameters[param].Value);
        }

        newStrategy.Start();
        return newStrategy;
    }

    protected void UpdateDeps(TStrategy strategy, Exchange exchange)
    {
        strategy.ChartDataStorage = _chartDataStorage;
        strategy.Connector = _exchangesConnectors[exchange];
    }

    protected virtual async Task SendEventToStrategiesInMemory<T>(Func<BaseStrategy<TConnector>, T, Task> send, T arg)
        where T : ITradeEvent
    {
        switch (_lifeTime)
        {
            case LifeTime.SingleInstance:
            {
                await SendEventForSingleInstance(send, arg);
                break;
            }
            case LifeTime.MultipleInstancesControlledByStatus:
            {
                await SendEventForMultipleInstancesControlledByStatus(send, arg);
                break;
            }
            case LifeTime.SingleInstancePerExchangePairTimeframe:
            {
                await SendEventForSingleInstancePerExchangePairTimeframe(send, arg);
                break;
            }
            default:
                throw new ArgumentOutOfRangeException(_lifeTime.ToString());
        }
    }

    private async Task SendEventForMultipleInstancesControlledByStatus<T>(
        Func<BaseStrategy<TConnector>, T, Task> send,
        T arg
        )
        where T : ITradeEvent
    {
        if (!State.ActiveStrategiesByExchange.ContainsKey(arg.Exchange))
        {
            State.ActiveStrategiesByExchange[arg.Exchange] = [];
        }

        var currentStrategies = State.ActiveStrategiesByExchange[arg.Exchange];
        
        var newStrategy = GetCopyOfStrategy(arg.Exchange);
        var sendEventToNewStrategy = send(newStrategy, arg);

        var tasks = new List<Task>();

        foreach (var strategy in currentStrategies.ToList())
        {
            if (strategy.Status == StrategyStatus.Opened)
            {
                tasks.Add(send(strategy, arg));
            }
            else if (strategy.Status == StrategyStatus.Closed)
            {
                currentStrategies.Remove(strategy);
            }
        }

        await sendEventToNewStrategy;
        if (newStrategy.Status == StrategyStatus.Opened)
        {
            currentStrategies.Add(newStrategy);
        }

        await Task.WhenAll(tasks);
    }

    private async Task SendEventForSingleInstance<T>(
        Func<BaseStrategy<TConnector>, T, Task> send,
        T arg
        )
        where T : ITradeEvent
    {
        if (!State.ActiveStrategiesByExchange.ContainsKey(arg.Exchange))
        {
            State.ActiveStrategiesByExchange[arg.Exchange] = [];
        }

        var currentStrategies = State.ActiveStrategiesByExchange[arg.Exchange];
        if (currentStrategies.Count == 0)
        {
            var newStrategy1 = GetCopyOfStrategy(arg.Exchange);
            currentStrategies.Add(newStrategy1);
        }

        await send(currentStrategies[0], arg);
    }
    
    private async Task SendEventForSingleInstancePerExchangePairTimeframe<T>(
        Func<BaseStrategy<TConnector>, T, Task> send,
        T arg
    )
        where T : ITradeEvent
    {
        var key = arg.ToExchangePairTimeframe();
        if (!State.ActiveStrategiesByExchangePairTimeFrame.ContainsKey(key))
        {
            State.ActiveStrategiesByExchangePairTimeFrame[key] = [];
        }

        var currentStrategies = State.ActiveStrategiesByExchangePairTimeFrame[key];
        if (currentStrategies.Count == 0)
        {
            var newStrategy1 = GetCopyOfStrategy(arg.Exchange);
            currentStrategies.Add(newStrategy1);
        }

        await send(currentStrategies[0], arg);
    }

    public async ValueTask DisposeAsync()
    {
        //unsubscribe from events
        var artifactStorageTask = _chartDataStorage.DisposeAsync();
        var connectorTasks = _exchangesConnectors.Select(x => x.Value.DisposeAsync().AsTask()).ToList();

        await artifactStorageTask;
        await Task.WhenAll(connectorTasks);
    }


    protected StrategyManagerState<TStrategy> State { get; set; }
    protected HashSet<ExchangePairTimeFrame> AllowedSources { get; }
    private readonly IChartDataStorage _chartDataStorage;
    private readonly Dictionary<Exchange, TConnector> _exchangesConnectors;
    private readonly Dictionary<string, StrategyParameter> _parameters;
    private readonly Dictionary<string, PropertyInfo> _editableProperties;
    private readonly LifeTime _lifeTime;
}