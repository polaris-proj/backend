﻿using Domain.StrategyProcessor;
using StrategyProcessor.Infrastructure;
using StrategyProcessor.Infrastructure.DAL.Repositories.RestoreAfterFailure;

namespace StrategyProcessor.Strategy;

internal class StrategyManagerWithStoreInDbParameters<TConnector> : StrategyManagerParameters<TConnector>
{
    internal StrategyManagerWithStoreInDbParameters(
        Guid runId,
        IReadOnlyList<StrategyParameter> parameters,
        IReadOnlyList<ExchangePairTimeFrame> allowedSources,
        Dictionary<Exchange, TConnector> exchangesConnectors,
        IChartDataStorage chartDataStorage,
        IStrategyManagerRepository strategyManagerRepository,
        IDarkMagic darkMagic
        )
        : base(runId, parameters, allowedSources, exchangesConnectors, chartDataStorage, darkMagic)
    {
        StrategyManagerRepository = strategyManagerRepository;
    }

    public IStrategyManagerRepository StrategyManagerRepository { get; }
}