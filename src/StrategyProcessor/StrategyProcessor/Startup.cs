﻿using EventStore.Client;
using Infrastructure;
using Infrastructure.DAL.Dapper;
using Infrastructure.EventDriven;
using Infrastructure.Grpc;
using Infrastructure.Json;
using Infrastructure.Metric;
using Infrastructure.Metric.Grpc;
using Infrastructure.RTQConnector;
using IODataModule.Client;
using IODataModule.Client.Clients;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using OrderConfirmation.Client;
using Prometheus;
using ProtoBuf.Grpc.Server;
using StrategyProcessor.Controllers;
using StrategyProcessor.Infrastructure;
using StrategyProcessor.Infrastructure.Connectors.Configurators;
using StrategyProcessor.Infrastructure.DAL;
using StrategyProcessor.Infrastructure.DAL.Repositories;
using StrategyProcessor.Infrastructure.DAL.Repositories.RestoreAfterFailure;
using StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;
using StrategyProcessor.Services;
using StrategyProcessor.Services.Consumers;
using StrategyProcessor.Shared;
using StrategyProcessor.Strategies;

namespace StrategyProcessor;

public static class Startup
{
    public static void ConfigureServices(this IServiceCollection services, IWebHostEnvironment env)
    {
        services.AddHealthChecks();
        services
            .AddSingleton<IVault, Vault>()
            .AddSingleton(_ => JsonOptions.JsonSerializerOptions)
            .Scan(i =>
            {
                i.FromCallingAssembly()
                    .AddClasses(cl => cl.AssignableTo<StartUpService>())
                    .AsSelf().As<StartUpService>()
                    .WithScopedLifetime();

                i.FromAssembliesOf(typeof(CandleService), typeof(OrderSenderClient), typeof(InfrastructureAssembly), typeof(EventStoreClient))
                    .AddClasses(cl => cl.AssignableTo<IParameters>())
                    .AsSelf()
                    .WithSingletonLifetime();
            })
            .AddSingleton<IDarkMagic, DarkMagic>(_ => new DarkMagic(typeof(StrategyProcessorStrategyAssembly).Assembly)) //pointer on assembly with strategies

            #region ConfigureKafka

            .AddSingleton<KafkaParameters>()
            .AddSingleton<CandleHandler>()
            .AddSingleton<NewPatternHandler>()

            #endregion

            #region ConfigureDb

            .ConfigureNpgSql(true)
            .AddDbContextFactory<StrategyProcessorContext>()
            .AddDbContext<StrategyProcessorContext>(ServiceLifetime.Scoped)
            //.AddFactory<>()

            #endregion

            #region ConfigureEvents

            .AddSingleton<RealTimeDataSource>()

            #endregion
            .AddScoped<IEventStoreClient, EventStoreClient>()
            .AddSingleton<TradingStrategyManagerStorage>()
            .AddSingleton<TestingStrategyManagerStorage>()
            .AddSingleton<IStrategyManagerRepository, StrategyManagerRepository>()
            .AddScoped<IStrategyRepository, StrategyRepository>()
            .AddScoped<IStartStopStrategyServiceTypedFactory, StartStopStrategyServiceTypedFactory>()
            .AddSingleton<IStrategyManagerFactory, StrategyManagerFactory>()
            .AddSingleton<IOrderSenderClientFactory, OrderSenderClientFactory>()
            .AddScoped(typeof(StrategyService<,>))
            .AddSingleton<IRemoteConnectorFactory, RemoteConnectorFactory>()

            #region Grpc

            .AddCodeFirstGrpc(options =>
            {
                options.EnableDetailedErrors = true;
                options.MaxReceiveMessageSize = 2000 * 1024 * 1024; // 2 MB
                options.MaxSendMessageSize = 2000 * 1024 * 1024; // 5 MB
                options.Interceptors.Add<GrpcErrorInterceptor>();
            });
        
        services.ConfigureGrpcChannel<IoDataModuleClientChannel, IoDataModuleGrpcServiceParameters>(env);
        services.ConfigureGrpcChannel<EventStoreClientChannel, EventStoreClientParameters>(env);
        #endregion

        var port = services.BuildServiceProvider().GetRequiredService<MetricSettings>().Port;
        services.AddMetricServer(x => x.Port = port);
    }

    public static void ConfigureForTest(this IServiceCollection services, bool isIndependentMode)
    {
        //  if (isIndependentMode)
        //  {
        services.AddScoped<IStrategyServiceGrpc, StrategyServiceGrpc>();
        //}
        //else
        //{
        services.AddScoped<IStrategyRepository, StrategyRepository>();
        services.AddScoped<IRunStrategyRepository, RunStrategyRepository>();
        services.AddScoped<IChartRepository, ChartRepository>();
        services.AddScoped<INotesRepository, NotesRepository>();
        services.AddScoped<IOrderRepository, OrderRepository>();
        services.AddScoped<ICandleService, CandleService>();
        //   }
    }

    public static void ConfigureApp(this IApplicationBuilder app)
    {
        app.UseRouting();

        app.UseGrpcMetrics();
        app.UseHttpMetrics();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapGrpcService<StrategyServiceGrpc>();
            endpoints.MapGrpcService<GetStrategiesService>();
            endpoints.MapGrpcService<RunOfStrategyService>();
            endpoints.MapHealthChecks("health");
        });
    }
}