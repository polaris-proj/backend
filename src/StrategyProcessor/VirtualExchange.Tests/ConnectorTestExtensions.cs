﻿using System.Linq;

namespace VirtualExchange.Tests;

internal static class ConnectorTestExtensions
{
    internal static Balance GetBalance(this VirtualExchangeConnector connector, Ticker ticker)
        => connector.GetCoinsAmount().GetAwaiter().GetResult().FirstOrDefault(x => x.Asset == ticker, new Balance(ticker, 0, 0));
}