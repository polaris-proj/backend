using System.Collections.Generic;
using System.Threading.Tasks;
using Domain;
using Domain.StrategyProcessor;
using FluentAssertions;
using Infrastructure.FluentAssertions;

namespace VirtualExchange.Tests
{
    [TestFixture]
    internal class FuturesOrderShortTest : BaseOrderTest
    {


        [Test]
        public async Task JustShortSomeCoinsAndCloseByTake()
        {
            var startBalance = 2000m;
            var leverage = 1;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

            var entry = new List<Order> {new Order(10, 100)};
            var take = new List<Order> {new Order(8, 100)};
            var orderId = await connector.CreateFuturesOrder(tickerName, OrderType.Short, leverage, entry, take);
            var priceGoals = new List<decimal> {9, 10, 8};
            await MovePrice(priceGoals, tickerName);

            var orderInfo = await connector.GetOrderInfo(tickerName, orderId);
            orderInfo.OrderStatus.Should().Be(OrderStatus.Close);
            connector.GetBalance(secondTicker).Available.Should().BeInRadius(startBalance + 2 * 100,0.5m);
        }

        [Test]
        public async Task BuySomeCoinsWhenNotEnoughMoneys()
        {
            var startBalance = 20m;
            var leverage = 1;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

            var buy = new List<Order> {new Order(10, 100)};
            Assert.Catch(
                delegate { connector.CreateFuturesOrder(tickerName, OrderType.Short, leverage, buy); });
            var priceGoals = new List<decimal> {9m, 10m, 11m, 8m};
            await MovePrice(priceGoals, tickerName);
        }

        [Test]
        public async Task ShortSomeCoinsWhenPriceHasNotReached()
        {
            var startBalance = 2000m;
            var leverage = 1;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

            var buy = new List<Order> {new Order(10, 100)};
            var orderId = await connector.CreateFuturesOrder(tickerName, OrderType.Short, leverage, buy);
            var priceGoals = new List<decimal> {9m, 9.9999m};
            await MovePrice(priceGoals, tickerName);

            var orderInfo = await connector.GetOrderInfo(tickerName, orderId);
            orderInfo.OrderStatus.Should().Be(OrderStatus.Open);
            orderInfo.FilledAmount.Should().Be(0);
            connector.GetBalance(secondTicker).Available.Should().BeInRadius(startBalance - 10 * 100,0.5m);
        }

        [Test]
        public async Task ShortSomeCoinsAndCloseAllByTakes()
        {
            var startBalance = 2000m;
            var leverage = 1;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

            var entry = new List<Order> {new Order(15, 100)};
            var take = new List<Order> {new Order(12, 25), new Order(10, 75),};
            var orderId = await connector.CreateFuturesOrder(tickerName, OrderType.Short, leverage, entry, take);
            var priceGoals = new List<decimal> {13, 15, 11, 9};
            await MovePrice(priceGoals, tickerName);
            var orderInfo = await connector.GetOrderInfo(tickerName, orderId);
            orderInfo.OrderStatus.Should().Be(OrderStatus.Close);
            connector.GetBalance(secondTicker).Available.Should().BeInRadius(startBalance + 3 * 25 + 5 * 75,0.5m);
        }

        [Test]
        public async Task ShortSomeCoinsAndCloseAllByStop()
        {
            var startBalance = 2000m;
            var leverage = 1;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

            var buy = new List<Order> {new Order(10, 100)};
            var stop = new List<Order> {new Order(11, 100)};
            var orderId = await connector.CreateFuturesOrder(tickerName, OrderType.Short, leverage, buy, null, stop);
            var priceGoals = new List<decimal> {9m, 10m, 11m};
            await MovePrice(priceGoals, tickerName);
            var orderInfo = await connector.GetOrderInfo(tickerName, orderId);
            orderInfo.OrderStatus.Should().Be(OrderStatus.Close);
            connector.GetBalance(secondTicker).Available.Should().BeInRadius(startBalance - 100,0.5m);
        }

        [Test]
        public async Task ShortSomeCoinsAndClosePartAndPartByTakeByStop()
        {
            var startBalance = 2000m;
            var leverage = 1;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

            var entry = new List<Order> {new Order(10, 100)};
            var stop = new List<Order> {new Order(11, 50)};
            var take = new List<Order> {new Order(7, 50)};
            var orderId = await connector.CreateFuturesOrder(tickerName, OrderType.Short, leverage, entry, take, stop);
            var priceGoals = new List<decimal> {9m, 10.5m, 11m, 7m, 6m, 3m, 1m};
            await MovePrice(priceGoals, tickerName);
            var orderInfo = await connector.GetOrderInfo(tickerName, orderId);
            orderInfo.OrderStatus.Should().Be(OrderStatus.Close);
           // orderInfo.FilledAmount.Should().Be(0);
            connector.GetBalance(secondTicker).Available.Should().BeInRadius(startBalance - 50 + 3 * 50,0.5m);
        }

        [Test]
        public async Task BuySomeCoinsWhenNotEnoughMoneysWithLeverage()
        {
            var startBalance = 20m;
            var leverage = 10;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

            var buy = new List<Order> {new Order(10, 100)};
            Assert.Catch(
                delegate { connector.CreateFuturesOrder(tickerName, OrderType.Short, leverage, buy); });
            var priceGoals = new List<decimal> {9m, 10m, 11m, 8m};
            await MovePrice(priceGoals, tickerName);
        }

        public async Task BuySomeCoinsWithLeverageLessWhen1()
        {
            var startBalance = 20m;
            var leverage = 0;
            var firstTicker = "шоколадные";
            var secondTicker = "монетки";
            var tickerName = new Pair(firstTicker, secondTicker);
            var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

            var buy = new List<Order> {new Order(10, 100)};
            Assert.Catch(() => connector.CreateFuturesOrder(tickerName, OrderType.Short, leverage, buy));
            var priceGoals = new List<decimal> {9m, 10m, 11m, 8m};
            await MovePrice(priceGoals, tickerName);
        }


        [Test]
        public async Task ShortSomeCoinsAndCloseAllByTakesWithLeverage()
        {
            for (int i = 1; i < 10; i++)
            {
                var startBalance = 150 * (11 - i)+100;
                var leverage = i;
                var firstTicker = "шоколадные";
                var secondTicker = "монетки";
                var tickerName = new Pair(firstTicker, secondTicker);
                var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);
                var entry = new List<Order> {new Order(15, 100)};
                var take = new List<Order> {new Order(12, 25), new Order(10, 75),};
                var orderId = await connector.CreateFuturesOrder(tickerName, OrderType.Short, leverage, entry, take);

                connector.GetBalance(secondTicker).Available.Should().BeInRadius(startBalance - 15m * 100 / leverage,0.5m);
                var priceGoals = new List<decimal> {13, 15, 11, 9};
                await MovePrice(priceGoals, tickerName);
                var orderInfo = await connector.GetOrderInfo(tickerName, orderId);
                orderInfo.OrderStatus.Should().Be(OrderStatus.Close);
                connector.GetBalance(secondTicker).Available.Should().BeInRadius(startBalance + 3 * 25 + 5 * 75,0.5m);
            }
        }

        [Test]
        public async Task ShortSomeCoinsAndCloseAllByStopWithLeverage()
        {
            for (int i = 1; i < 10; i++)
            {
                var startBalance = 2000m;
                var leverage = i;
                var firstTicker = "шоколадные";
                var secondTicker = "монетки";
                var tickerName = new Pair(firstTicker, secondTicker);
                var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

                var buy = new List<Order> {new Order(10, 100)};
                var stop = new List<Order> {new Order(11, 100)};
                var orderId =
                    await connector.CreateFuturesOrder(tickerName, OrderType.Short, leverage, buy, null, stop);
                connector.GetBalance(secondTicker).Available.Should().BeInRadius(startBalance - 10 * 100m / leverage,0.5m);
                var priceGoals = new List<decimal> {9m, 10m, 11m};
                await MovePrice(priceGoals, tickerName);

                var orderInfo = await connector.GetOrderInfo(tickerName, orderId);
                orderInfo.OrderStatus.Should().Be(OrderStatus.Close);
                connector.GetBalance(secondTicker).Available.Should().BeInRadius(startBalance - 100,0.5m);
            }
        }

        [Test]
        public async Task ShortSomeCoinsAndClosePartAndPartByTakeByStopWithLeverage()
        {
            for (int i = 1; i < 10; i++)
            {
                var startBalance = 2000m;
                var leverage = i;
                var firstTicker = "шоколадные";
                var secondTicker = "монетки";
                var tickerName = new Pair(firstTicker, secondTicker);
                var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

                var entry = new List<Order> {new Order(10, 100)};
                var stop = new List<Order> {new Order(11, 50)};
                var take = new List<Order> {new Order(7, 50)};
                var orderId =
                    await connector.CreateFuturesOrder(tickerName, OrderType.Short, leverage, entry, take, stop);
                connector.GetBalance(secondTicker).Available.Should().BeInRadius(startBalance - 10 * 100m / leverage,1);
                var priceGoals = new List<decimal> {9m, 10.5m, 11m, 7m, 6m, 3m, 1m, 1000m};
                await MovePrice(priceGoals, tickerName);
                var orderInfo = await connector.GetOrderInfo(tickerName, orderId);
                orderInfo.OrderStatus.Should().Be(OrderStatus.Close);
               // orderInfo.FilledAmount.Should().Be(0);
                connector.GetBalance(secondTicker).Available.Should().BeInRadius(startBalance - 50 + 3 * 50,1m);
                connector.GetBalance(firstTicker).Available.Should().Be(0);
            }
        }

        [Test]
        public async Task ShortSomeCoinsAndGetLiquidationWithLeverage()
        {
            for (var i = 1; i < 10; i++)
            {
                var startBalance = 2000m;
                var leverage = i;
                var firstTicker = "шоколадные";
                var secondTicker = "монетки";
                var tickerName = new Pair(firstTicker, secondTicker);
                var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);

                var entry = new List<Order> {new Order(10, 100)};
                var take = new List<Order> {new Order(7, 50)};
                var orderId = await connector.CreateFuturesOrder(tickerName, OrderType.Short, leverage, entry, take);

                connector.GetBalance(secondTicker).Available.Should().BeInRadius(startBalance - 10 * 100m / leverage,0.5m);
                var orderInfo = await connector.GetOrderInfo(tickerName, orderId);
                orderInfo.OrderStatus.Should().Be(OrderStatus.Open);
               // orderInfo.OrderType.Should().Be(OrderType.Short);
                orderInfo.FilledAmount.Should().Be(0);
                orderInfo.Amount.Should().Be(100);
                orderInfo.Price.Should().Be(10);

                var priceGoals = new List<decimal> {9m, 10.5m, 11m, 100000m};
                await MovePrice(priceGoals, tickerName);
                orderInfo = await connector.GetOrderInfo(tickerName, orderId);
                connector.GetBalance(secondTicker).Available.Should().BeInRadius(startBalance - 10 * 100m / leverage,1);
                connector.GetBalance(secondTicker).Locked.Should().Be(0);
                orderInfo.OrderStatus.Should().Be(OrderStatus.Close);
//                orderInfo.OrderType.Should().Be(OrderType.Short);
            //    orderInfo.FilledAmount.Should().Be(0);
                orderInfo.Amount.Should().Be(100);
                orderInfo.Price.Should().Be(10);
            }
        }
    }
}