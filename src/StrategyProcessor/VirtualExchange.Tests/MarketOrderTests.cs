﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Domain;
using Domain.StrategyProcessor;
using FluentAssertions;
using Infrastructure.FluentAssertions;

namespace VirtualExchange.Tests;

internal class MarketOrderTests : BaseOrderTest
{
    [TestCase("BTC", "USD", 100, OrderType.Buy)]
    [TestCase("BTC", "US", 0.001, OrderType.Buy)]
    public async Task JustBuySomeCoins(string firstTicker, string secondTicker, decimal amount, OrderType orderType)
    {
        // arrange
        var startBalance = 1000m;
        var pairName = new Pair(firstTicker, secondTicker);
        var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);
        await MovePrice(new List<decimal> {6m}, pairName);

        connector.GetBalance(secondTicker).Available.Should().Be(startBalance);
        connector.GetBalance(secondTicker).Locked.Should().Be(0);

        // act
        var order = await connector.CreateOrder(pairName, orderType, amount);
        await MovePrice(new List<decimal> {6m, 10m, 11m, 8m}, pairName);
        var info = await connector.GetOrderInfo(pairName, order);

        // assert
        info.OrderType.Should().Be(orderType);
        info.OrderStatus.Should().Be(OrderStatus.Close);

        connector.GetBalance(firstTicker).Available.Should().Be(amount);
        connector.GetBalance(firstTicker).Locked.Should().Be(0);

        connector.GetBalance(secondTicker).Available.Should().BeInRadius(startBalance - 6 * amount, 1);
        connector.GetBalance(secondTicker).Locked.Should().Be(0);
    }

    [Test]
    public async Task BuySomeCoinsAndCheckThatTheyGivesOnlyAfterPriceMove()
    {
        var startBalance = 1000m;
        var firstTicker = "BTC";
        var secondTicker = "USD";
        var pairName = new Pair(firstTicker, secondTicker);
        var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);
        await MovePrice(new List<decimal> {6m}, pairName);

        var order = await connector.CreateOrder(pairName, OrderType.Buy, 100);
        var info = await connector.GetOrderInfo(pairName, order);

        info.OrderType.Should().Be(OrderType.Buy);
        info.OrderStatus.Should().Be(OrderStatus.Close);
        info.Amount.Should().Be(100);
        info.FilledAmount.Should().Be(100);

        connector.GetBalance(firstTicker).Available.Should().Be(100);
        connector.GetBalance(firstTicker).Locked.Should().Be(0);
        connector.GetBalance(secondTicker).Available.Should().BeInRadius(1000 - 6 * 100, 1);
        connector.GetBalance(secondTicker).Locked.Should().Be(0);
    }

    [Test]
    public async Task BuySomeCoinsMultiplyAmount()
    {
        var startBalance = 10000m;
        var firstTicker = "BTC";
        var anotherFirstTicker = "ETH";
        var secondTicker = "USD";
        var pairName = new Pair(firstTicker, secondTicker);
        var anotherPairName = new Pair(anotherFirstTicker, secondTicker);
        var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);
        await MovePrice(new List<decimal> {6m}, pairName);
        await MovePrice(new List<decimal> {3m}, anotherPairName);

        var order1 = await connector.CreateOrder(pairName, OrderType.Buy, 100);
        var order2 = await connector.CreateOrder(pairName, OrderType.Buy, 10);
        var order11 = await connector.CreateOrder(anotherPairName, OrderType.Buy, 90);
        var order22 = await connector.CreateOrder(anotherPairName, OrderType.Buy, 10);
        await MovePrice(new List<decimal> {6m, 10m, 8m}, pairName);
        await MovePrice(new List<decimal> {3m, 6m}, anotherPairName);


        connector.GetBalance(firstTicker).Available.Should().Be(110);
        connector.GetBalance(firstTicker).Locked.Should().Be(0);

        connector.GetBalance(anotherFirstTicker).Available.Should().Be(100);
        connector.GetBalance(anotherFirstTicker).Locked.Should().Be(0);

        connector.GetBalance(secondTicker).Available.Should().BeInRadius(10000 - 6 * 110 - 3 * 100, 1);
        connector.GetBalance(secondTicker).Locked.Should().Be(0);


        var info = await connector.GetOrderInfo(pairName, order1);
        info.OrderType.Should().Be(OrderType.Buy);
        info.OrderStatus.Should().Be(OrderStatus.Close);
        info.Amount.Should().Be(100);
        info.FilledAmount.Should().Be(100);

        info = await connector.GetOrderInfo(pairName, order2);
        info.OrderType.Should().Be(OrderType.Buy);
        info.OrderStatus.Should().Be(OrderStatus.Close);
        info.Amount.Should().Be(10);
        info.FilledAmount.Should().Be(10);


        info = await connector.GetOrderInfo(pairName, order11);
        info.OrderType.Should().Be(OrderType.Buy);
        info.OrderStatus.Should().Be(OrderStatus.Close);
        info.Amount.Should().Be(90);
        info.FilledAmount.Should().Be(90);

        info = await connector.GetOrderInfo(pairName, order22);
        info.OrderType.Should().Be(OrderType.Buy);
        info.OrderStatus.Should().Be(OrderStatus.Close);
        info.Amount.Should().Be(10);
        info.FilledAmount.Should().Be(10);
    }


    [Test]
    [TestCase("BTC", "USD", 100, OrderType.Sell)]
    [TestCase("BTC", "US", 0.01, OrderType.Sell)]
    public async Task JustSellSomeCoins(string firstTicker, string secondTicker, decimal amount, OrderType orderType)
    {
        // arrange
        var startBalance = 1000m;
        var pairName = new Pair(firstTicker, secondTicker);
        var connector = Exchange.CreateConnector("юджин", firstTicker, startBalance);
        await MovePrice(new List<decimal> {6m}, pairName);

        connector.GetBalance(firstTicker).Available.Should().Be(startBalance);
        connector.GetBalance(firstTicker).Locked.Should().Be(0);

        // act
        var order = await connector.CreateOrder(pairName, orderType, amount);
        await MovePrice(new List<decimal> {6m, 10m, 11m, 8m}, pairName);
        var info = await connector.GetOrderInfo(pairName, order);

        // assert
        info.OrderType.Should().Be(orderType);
        info.OrderStatus.Should().Be(OrderStatus.Close);

        connector.GetBalance(firstTicker).Available.Should().Be(startBalance - amount);
        connector.GetBalance(firstTicker).Locked.Should().Be(0);

        connector.GetBalance(secondTicker).Available.Should().BeInRadius(6 * amount, 1);
        connector.GetBalance(secondTicker).Locked.Should().Be(0);
    }

    [Test]
    public async Task BuyAndSellCoin()
    {
        // arrange
        var startBalance = 10000m;
        var firstTicker = "BTC";
        var secondTicker = "USD";
        var pairName = new Pair(firstTicker, secondTicker);
        var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);
        await MovePrice(new List<decimal> {6m}, pairName);

        connector.GetBalance(secondTicker).Available.Should().Be(startBalance);
        connector.GetBalance(secondTicker).Locked.Should().Be(0);

        // act
        await connector.CreateOrder(pairName, OrderType.Buy, 100);
        await MovePrice(new List<decimal> {6m, 10m, 11m, 6m}, pairName);
        await connector.CreateOrder(pairName, OrderType.Sell, 100);

        // assert
        connector.GetBalance(secondTicker).Available.Should().BeInRadius(startBalance, 1);
        connector.GetBalance(secondTicker).Locked.Should().Be(0);
    }

    [Test]
    public async Task ThrowExceptionThanNotEnoughMoneyToBuy()
    {
        // arrange
        var startBalance = 100m;
        var firstTicker = "BTC";
        var secondTicker = "USD";
        var pairName = new Pair(firstTicker, secondTicker);
        var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);
        await MovePrice(new List<decimal> {1000m}, pairName);

        connector.GetBalance(secondTicker).Available.Should().Be(startBalance);
        connector.GetBalance(secondTicker).Locked.Should().Be(0);

        Assert.Catch(() => connector.CreateOrder(pairName, OrderType.Buy, 100).GetAwaiter().GetResult());

        connector.GetBalance(secondTicker).Available.Should().Be(startBalance);
        connector.GetBalance(secondTicker).Locked.Should().Be(0);
    }

    [Test]
    public async Task ThrowExceptionThanNotEnoughMoneyToSell()
    {
        // arrange
        var startBalance = 100m;
        var firstTicker = "BTC";
        var secondTicker = "USD";
        var pairName = new Pair(firstTicker, secondTicker);
        var connector = Exchange.CreateConnector("юджин", secondTicker, startBalance);
        await MovePrice(new List<decimal> {1000m}, pairName);

        connector.GetBalance(secondTicker).Available.Should().Be(startBalance);
        connector.GetBalance(secondTicker).Locked.Should().Be(0);

        Assert.Catch(() => connector.CreateOrder(pairName, OrderType.Sell, 100).GetAwaiter().GetResult());

        connector.GetBalance(secondTicker).Available.Should().Be(startBalance);
        connector.GetBalance(secondTicker).Locked.Should().Be(0);
    }
}