﻿using System.Text.Json;
using Domain.Connectors;
using Domain.Patterns.Objects;
using Domain.StrategyProcessor;
using EventStore.Client;
using Infrastructure.EventDriven;
using Infrastructure.Json;
using Infrastructure.Extensions;
using IODataModule.Client.Clients;
using StrategyProcessor.Infrastructure;
using StrategyProcessor.Infrastructure.DAL.Models;
using StrategyProcessor.Infrastructure.DAL.Repositories;
using StrategyProcessor.Infrastructure.DAL.Repositories.RestoreAfterFailure;
using StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;
using StrategyProcessor.Infrastructure.Mappers;
using StrategyProcessor.Services;
using StrategyProcessor.Strategy;
using StrategyProcessor.Tests.Common;
using StrategyProcessor.Tests.Common.InMemoryStorages;

// ReSharper disable UnusedAutoPropertyAccessor.Local

namespace StrategyProcessor.Tests;

[NonParallelizable]
internal class StrategyServiceTest
{
    [SetUp]
    public void Setup()
    {
        _tradingStrategyManagerStorage = new TradingStrategyManagerStorage();
        _testingStrategyManagerStorage = new TestingStrategyManagerStorage();
        _marketOrders = new();
        _limitOrders = new();
        _futuresOrders = new();

        _dataSource = new();
        _candles = new List<Candle> {new(1, 1, 1, 1, 1)};

        var spotMock = new Mock<ISpotConnector>();
        spotMock.Setup(a => a.GetCandles(It.IsAny<Pair>(), It.IsAny<TimeFrame>(), It.IsAny<DateTimeRange>()).Result)
            .Returns((Pair _, TimeFrame _, DateTimeRange _) => _candles);
        spotMock.Setup(a => a.CreateMarketOrder(It.IsAny<Pair>(), It.IsAny<MarketOrder>(), It.IsAny<decimal>()))
            .Callback((Pair pair, MarketOrder order, decimal amount) => _marketOrders.Add((pair, order, amount)))
            .ReturnsAsync((Pair _, MarketOrder _, decimal _) => new OrderId("ASDASDSAD21313"));
        spotMock.Setup(a => a.CreateLimitOrder(It.IsAny<Pair>(), It.IsAny<LimitOrder>(), It.IsAny<PlacementOrder>()))
            .Callback((Pair pair, LimitOrder type, PlacementOrder order) => _limitOrders.Add((pair, type, order)))
            .ReturnsAsync((Pair _, MarketOrder _, PlacementOrder _) => new OrderId("jhhgdhdfgdf34t34tgf43"));

        var futuresMock = new Mock<IFuturesConnector>();
        futuresMock.Setup(a => a.GetCandles(It.IsAny<Pair>(), It.IsAny<TimeFrame>(), It.IsAny<DateTimeRange>()).Result)
            .Returns((Pair _, TimeFrame _, DateTimeRange _) => _candles);
        futuresMock.Setup(a => a.CreateFuturesOrder(It.IsAny<Pair>(), It.IsAny<FuturesOrder>(), It.IsAny<int>(),
                It.IsAny<PlacementOrder>(), It.IsAny<PlacementOrder>(), It.IsAny<PlacementOrder>()))
            .Callback((Pair pair, FuturesOrder type, int leverage, PlacementOrder entry, PlacementOrder? take, PlacementOrder? stop) => _futuresOrders.Add((pair, type, leverage, entry, take, stop)))
            .ReturnsAsync((Pair _, FuturesOrder _, int _, PlacementOrder _, PlacementOrder? _, PlacementOrder? _) => new OrderId("jghjjfey5vw34x21ze31xe332"));

        _remoteConnectorFactory = new MockedRemoteConnectorFactory(spotMock.Object, futuresMock.Object);

        _strategyManagerRepository = new InMemoryStrategyManagerRepository();

        _runStrategyRepository = new Mock<IRunStrategyRepository>();
        _orderRepository = new InMemoryOrderRepository();
        _chartRepository = new InMemoryChartRepository();
        _notesRepository = new InMemoryNotesRepository();


        _candleServiceMock = new Mock<ICandleService>();
        _candleServiceMock.Setup(x =>
                x.GetRange(It.IsAny<Guid>(), It.IsAny<ExchangePairTimeFrame>(), It.IsAny<long>(), It.IsAny<long>()).Result)
            .Returns((Guid _, ExchangePairTimeFrame _, long _, long _) => _candles);

        _candleServiceMock.Setup(x =>
                x.GetRangeStream(It.IsAny<Guid>(), It.IsAny<ExchangePairTimeFrame>(), It.IsAny<long>(), It.IsAny<long>()))
            .Returns((Guid _, ExchangePairTimeFrame _, long _, long _) => _candles.ToAsyncEnumerable());
    }
    
    [TestCase(Exchange.BinanceFutures, RunMode.Real, 1)]
    [TestCase(Exchange.BinanceFutures, RunMode.RealDataTest, 1)]
    [TestCase(Exchange.BinanceFutures, RunMode.HistoryTest, 0)]
    public async Task CheckThatOpenedFuturesStrategiesStoreOrNotInDb(Exchange exchange, RunMode runMode,
        int expectedCount)
    {
        var dataSources = new List<ExchangePairTimeFrame> {new(exchange, new Pair("BTC", "USDT"), TimeFrame.h4)};
        var (strategyId, ownerId) = (Guid.NewGuid(), Guid.NewGuid());

        await GetStartStopStrategyService<TestStrategyEveryCandleSetStatusOpenedFutures, IFuturesConnector>()
            .StartNewStrategyAsync(new StartStrategyParameters(
                strategyId,
                ownerId,
                runMode,
                false,
                dataSources,
                new List<StrategyParameterDbo>(),
                new List<Balance> {new("USDT", 100, 0)},
                new DateTimeRange(DateTime.Now, DateTime.Now)
            ));

        var newCandleEvent = new NewCandleEvent(new Candle(1, 10, 20, 5, 10), new Pair("BTC", "USDT"),
            TimeFrame.h4, exchange, DateTime.Now.ToMilliseconds());
        await SendEvent(newCandleEvent);

        var state = _strategyManagerRepository.ReadAll().Result.FirstOrDefault()?.StrategyManagerState
            .Deserialize<StrategyManagerState<TestStrategyEveryCandleSetStatusOpenedFutures>>(JsonOptions
                .JsonSerializerOptions);
        state?.ActiveStrategiesByExchange.Count.Should().Be(expectedCount);
    }

    [TestCase(Exchange.BinanceSpot, RunMode.Real, 1)]
    [TestCase(Exchange.BinanceSpot, RunMode.RealDataTest, 1)]
    [TestCase(Exchange.BinanceSpot, RunMode.HistoryTest, 0)]
    public async Task CheckThatOpenedSpotStrategiesStoreOrNotInDb(Exchange exchange, RunMode runMode, int expectedCount)
    {
        var dataSources = new List<ExchangePairTimeFrame> {new(exchange, new Pair("BTC", "USDT"), TimeFrame.h4)};
        var (strategyId, ownerId) = (Guid.NewGuid(), Guid.NewGuid());

        await GetStartStopStrategyService<TestStrategyEveryCandleSetStatusOpenedSpot, ISpotConnector>()
            .StartNewStrategyAsync(new StartStrategyParameters(
                strategyId,
                ownerId,
                runMode,
                false,
                dataSources,
                new List<StrategyParameterDbo>(),
                new List<Balance> {new("USDT", 100, 0)},
                new DateTimeRange(DateTime.Now, DateTime.Now.AddMinutes(1))
            ));

        var newCandleEvent = new NewCandleEvent(candle: new Candle(1, 10, 20, 5, 10), exchange: exchange,
            pair: new Pair("BTC", "USDT"), timeFrame: TimeFrame.h4, timeStamp: DateTime.Now.ToMilliseconds());
        await SendEvent(newCandleEvent);

        var state = _strategyManagerRepository.ReadAll().Result.FirstOrDefault()?.StrategyManagerState
            .Deserialize<StrategyManagerState<TestStrategyEveryCandleSetStatusOpenedSpot>>(JsonOptions.JsonSerializerOptions);
        state?.ActiveStrategiesByExchange.Count.Should().Be(expectedCount);
    }

    [TestCase(RunMode.Real)]
    [TestCase(RunMode.HistoryTest)]
    [TestCase(RunMode.RealDataTest)]
    public async Task CheckThatCreatedStrategiesNotStoreInDb(RunMode runMode)
    {
        var dataSources = new List<ExchangePairTimeFrame>
        {
            new(Exchange.BinanceSpot, new Pair("BTC", "USDT"), TimeFrame.h4)
        };

        var (strategyId, ownerId) = (Guid.NewGuid(), Guid.NewGuid());

        var events = new[]
        {
            new NewCandleEvent(candle: new Candle(1, 10, 20, 5, 10), exchange: Exchange.BinanceSpot,
                pair: new Pair("BTC", "USDT"), timeFrame: TimeFrame.h4, timeStamp: DateTime.Now.ToMilliseconds())
        };


        var runId = await StartStrategyAndSendCandles<TestStrategy2, ISpotConnector>(
            events,
            strategyId,
            ownerId,
            runMode,
            dataSources,
            new List<StrategyParameterDbo>(),
            new List<Balance> {new Balance("USDT", 1000, 0)},
            _fromDateTime,
            _toDateTime
        );


        if (!runMode.IsRealTime())
        {
            _strategyManagerRepository.ReadAll().Result.Count(x => x.RunId == runId).Should().Be(0);
        }
        else
        {
            var state = _strategyManagerRepository.ReadAll().Result.First(x => x.RunId == runId).StrategyManagerState
                .Deserialize<StrategyManagerState<TestStrategy2>>(JsonSerializerOptions.Default);
            state?.ActiveStrategiesByExchange.Sum(x => x.Value.Count).Should().Be(0);
        }
    }

    [Test]
    public async Task CheckThatIfCandleNotInAllowListItNotSendedToStrategy()
    {
        var dataSources = new List<ExchangePairTimeFrame>
        {
            new(Exchange.BinanceSpot, new Pair("BTC", "USDT"), TimeFrame.h4)
        };

        var (strategyId, ownerId) = (Guid.NewGuid(), Guid.NewGuid());

        await GetStartStopStrategyService<TestStrategyEveryCandleSetStatusClosed, ISpotConnector>()
            .StartNewStrategyAsync(new StartStrategyParameters(
                strategyId,
                ownerId,
                RunMode.Real,
                false,
                dataSources,
                new List<StrategyParameterDbo>(),
                new List<Balance> {new("USDT", 100, 0)},
                new DateTimeRange(DateTime.Now, DateTime.Now.AddMinutes(1)))
            );

        var newCandleEvent = new NewCandleEvent(candle: new Candle(1, 10, 20, 5, 10),
            exchange: Exchange.BinanceSpot, pair: new Pair("ETH", "USDT"), timeFrame: TimeFrame.h1,
            timeStamp: DateTime.Now.ToMilliseconds());
        await SendEvent(newCandleEvent);

        var state = _strategyManagerRepository.ReadAll().Result.First().StrategyManagerState
            .Deserialize<StrategyManagerState<TestStrategyEveryCandleSetStatusClosed>>(JsonSerializerOptions.Default);
        state?.ActiveStrategiesByExchange.Count.Should().Be(0);
    }


    [TestCase(RunMode.Real)]
    [TestCase(RunMode.HistoryTest)]
    [TestCase(RunMode.RealDataTest)]
    public async Task CheckThatStrategyBuyOnEveryCandle(RunMode runMode)
    {
        var pair = new Pair("BTC", "USDT");
        var exchange = Exchange.BinanceSpot;
        var dataSources = new List<ExchangePairTimeFrame>
        {
            new(exchange, pair, TimeFrame.h4)
        };

        var (strategyId, ownerId) = (Guid.NewGuid(), Guid.NewGuid());


        var events = Enumerable.Repeat(0, 10).Select(x => new NewCandleEvent(new Candle(x, 10, 10, 10, 10), pair,
            TimeFrame.h4, exchange, DateTime.Now.ToMilliseconds()));


        await StartStrategyAndSendCandles<TestStrategyCheckThatStrategyBuyOnEveryCandle, ISpotConnector>(
            events,
            strategyId,
            ownerId,
            runMode,
            dataSources,
            new List<StrategyParameterDbo>(),
            new List<Balance> {new Balance("USDT", 100000, 0)},
            _fromDateTime,
            _toDateTime
        );


        if (runMode.IsTest())
            _limitOrders.Count.Should().Be(0);
        else
            _limitOrders.ForEach(x => x.Should().Be((pair, LimitOrder.BuyLimit, new PlacementOrder(10, 1))));

        //9 а не 10 т.к цена передается дискретно. последний ордер не выполнится, пока новая цена не придет
    }

    [TestCase(RunMode.Real)]
    [TestCase(RunMode.HistoryTest)]
    [TestCase(RunMode.RealDataTest)]
    public async Task CheckThatStrategyCanUseMarketOrders(RunMode runMode)
    {
        var pair = new Pair("BTC", "USDT");
        var exchange = Exchange.BinanceSpot;
        var dataSources = new List<ExchangePairTimeFrame>
        {
            new(exchange, pair, TimeFrame.h4)
        };

        var (strategyId, ownerId) = (Guid.NewGuid(), Guid.NewGuid());

        await StartStrategyAndSendCandles<TestStrategyCheckThatStrategyCanUseMarketOrders, ISpotConnector>(
            TestCandles.Select(x => new NewCandleEvent(x, pair, TimeFrame.h4, exchange, x.TimeStamp)),
            strategyId,
            ownerId,
            runMode,
            dataSources,
            new List<StrategyParameterDbo>(),
            new List<Balance> {new Balance("USDT", 100, 0)},
            _fromDateTime,
            _toDateTime
        );


        if (runMode.IsTest())
        {
            _marketOrders.Count.Should().Be(0);
        }
        else
        {
            _marketOrders.Where(x => x.Item2 == MarketOrder.Buy).ToList()
                .ForEach(x => x.Should().Be((pair, MarketOrder.Buy, 1)));
            _marketOrders.Count(x => x.Item2 == MarketOrder.Buy).Should().Be(7);

            _marketOrders.Where(x => x.Item2 == MarketOrder.Sell).ToList()
                .ForEach(x => x.Should().Be((pair, MarketOrder.Sell, 1)));
            _marketOrders.Count(x => x.Item2 == MarketOrder.Sell).Should().Be(6);
        }
        //    coins.First(x => x.Asset == new Ticker("USDT")).Available.Should().Be(10000 - 10);

        //-10 т.к цена передается дискретно. последний ордер не выполнится, пока новая цена не придет
    }

    [TestCase(RunMode.Real)]
    [TestCase(RunMode.HistoryTest)]
    [TestCase(RunMode.RealDataTest)]
    public async Task CheckThatStrategyCanUseLimitOrders(RunMode runMode)
    {
        var pair = new Pair("BTC", "USDT");
        var exchange = Exchange.BinanceSpot;
        var dataSources = new List<ExchangePairTimeFrame>
        {
            new(exchange, pair, TimeFrame.h4)
        };

        var (strategyId, ownerId) = (Guid.NewGuid(), Guid.NewGuid());

        TestStrategyCheckThatStrategyCanUseLimitOrders.OrderAmount = 0;
        await StartStrategyAndSendCandles<TestStrategyCheckThatStrategyCanUseLimitOrders, ISpotConnector>(
            TestCandles.Select(x => new NewCandleEvent(x, pair, TimeFrame.h4, exchange, x.TimeStamp)),
            strategyId,
            ownerId,
            runMode,
            dataSources,
            new List<StrategyParameterDbo>(),
            new List<Balance> {new Balance("USDT", 100, 0), new Balance("BTC", 100, 0)},
            _fromDateTime,
            _toDateTime
        );


        if (runMode.IsTest())
        {
            _limitOrders.Count.Should().Be(0);
        }
        else
        {
            _limitOrders.Where(x => x.Item2 == LimitOrder.SellLimit).ToList().ForEach(x =>
                x.Should().Be((pair, LimitOrder.SellLimit, new PlacementOrder(10, 1))));
            _limitOrders.Count(x => x.Item2 == LimitOrder.SellLimit).Should().Be(TestCandles.Length - 7);

            _limitOrders.Where(x => x.Item2 == LimitOrder.BuyLimit).ToList().ForEach(x =>
                x.Should().Be((pair, LimitOrder.BuyLimit, new PlacementOrder(5, 1))));
            _limitOrders.Count(x => x.Item2 == LimitOrder.BuyLimit).Should().Be(7);
        }
    }


    [TestCase(RunMode.Real)]
    [TestCase(RunMode.HistoryTest)]
    [TestCase(RunMode.RealDataTest)]
    public async Task CheckThatStrategyCanUseFuturesOrders(RunMode runMode)
    {
        var pair = new Pair("BTC", "USDT");
        var exchange = Exchange.BinanceFutures;
        var dataSources = new List<ExchangePairTimeFrame>
        {
            new(exchange, pair, TimeFrame.h4)
        };

        var (strategyId, ownerId) = (Guid.NewGuid(), Guid.NewGuid());


        await StartStrategyAndSendCandles<TestStrategyCheckThatStrategyCanUseFuturesOrders, IFuturesConnector>(
            TestCandles.Select(x => new NewCandleEvent(x, pair, TimeFrame.h4, exchange, x.TimeStamp)),
            strategyId,
            ownerId,
            runMode,
            dataSources,
            new List<StrategyParameterDbo>(),
            new List<Balance> {new Balance("USDT", 100, 0)},
            _fromDateTime,
            _toDateTime
        );

        if (runMode.IsTest())
        {
            _futuresOrders.Count.Should().Be(0);
        }
        else
        {
            _futuresOrders.Where(x => x.Item2 == FuturesOrder.Short).ToList().ForEach(x =>
                x.Should().Be((pair, FuturesOrder.Short, 10, new PlacementOrder(10, 1),
                    new PlacementOrder(4, 1), new PlacementOrder(12, 1))));
            _futuresOrders.Count(x => x.Item2 == FuturesOrder.Short).Should().Be(6);

            _futuresOrders.Where(x => x.Item2 == FuturesOrder.Long).ToList().ForEach(x =>
                x.Should().Be((pair, FuturesOrder.Long, 10, new PlacementOrder(5, 1),
                    new PlacementOrder(12, 1), new PlacementOrder(4, 1))));
            _futuresOrders.Count(x => x.Item2 == FuturesOrder.Long).Should().Be(7);
        }
    }

    [TestCase(RunMode.Real, Exchange.BinanceSpot)]
    [TestCase(RunMode.HistoryTest, Exchange.BinanceSpot)]
    [TestCase(RunMode.RealDataTest, Exchange.BinanceSpot)]
    public async Task Strategy_CanUseNotes(RunMode runMode, Exchange exchange)
    {
        var pair = new Pair("BTC", "USDT");
        var dataSources = new List<ExchangePairTimeFrame> {new(exchange, pair, TimeFrame.h4)};

        var (strategyId, ownerId) = (Guid.NewGuid(), Guid.NewGuid());

        var runId = await StartStrategyAndSendCandles<TestStrategySpotThatCreateNoteOnEveryCandle, ISpotConnector>(
            TestCandles.Select(x => new NewCandleEvent(x, pair, TimeFrame.h4, exchange, x.TimeStamp)),
            strategyId,
            ownerId,
            runMode,
            dataSources,
            new List<StrategyParameterDbo>(),
            new List<Balance> {new("USDT", 100, 0)},
            _fromDateTime,
            _toDateTime
            );


        var notes = _notesRepository.ReadAsync(runId, pair, TimeFrame.h4, new DateTimeRange(_fromDateTime, _toDateTime)).Result
            .ToList();

        notes.Count.Should().Be(TestCandles.Length);

        for (var i = 0; i < TestCandles.Length; i++)
        {
            notes[i].Icon.Should().Be((Icon) (i % 2));
            notes[i].Text.Should().Be(i.ToString());
            notes[i].TimeStamp.Should().Be(TestCandles[i].TimeStamp);
            notes[i].Price.Should().Be(TestCandles[i].High);
        }
    }

    [TestCase(RunMode.Real, Exchange.BinanceFutures)]
    [TestCase(RunMode.HistoryTest, Exchange.BinanceFutures)]
    [TestCase(RunMode.RealDataTest, Exchange.BinanceFutures)]
    public async Task FuturesStrategy_UseNotes(RunMode runMode, Exchange exchange)
    {
        var pair = new Pair("BTC", "USDT");
        var dataSources = new List<ExchangePairTimeFrame> {new(exchange, pair, TimeFrame.h4)};

        var (strategyId, ownerId) = (Guid.NewGuid(), Guid.NewGuid());

        var runId =
            await StartStrategyAndSendCandles<TestStrategyFuturesThatCreateNotesOnEveryCandle, IFuturesConnector>(
                TestCandles.Select(x => new NewCandleEvent(x, pair, TimeFrame.h4, exchange, x.TimeStamp)),
                strategyId,
                ownerId,
                runMode,
                dataSources,
                new List<StrategyParameterDbo>(),
                new List<Balance> {new("USDT", 100, 0)},
                _fromDateTime,
                _toDateTime
                );


        var notes = _notesRepository.ReadAsync(runId, pair, TimeFrame.h4, new DateTimeRange(_fromDateTime, _toDateTime)).Result
            .ToList();

        notes.Count.Should().Be(TestCandles.Length);

        for (var i = 0; i < TestCandles.Length; i++)
        {
            notes[i].Icon.Should().Be((Icon) (i % 2));
            notes[i].Text.Should().Be(i.ToString());
            notes[i].TimeStamp.Should().Be(TestCandles[i].TimeStamp);
            notes[i].Price.Should().Be(TestCandles[i].High);
        }
    }


    [TestCase(RunMode.Real)]
    [TestCase(RunMode.HistoryTest)]
    [TestCase(RunMode.RealDataTest)]
    public async Task CheckThatStrategiesMarketOrdersLogging(RunMode runMode)
    {
        var pair = new Pair("BTC", "USDT");
        var exchange = Exchange.BinanceSpot;
        var dataSources = new List<ExchangePairTimeFrame>
        {
            new(exchange, pair, TimeFrame.h4)
        };

        var (strategyId, ownerId) = (Guid.NewGuid(), Guid.NewGuid());

        var runId = await StartStrategyAndSendCandles<TestStrategyCheckThatStrategyCanUseMarketOrders, ISpotConnector>(
            TestCandles.Select(x => new NewCandleEvent(x, pair, TimeFrame.h4, exchange, x.TimeStamp)),
            strategyId,
            ownerId,
            runMode,
            dataSources,
            new List<StrategyParameterDbo>(),
            new List<Balance> {new Balance("USDT", 100, 0)},
            _fromDateTime,
            _toDateTime
        );


        var orders = _orderRepository.ReadAsync(runId, pair, TimeFrame.h4, new DateTimeRange(_fromDateTime, DateTime.UtcNow.AddMinutes(1))).Result
            .ToList();

        orders.Count.Should().Be(13);

        orders.Where(x => x.Type == OrderType.Buy).ToList()
            .ForEach(x =>
            {
                x.Pair.Should().Be(pair);
                x.Leverage.Should().BeNull();
                x.Amount.Should().Be(1);
                x.Price.Should().BeNull();
                x.StopPrice.Should().BeNull();
                x.TakePrice.Should().BeNull();
            });
        orders.Count(x => x.Type == OrderType.Buy).Should().Be(7);

        orders.Where(x => x.Type == OrderType.Sell).ToList()
            .ForEach(x =>
            {
                x.Pair.Should().Be(pair);
                x.Leverage.Should().BeNull();
                x.Amount.Should().Be(1);
                x.Price.Should().BeNull();
                x.StopPrice.Should().BeNull();
                x.TakePrice.Should().BeNull();
            });
        orders.Count(x => x.Type == OrderType.Sell).Should().Be(6);

        //-10 т.к цена передается дискретно. последний ордер не выполнится, пока новая цена не придет
    }

    [TestCase(RunMode.Real)]
    [TestCase(RunMode.HistoryTest)]
    [TestCase(RunMode.RealDataTest)]
    public async Task CheckThatStrategiesLimitOrdersLogging(RunMode runMode)
    {
        var pair = new Pair("BTC", "USDT");
        var exchange = Exchange.BinanceSpot;
        var dataSources = new List<ExchangePairTimeFrame>
        {
            new(exchange, pair, TimeFrame.h4)
        };

        var (strategyId, ownerId) = (Guid.NewGuid(), Guid.NewGuid());
        TestStrategyCheckThatStrategyCanUseLimitOrders.OrderAmount = 0;
        var runId = await StartStrategyAndSendCandles<TestStrategyCheckThatStrategyCanUseLimitOrders, ISpotConnector>(
            TestCandles.Select(x => new NewCandleEvent(x, pair, TimeFrame.h4, exchange, x.TimeStamp)),
            strategyId,
            ownerId,
            runMode,
            dataSources,
            new List<StrategyParameterDbo>(),
            new List<Balance> {new Balance("USDT", 1000000, 0), new Balance("BTC", 10000, 0)},
            _fromDateTime,
            _toDateTime
        );

        var orders = _orderRepository.ReadAsync(runId, pair, TimeFrame.h4, new DateTimeRange(_fromDateTime, DateTime.UtcNow.AddMinutes(1))).Result
            .ToList();

        orders.Count.Should().Be(TestCandles.Length);

        orders.Where(x => x.Type == OrderType.BuyLimit).ToList()
            .ForEach(x =>
            {
                x.Pair.Should().Be(pair);
                x.Leverage.Should().BeNull();
                x.Amount.Should().Be(1);
            });
        orders.Count(x => x.Type == OrderType.BuyLimit).Should().Be(7);

        orders.Where(x => x.Type == OrderType.SellLimit).ToList()
            .ForEach(x =>
            {
                x.Pair.Should().Be(pair);
                x.Leverage.Should().BeNull();
                x.StopPrice.Should().BeNull();
                x.TakePrice.Should().BeNull();
            });
        orders.Count(x => x.Type == OrderType.SellLimit).Should().Be(TestCandles.Length - 7);
    }

    [TestCase(RunMode.Real)]
    [TestCase(RunMode.HistoryTest)]
    [TestCase(RunMode.RealDataTest)]
    public async Task CheckThatStrategiesFuturesOrdersLogging(RunMode runMode)
    {
        var pair = new Pair("BTC", "USDT");
        var exchange = Exchange.BinanceFutures;
        var dataSources = new List<ExchangePairTimeFrame>
        {
            new(exchange, pair, TimeFrame.h4)
        };

        var (strategyId, ownerId) = (Guid.NewGuid(), Guid.NewGuid());

        var runId =
            await StartStrategyAndSendCandles<TestStrategyCheckThatStrategyCanUseFuturesOrders, IFuturesConnector>(
                TestCandles.Select(x => new NewCandleEvent(x, pair, TimeFrame.h4, exchange, x.TimeStamp)),
                strategyId,
                ownerId,
                runMode,
                dataSources,
                new List<StrategyParameterDbo>(),
                new List<Balance> {new Balance("USDT", 100, 0)},
                _fromDateTime,
                _toDateTime
            );

        var orders = _orderRepository.ReadAsync(runId, pair, TimeFrame.h4, new DateTimeRange(_fromDateTime, DateTime.UtcNow.AddMinutes(1)))
            .Result.ToList();

        orders.Count.Should().Be(TestCandles.Length);

        orders.Where(x => x.Type == OrderType.Long).ToList()
            .ForEach(x =>
            {
                x.Pair.Should().Be(pair);
                x.Leverage.Should().Be(10);
                x.Amount.Should().Be(1);
                x.Price.Should().Be(5);
                x.StopPrice.Should().Be(4);
                x.TakePrice.Should().Be(12);
            });
        orders.Count(x => x.Type == OrderType.Long).Should().Be(7);

        orders.Where(x => x.Type == OrderType.Short).ToList()
            .ForEach(x =>
            {
                x.Pair.Should().Be(pair);
                x.Leverage.Should().Be(10);
                x.Amount.Should().Be(1);
                x.Price.Should().Be(10);
                x.StopPrice.Should().Be(12);
                x.TakePrice.Should().Be(4);
            });
        orders.Count(x => x.Type == OrderType.Short).Should().Be(6);
    }

    [TestCaseSource(nameof(TestDataCheckThatStrategyConsumeCorrectCandlesSpot))]
    public async Task CheckThatStrategyConsumeCorrectCandles(RunMode runMode, TimeFrame timeFrame)
    {
        var pair = new Pair("BTC", "USDT");
        var exchange = Exchange.BinanceSpot;
        var dataSources = new List<ExchangePairTimeFrame>
        {
            new(exchange, pair, timeFrame)
        };

        var (strategyId, ownerId) = (Guid.NewGuid(), Guid.NewGuid());

        await GetStartStopStrategyService<TestStrategyThatSaveLastCandleEventInVariableSpot, ISpotConnector>()
            .StartNewStrategyAsync(new StartStrategyParameters(
                strategyId,
                ownerId,
                runMode,
                false,
                dataSources,
                new List<StrategyParameterDbo>(),
                new List<Balance> {new Balance("USDT", 100, 0)},
                new DateTimeRange(DateTime.Now, DateTime.Now))
            );

        foreach (var candle in TestCandles)
        {
            var newCandleEvent = new NewCandleEvent(candle, pair, timeFrame, exchange, candle.TimeStamp);
            await SendEvent(newCandleEvent);
            TestStorage.LastEvent.Should().Be(newCandleEvent);
        }
    }

    private static int[] _runModes = {(int) RunMode.RealDataTest, (int) RunMode.Real};
    private static int[] _tfs = {(int) TimeFrame.h1, (int) TimeFrame.m1, (int) TimeFrame.Mo1, (int) TimeFrame.D1};

    public static object[][] TestDataCheckThatStrategyConsumeCorrectCandlesSpot = new[] {_runModes, _tfs}
        .CartesianProduct().Select(x => x.Select(i => (object) i).ToArray()).ToArray();

    public static object[][] TestDataCheckThatStrategyConsumeCorrectCandlesFutures = new[] {_runModes, _tfs}
        .CartesianProduct().Select(x => x.Select(i => (object) i).ToArray()).ToArray();


    [TestCaseSource(nameof(TestDataCheckThatStrategyConsumeCorrectCandlesFutures))]
    public async Task CheckThatStrategyConsumeCorrectCandlesFutures(RunMode runMode, TimeFrame timeFrame)
    {
        var pair = new Pair("BTC", "USDT");
        var exchange = Exchange.BinanceFutures;
        var dataSources = new List<ExchangePairTimeFrame>
        {
            new(exchange, pair, timeFrame)
        };

        var (strategyId, ownerId) = (Guid.NewGuid(), Guid.NewGuid());

        await GetStartStopStrategyService<TestStrategyThatSaveLastCandleEventInVariableFutures, IFuturesConnector>()
            .StartNewStrategyAsync(new StartStrategyParameters(
                strategyId,
                ownerId,
                runMode,
                false,
                dataSources,
                new List<StrategyParameterDbo>(),
                new List<Balance> {new Balance("USDT", 100, 0)},
                new DateTimeRange(DateTime.Now, DateTime.Now))
            );

        foreach (var candle in TestCandles)
        {
            var newCandleEvent = new NewCandleEvent(candle, pair, timeFrame, exchange, candle.TimeStamp);
            await SendEvent(newCandleEvent);
            TestStorage.LastEvent.Should().Be(newCandleEvent);
        }
    }


    [Test]
    public async Task CheckThatStrategyFuturesConsumeOnlyHisCandles()
    {
        var exchanges = EnumEnumerator.GetEnums<Exchange>().Where(x => x.IsFutures()).ToArray();
        var timeFrames = EnumEnumerator.GetEnums<TimeFrame>();
        var pairs = new[]
        {
            new Pair("BTC", "USDT"), new Pair("ETH", "USDT"), new Pair("LTC", "USD"), new Pair("SHIT", "COIN"),
            new Pair("ABO", "BA")
        };

        var dataSources = new List<ExchangePairTimeFrame>();

        for (var i = 0; i < _rnd.Next(1, 4); i++)
        {
            var pair = pairs.RandomElement();
            var exchange = exchanges.RandomElement();
            var timeFrame = timeFrames.RandomElement();
            dataSources.Add(new ExchangePairTimeFrame(exchange, pair, timeFrame));
        }

        var inputDatas = new List<(Exchange, Pair, TimeFrame)>();

        foreach (var exchange in exchanges)
        {
            foreach (var pair in pairs)
            {
                foreach (var timeFrame in timeFrames)
                    inputDatas.Add((exchange, pair, timeFrame));
            }
        }

        var (strategyId, ownerId) = (Guid.NewGuid(), Guid.NewGuid());

        foreach (var runMode in new[] {RunMode.RealDataTest, RunMode.Real})
        {
            await GetStartStopStrategyService<TestStrategyThatSaveLastCandleEventInVariableFutures, IFuturesConnector>()
                .StartNewStrategyAsync(new StartStrategyParameters(
                    strategyId,
                    ownerId,
                    runMode,
                    false,
                    dataSources,
                    new List<StrategyParameterDbo>(),
                    new List<Balance> {new Balance("USDT", 100, 0)},
                    new DateTimeRange(DateTime.Now, DateTime.Now)
                ));

            foreach (var (exchange, pair, timeFrame) in inputDatas)
            {
                foreach (var candle in TestCandles)
                {
                    var newCandleEvent = new NewCandleEvent(candle, pair, timeFrame, exchange, candle.TimeStamp);
                    await SendEvent(newCandleEvent);

                    if (dataSources.Contains(new ExchangePairTimeFrame(exchange, pair, timeFrame)))
                        TestStorage.LastEvent.Should().Be(newCandleEvent);
                    else
                        TestStorage.LastEvent.Should().NotBe(newCandleEvent);
                }
            }
        }
    }

    [Test]
    public async Task CheckThatStrategySpotConsumeOnlyHisCandles()
    {
        var exchanges = EnumEnumerator.GetEnums<Exchange>().Where(x => x.IsSpot()).ToArray();
        var timeFrames = EnumEnumerator.GetEnums<TimeFrame>();
        var pairs = new[]
        {
            new Pair("BTC", "USDT"), new Pair("ETH", "USDT"), new Pair("LTC", "USD"), new Pair("SHIT", "COIN"),
            new Pair("ABO", "BA")
        };


        var dataSources = new List<ExchangePairTimeFrame>();

        for (var i = 0; i < _rnd.Next(1, 4); i++)
        {
            var pair = pairs.RandomElement();
            var exchange = exchanges.RandomElement();
            var timeFrame = timeFrames.RandomElement();
            dataSources.Add(new ExchangePairTimeFrame(exchange, pair, timeFrame));
        }

        var inputDatas = new List<(Exchange, Pair, TimeFrame)>();

        foreach (var exchange in exchanges)
        {
            foreach (var pair in pairs)
            {
                foreach (var timeFrame in timeFrames)
                    inputDatas.Add((exchange, pair, timeFrame));
            }
        }


        var (strategyId, ownerId) = (Guid.NewGuid(), Guid.NewGuid());

        foreach (var runMode in new[] {RunMode.RealDataTest, RunMode.Real})
        {
            await GetStartStopStrategyService<TestStrategyThatSaveLastCandleEventInVariableSpot, ISpotConnector>()
                .StartNewStrategyAsync(new StartStrategyParameters(
                    strategyId,
                    ownerId,
                    runMode,
                    false,
                    dataSources,
                    new List<StrategyParameterDbo>(),
                    new List<Balance> {new Balance("USDT", 100, 0)},
                    new DateTimeRange(DateTime.Now, DateTime.Now))
                );

            foreach (var (exchange, pair, timeFrame) in inputDatas)
            {
                foreach (var candle in TestCandles)
                {
                    var newCandleEvent = new NewCandleEvent(candle, pair, timeFrame, exchange, candle.TimeStamp);
                    await SendEvent(newCandleEvent);

                    if (dataSources.Contains(new ExchangePairTimeFrame(exchange, pair, timeFrame)))
                        TestStorage.LastEvent.Should().Be(newCandleEvent);
                    else
                        TestStorage.LastEvent.Should().NotBe(newCandleEvent);
                }
            }
        }
    }

    [TestCase(RunMode.Real)]
    [TestCase(RunMode.HistoryTest)]
    [TestCase(RunMode.RealDataTest)]
    public async Task CheckThatParametersOfStrategyFillCorrect(RunMode runMode)
    {
        var pair = new Pair("BTC", "USDT");
        var exchange = Exchange.BinanceSpot;
        var dataSources = new List<ExchangePairTimeFrame>
        {
            new(exchange, pair, TimeFrame.h4)
        };

        var (strategyId, ownerId) = (Guid.NewGuid(), Guid.NewGuid());

        var parameters = new List<StrategyParameterDbo>
        {
            new StrategyParameter<decimal>("Decimal", 1001.1m).ToDbo(),
            new StrategyParameter<int>("Int", 312).ToDbo(),
            new StrategyParameter<float>("Float", 1001.1f).ToDbo(),
            new StrategyParameter<string>("String", "sdfsdfsdfw2132!@@1").ToDbo(),
            new StrategyParameter<DateTime>("DateTime", new DateTime(2100, 10, 10)).ToDbo(),
            new StrategyParameter<decimal[]>("Decimal[]", new[] {12m, 854m, -12m, 0m}).ToDbo(),
            new StrategyParameter<int[]>("Int[]", new[] {312, 0, -635342, 687574574}).ToDbo(),
            new StrategyParameter<char>("Char", 'k').ToDbo(),
            new StrategyParameter<bool>("Bool", true).ToDbo(),
            new StrategyParameter<List<string>>("List<string>", new List<string> {"1", "aa"}).ToDbo(),
        };

        await StartStrategyAndSendCandles<TestStrategySpotUseParameters, ISpotConnector>(
            TestCandles.Select(x => new NewCandleEvent(x, pair, TimeFrame.h4, exchange, x.TimeStamp)),
            strategyId,
            ownerId,
            runMode,
            dataSources,
            parameters,
            new List<Balance> {new Balance("USDT", 100, 0)},
            _fromDateTime,
            _toDateTime
        );

        TestStorage.Decimal.Should().Be(1001.1m);
        TestStorage.Int.Should().Be(312);
        TestStorage.Float.Should().Be(1001.1f);
        TestStorage.String.Should().Be("sdfsdfsdfw2132!@@1");
        TestStorage.DateTime.Should().Be(new DateTime(2100, 10, 10));
        TestStorage.DecimalArray.Should().BeEquivalentTo(new[] {12m, 854m, -12m, 0m});
        TestStorage.IntArray.Should().BeEquivalentTo(new[] {312, 0, -635342, 687574574});
        TestStorage.Char.Should().Be('k');
        TestStorage.Bool.Should().Be(true);
        TestStorage.ListOfString.Should().BeEquivalentTo(new List<string> {"1", "aa"});
    }
    
    //TODO тесты на ордер конфирматион

    private IStrategyService GetStartStopStrategyService<TStrategy, TConnector>()
        where TStrategy : BaseStrategy<TConnector>, new() where TConnector : class, IConnector
    {
        var orderSenderFactory = new OrderSenderClientFactory(null!);
        var strategyManagerFactory = new StrategyManagerFactory(_strategyManagerRepository, new DarkMagic());

        return new StrategyService<TStrategy, TConnector>(new RealTimeDataSource(), _runStrategyRepository.Object,
            _orderRepository, _chartRepository, _notesRepository,
            _tradingStrategyManagerStorage, _testingStrategyManagerStorage, _strategyManagerRepository,
            strategyManagerFactory, _remoteConnectorFactory, orderSenderFactory, _candleServiceMock.Object,
            new Mock<IEventStoreClient>().Object);
    }

    private async Task<Guid> StartStrategyAndSendCandles<TStrategy, TConnector>(
        IEnumerable<NewCandleEvent> candles,
        Guid strategyId,
        Guid ownerId,
        RunMode runMode,
        IReadOnlyList<ExchangePairTimeFrame> allowedSources,
        IReadOnlyList<StrategyParameterDbo> parameters,
        IReadOnlyList<Balance> startBalance,
        DateTime startTime,
        DateTime endTime
    )
        where TStrategy : BaseStrategy<TConnector>, new()
        where TConnector : class, IConnector
    {
        var newCandleEvents = candles.ToList();
        if (runMode == RunMode.HistoryTest)
            _candles = newCandleEvents.Select(x => x.Candle).ToList();

        var runId = await GetStartStopStrategyService<TStrategy, TConnector>().StartNewStrategyAsync(
            new StartStrategyParameters(
                strategyId,
                ownerId,
                runMode,
                false,
                allowedSources,
                parameters,
                startBalance,
                new DateTimeRange(startTime, endTime)
            ));

        if (runMode != RunMode.HistoryTest)
        {
            foreach (var newCandleEvent in newCandleEvents)
            {
                await SendEvent(newCandleEvent);
            }
        }

        return runId;
    }

    private async Task SendEvent(NewCandleEvent candleEvent)
    {
        await _dataSource.SendEvent(candleEvent);
        await Task.WhenAll(
            _tradingStrategyManagerStorage.StrategyManagers.Select(x => x.HandleCandleEvent(candleEvent)));
        await Task.WhenAll(
            _testingStrategyManagerStorage.StrategyManagers.Select(x => x.HandleCandleEvent(candleEvent)));
    }
    
    private static readonly Candle[] TestCandles =
    [
        new(0, 5, 5, 5, 5), new(1, 5, 10, 5, 10), new(2, 10, 5, 5, 5), new(3, 5, 10, 5, 10), new(4, 10, 5, 5, 5),
        new(5, 5, 10, 5, 10), new(6, 10, 5, 5, 5), new(7, 5, 10, 5, 10), new(8, 10, 5, 5, 5),
        new(9, 5, 10, 5, 10), new(10, 10, 5, 5, 5), new(11, 5, 10, 5, 10), new(12, 10, 5, 5, 5)
    ];
    private readonly DateTime _fromDateTime = TestCandles.MinBy(x => x.TimeStamp)!.TimeStamp.ToDateTime();
    private readonly DateTime _toDateTime = TestCandles.MaxBy(x => x.TimeStamp)!.TimeStamp.ToDateTime();

    

    public static class TestStorage
    {
        public static NewCandleEvent? LastEvent;
        public static decimal Decimal { get; set; }
        public static int Int { get; set; }
        public static float Float { get; set; }

        public static string String { get; set; } = null!;
        public static DateTime DateTime { get; set; }
        public static decimal[] DecimalArray { get; set; } = null!;
        public static int[] IntArray { get; set; } = Array.Empty<int>();
        public static char Char { get; set; }
        public static bool Bool { get; set; }
        public static List<string> ListOfString { get; set; } = new();
    }

    private record TestStrategySpotUseParameters : BaseStrategy<ISpotConnector>
    {
        [StrategyParameter("Decimal")] public decimal Decimal { get; set; }
        [StrategyParameter("Int")] public int Int { get; set; }
        [StrategyParameter("Float")] public float Float { get; set; }
        [StrategyParameter("String")] public string String { get; set; } = string.Empty;
        [StrategyParameter("DateTime")] public DateTime DateTime { get; set; }
        [StrategyParameter("Decimal[]")] public decimal[] DecimalArray { get; set; } = {1, 3, 4m};
        [StrategyParameter("Int[]")] public int[] IntArray { get; set; } = {1, 2, 7};
        [StrategyParameter("Char")] public char Char { get; set; }
        [StrategyParameter("Bool")] public bool Bool { get; set; }
        [StrategyParameter("List<string>")] public List<string> ListOfString { get; set; } = new() {"12", "3212"};

        public override Task EventsCatalog_NewCandle(NewCandleEvent obj)
        {
            Status = StrategyStatus.Opened;

            TestStorage.DateTime = DateTime;
            TestStorage.Bool = Bool;
            TestStorage.Float = Float;
            TestStorage.IntArray = IntArray;
            TestStorage.Char = Char;
            TestStorage.Decimal = Decimal;
            TestStorage.Int = Int;
            TestStorage.DecimalArray = DecimalArray;
            TestStorage.ListOfString = ListOfString;
            TestStorage.String = String;
            return Task.CompletedTask;
        }
    }

    private record TestStrategyThatSaveLastCandleEventInVariableSpot : BaseStrategy<ISpotConnector>
    {
        public override Task EventsCatalog_NewCandle(NewCandleEvent obj)
        {
            TestStorage.LastEvent = obj;
            return Task.CompletedTask;
        }
    }

    private record TestStrategyThatSaveLastCandleEventInVariableFutures : BaseStrategy<IFuturesConnector>
    {
        public override Task EventsCatalog_NewCandle(NewCandleEvent obj)
        {
            TestStorage.LastEvent = obj;
            return Task.CompletedTask;
        }
    }

    private record TestStrategyCheckThatStrategyBuyOnEveryCandle : BaseStrategy<ISpotConnector>
    {
        public override async Task EventsCatalog_NewCandle(NewCandleEvent obj)
        {
            await Connector.CreateLimitOrder(obj.Pair, LimitOrder.BuyLimit, new PlacementOrder(10, 1));
        }
    }

    private record TestStrategySpotThatCreateNoteOnEveryCandle : BaseStrategy<ISpotConnector>
    {
        private static int _count;

        public override Task EventsCatalog_NewCandle(NewCandleEvent obj)
        {
            if (obj.TimeStamp == 0)
            {
                _count = 0;
            }

            if (_count == 0)
            {
                Status = StrategyStatus.Opened;
            }

            if (Status == StrategyStatus.Opened)
            {
                CreateNote(obj.Pair, obj.TimeFrame, obj.TimeStamp, obj.Candle.High, _count.ToString(),
                    (Icon) (_count % 2));
                _count++;
            }

            return Task.CompletedTask;
        }
    }

    private record TestStrategyFuturesThatCreateNotesOnEveryCandle : BaseStrategy<IFuturesConnector>
    {
        private static int _count;

        public override Task EventsCatalog_NewCandle(NewCandleEvent obj)
        {
            if (obj.TimeStamp == 0)
            {
                _count = 0;
            }

            if (_count == 0)
            {
                Status = StrategyStatus.Opened;
            }

            if (Status == StrategyStatus.Opened)
            {
                CreateNote(obj.Pair, obj.TimeFrame, obj.TimeStamp, obj.Candle.High, _count.ToString(),
                    (Icon) (_count % 2));
                _count++;
            }

            return Task.CompletedTask;
        }
    }

    private record TestStrategyCheckThatStrategyCanUseMarketOrders : BaseStrategy<ISpotConnector>
    {
        public override async Task EventsCatalog_NewCandle(NewCandleEvent obj)
        {
            if (obj.Candle.High >= 10)
                await Connector.CreateMarketOrder(obj.Pair, MarketOrder.Sell, 1);
            else
                await Connector.CreateMarketOrder(obj.Pair, MarketOrder.Buy, 1);
        }
    }

    private record TestStrategyCheckThatStrategyCanUseLimitOrders : BaseStrategy<ISpotConnector>
    {
        public static int OrderAmount;

        public override async Task EventsCatalog_NewCandle(NewCandleEvent obj)
        {
            if (OrderAmount < 7)
                await Connector.CreateLimitOrder(obj.Pair, LimitOrder.BuyLimit, new PlacementOrder(5, 1));
            else
                await Connector.CreateLimitOrder(obj.Pair, LimitOrder.SellLimit, new PlacementOrder(10, 1));
            OrderAmount++;
        }
    }

    private record TestStrategyCheckThatStrategyCanUseFuturesOrders : BaseStrategy<IFuturesConnector>
    {
        public override async Task EventsCatalog_NewCandle(NewCandleEvent obj)
        {
            if (obj.Candle.High >= 10)
                await Connector.CreateFuturesOrder(obj.Pair, FuturesOrder.Short, 10, new PlacementOrder(10, 1),
                    new PlacementOrder(4, 1), new PlacementOrder(12, 1));
            else
                await Connector.CreateFuturesOrder(obj.Pair, FuturesOrder.Long, 10, new PlacementOrder(5, 1),
                    new PlacementOrder(12, 1), new PlacementOrder(4, 1));
        }
    }

    public record TestStrategyEveryCandleSetStatusOpenedSpot : BaseStrategy<ISpotConnector>
    {
        public override Task EventsCatalog_NewCandle(NewCandleEvent obj)
        {
            Status = StrategyStatus.Opened;
            return Task.CompletedTask;
        }
    }

    private record TestStrategyEveryCandleSetStatusOpenedFutures : BaseStrategy<IFuturesConnector>
    {
        public override Task EventsCatalog_NewCandle(NewCandleEvent obj)
        {
            Status = StrategyStatus.Opened;
            return Task.CompletedTask;
        }
    }

    private record TestStrategyEveryCandleSetStatusClosed : BaseStrategy<ISpotConnector>
    {
        public override Task EventsCatalog_NewCandle(NewCandleEvent obj)
        {
            Status = StrategyStatus.Closed;
            return Task.CompletedTask;
        }
    }

    private record TestStrategy2 : BaseStrategy<ISpotConnector>
    {
        public override Task EventsCatalog_NewCandle(NewCandleEvent obj)
        {
            return Task.CompletedTask;
        }
    }

    private HistoricalDataSource _dataSource = null!;
    private IStrategyManagerRepository _strategyManagerRepository = null!;
    private MockedRemoteConnectorFactory _remoteConnectorFactory = null!;
    private List<Candle> _candles = new();
    private List<(Pair, MarketOrder, decimal)> _marketOrders = new();
    private List<(Pair, LimitOrder, PlacementOrder)> _limitOrders = new();
    private List<(Pair, FuturesOrder, int, PlacementOrder, PlacementOrder?, PlacementOrder?)> _futuresOrders = new();
    private static Random _rnd = new(12);
    private Mock<ICandleService> _candleServiceMock = null!;
    private TradingStrategyManagerStorage _tradingStrategyManagerStorage = null!;
    private TestingStrategyManagerStorage _testingStrategyManagerStorage = null!;
    private Mock<IRunStrategyRepository> _runStrategyRepository = null!;
    private IOrderRepository _orderRepository = null!;
    private INotesRepository _notesRepository = null!;
    private IChartRepository _chartRepository = null!;
}