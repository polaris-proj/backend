﻿using Domain.Connectors;
using Domain.StrategyProcessor;
using StrategyProcessor.Infrastructure;
using StrategyProcessor.Infrastructure.DAL.Repositories.RestoreAfterFailure;
using StrategyProcessor.Strategy;

namespace StrategyProcessor.Tests;

[NonParallelizable]
[TestFixture]
internal class StrategyManagerFactoryTests
{
    public StrategyManagerFactoryTests()
    {
        Mock<IStrategyManagerRepository> strategyManagerRepositoryMock = new();
        _strategyManagerFactory = new StrategyManagerFactory(strategyManagerRepositoryMock.Object, new DarkMagic());
    }


    [Test]
    public void Create_WithEmptyConnectors_ThrowsArgumentOutOfRangeException()
    {
        var runId = Guid.NewGuid();

        Action action = () => _strategyManagerFactory.Create<MyStrategy<ISpotConnector>, ISpotConnector>(
            runId,
            new List<StrategyParameter>(),
            new Dictionary<Exchange, ISpotConnector>(),
            new List<ExchangePairTimeFrame>(),
            null!,
            false);
        
        action.Should().Throw<ArgumentOutOfRangeException>();
    }

    [Test]
    public void Create_WithStoreInDatabase_True_CreatesStrategyManagerWithStoreInDb()
    {
        var runId = Guid.NewGuid();
        var binanceSpotConnectorMock = new Mock<ISpotConnector>();
        var connectors = new Dictionary<Exchange, ISpotConnector>
        {
            {Exchange.BinanceSpot, binanceSpotConnectorMock.Object}
        };
      var allowedSources = new [] {new ExchangePairTimeFrame(Exchange.BinanceSpot, new Pair("12","21"), TimeFrame.h1)};
         var strategyArtifactsStorageMock = new Mock<IChartDataStorage>();

        
        var strategyManager = _strategyManagerFactory.Create<MyStrategy<ISpotConnector>, ISpotConnector>(
            runId,
            new List<StrategyParameter>(),
            connectors,
            allowedSources,
            strategyArtifactsStorageMock.Object,
            true);
        
        strategyManager.Should().BeOfType<StrategyManagerWithStoreInDb<MyStrategy<ISpotConnector>, ISpotConnector>>();
    }

    [Test]
    public void Create_WithStoreInDatabase_False_CreatesStrategyManager()
    {
// Arrange
        var runId = Guid.NewGuid();
        var binanceSpotConnectorMock = new Mock<ISpotConnector>();
        var connectors = new Dictionary<Exchange, ISpotConnector>
        {
            {Exchange.BinanceSpot, binanceSpotConnectorMock.Object}
        };
        var allowedSources = new [] {new ExchangePairTimeFrame(Exchange.BinanceSpot, new Pair("12","21"), TimeFrame.h1)};
        var strategyArtifactsStorageMock = new Mock<IChartDataStorage>();
        
        var strategyManager = _strategyManagerFactory.Create<MyStrategy<ISpotConnector>, ISpotConnector>(
            runId,
            new List<StrategyParameter>(),
            connectors,
            allowedSources,
            strategyArtifactsStorageMock.Object,
            false);
        
        strategyManager.Should().BeOfType<StrategyManager<MyStrategy<ISpotConnector>, ISpotConnector>>();
    }

    internal record MyStrategy<TConnector> : BaseStrategy<TConnector> where TConnector : IConnector;

    private readonly IStrategyManagerFactory _strategyManagerFactory;
}