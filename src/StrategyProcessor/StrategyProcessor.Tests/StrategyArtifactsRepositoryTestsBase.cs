﻿using AutoFixture;
using Domain.StrategyProcessor;
using Infrastructure.Tests;
using StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Tests;

public abstract class StrategyArtifactsRepositoryTestsBase
{
    [SetUp]
    public virtual void Setup()
    {
        ChartRepositoryMock = new Mock<IChartRepository>();
        NotesRepositoryMock = new Mock<INotesRepository>();
        OrderRepositoryMock = new Mock<IOrderRepository>();
        RunStrategyRepositoryMock = new Mock<IRunStrategyRepository>();
        RunId = Guid.NewGuid();
        Fixture = CustomAutoDataAttribute.CreateFixture();
    }

    protected virtual IEnumerable<ChartDto> CreateExpectedCharts()
    {
        return Fixture.Build<ChartDto>()
            .With(x => x.Chart, Fixture.Build<Point>().CreateMany().ToList())
            .CreateMany();
    }

    protected virtual IEnumerable<Note> CreateExpectedNotes()
    {
        return Fixture.Build<Note>()
            .CreateMany(10);
    }

    protected virtual bool CompareChartDboLists(IEnumerable<ChartDto> actualCharts, IEnumerable<ChartDto> expectedCharts)
    {
        actualCharts.Should().BeEquivalentTo(expectedCharts);
        return true;
    }

    protected virtual bool CompareNoteDboLists(IEnumerable<Note> actualNotes, IEnumerable<Note> expectedNotes)
    {
        actualNotes.Should().BeEquivalentTo(expectedNotes);
        return true;
    }


    protected Guid RunId;
    protected IFixture Fixture = null!;
    protected Mock<IChartRepository> ChartRepositoryMock = null!;
    protected Mock<INotesRepository> NotesRepositoryMock = null!;
    protected Mock<IRunStrategyRepository> RunStrategyRepositoryMock = null!;
    protected Mock<IOrderRepository> OrderRepositoryMock = null!;
}