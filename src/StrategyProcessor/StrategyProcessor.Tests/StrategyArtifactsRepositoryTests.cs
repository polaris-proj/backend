﻿using AutoFixture;
using Domain.StrategyProcessor;
using StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;

namespace StrategyProcessor.Tests;

[TestFixture]
public class StrategyArtifactsRepositoryTests : StrategyArtifactsRepositoryTestsBase
{
    [SetUp]
    public override void Setup()
    {
        base.Setup();
        _storage = new ChartDataStorage(RunId, ChartRepositoryMock.Object, NotesRepositoryMock.Object);
    }

    [TearDown]
    public async Task TearDown()
    {
        await _storage.DisposeAsync();
    }

    [Test]
    public void AddChartDot_ShouldCallAddDotToChartAsync()
    {
        // Arrange
        var name = "ChartName";
        var description = "Chart Description";
        var pair = new Pair("dsds", "12");
        var timeFrame = TimeFrame.D1;
        var point = Fixture.Create<Point>();


        // Act
        _storage.AddChartDot(name, description, point, pair, timeFrame);

        // Assert
        ChartRepositoryMock.Verify(r => r.AddDotToChartAsync(RunId, name, description, pair, timeFrame,
                It.Is<Point>(x => x.TimeStamp == point.TimeStamp
                                  && x.Value == point.Value)),
            Times.Once);
    }

    [Test]
    public void CreateNote_ShouldAddNote()
    {
        // Arrange
        var timeStamp = 123456789;
        var price = 9.99M;
        var text = "NoteText";
        var pair = new Pair("dsds", "12");
        var timeFrame = TimeFrame.D1;
        var icon = Icon.Buy;

        // Act
        _storage.CreateNote(new Note()
        {
            Price = price,
            Text = text,
            TimeStamp = timeStamp,
            Pair = pair,
            TimeFrame = timeFrame,
            Icon = icon
        });

        // Assert
        NotesRepositoryMock.Verify(r => r.AddNotesAsync(
            It.Is<Guid>(x => x == RunId),
            It.Is<IReadOnlyList<Note>>(x => x.Count == 1
                                            && x[0].TimeStamp == timeStamp
                                            && x[0].Price == price
                                            && x[0].Text == text
                                            && x[0].Icon == icon
            )), Times.Once);
    }


    [Test]
    public async Task DisposeAsync_ShouldCompleteSuccessfully()
    {
        await _storage.DisposeAsync();
    }


    private ChartDataStorage _storage = null!;
}