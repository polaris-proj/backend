﻿using Domain.Connectors;
using Domain.StrategyProcessor;
using OrderConfirmation.Client;
using StrategyProcessor.Infrastructure.Connectors.ConnectorWithOrderConfirmation;

namespace StrategyProcessor.Tests.ConfiguratorsTests;

[Parallelizable(ParallelScope.Fixtures)]
public class SpotConnectorWithOrderConfirmationTests
{
    public SpotConnectorWithOrderConfirmationTests()
    {
        _connectorMock = new Mock<ISpotConnector>();
        _orderSenderClientMock = new Mock<IOrderSenderClient>();

        _spotConnectorWithOrderConfirmation = new SpotConnectorWithOrderConfirmation(
            _connectorMock.Object, _orderSenderClientMock.Object);
    }
    
    [OneTimeTearDown]
    public async Task OneTimeTearDown()
    {
        await _spotConnectorWithOrderConfirmation.DisposeAsync();
    }

    [TestCase(MarketOrder.Buy)]
    [TestCase(MarketOrder.Sell)]
    public async Task CreateMarketOrder_CallsOrderSenderClient(MarketOrder orderType)
    {
        // Arrange
        var pair = new Pair("BTC", "USDT");
        var amount = 10;
        var expectedOrderId = new OrderId("xyz123");

        _orderSenderClientMock.Setup(x => x.CreateMarketOrder(pair, orderType, amount))
            .ReturnsAsync(expectedOrderId);

        // Act
        var result = await _spotConnectorWithOrderConfirmation.CreateMarketOrder(pair, orderType, amount);

        // Assert
        result.Should().Be(expectedOrderId);
        _orderSenderClientMock.Verify(x => x.CreateMarketOrder(pair, orderType, amount), Times.Once);
    }

    [TestCase(LimitOrder.BuyLimit)]
    [TestCase(LimitOrder.SellLimit)]
    public async Task CreateLimitOrder_CallsOrderSenderClient(LimitOrder orderType)
    {
        // Arrange
        var pair = new Pair("BTC", "USDT");
        var entry = new PlacementOrder(12, 100);
        var expectedOrderId = new OrderId("xyz123");

        _orderSenderClientMock.Setup(x => x.CreateLimitOrder(pair, orderType, entry))
            .ReturnsAsync(expectedOrderId);

        // Act
        var result = await _spotConnectorWithOrderConfirmation.CreateLimitOrder(pair, orderType, entry);

        // Assert
        result.Should().Be(expectedOrderId);
        _orderSenderClientMock.Verify(x => x.CreateLimitOrder(pair, orderType, entry), Times.Once);
    }

    [Test]
    public async Task DisposeAsync_DisposeInnerConnector()
    {
        _connectorMock.Verify(x => x.DisposeAsync(), Times.Never);
        await _spotConnectorWithOrderConfirmation.DisposeAsync();
        _connectorMock.Verify(x => x.DisposeAsync(), Times.Once());
    }


    private readonly SpotConnectorWithOrderConfirmation _spotConnectorWithOrderConfirmation;
    private readonly Mock<IOrderSenderClient> _orderSenderClientMock;
    private readonly Mock<ISpotConnector> _connectorMock;
}