﻿using Domain.Connectors;
using Domain.StrategyProcessor;
using StrategyProcessor.Infrastructure.Connectors.ConnectorWithOrderLogger;
using StrategyProcessor.Tests.Common.InMemoryStorages;

namespace StrategyProcessor.Tests.ConfiguratorsTests;

public class FuturesConnectorWithOrderLoggerTests
{
    [Test]
    public async Task CreateFuturesOrder_CallsCreateFuturesOrderInConnector_AndLogsOrder()
    {
        // Arrange
        var pair = new Pair("BTC", "USDT");
        var orderType = FuturesOrder.Long;
        var amount = 1;
        var expectedOrderId = new OrderId("xyz123");
        var entry = new PlacementOrder(1, 1);
        var take = new PlacementOrder(1, 1);
        var stop = new PlacementOrder(1, 1);
        
        var runId = Guid.NewGuid();

        var connectorMock = new Mock<IFuturesConnector>();
        var orderRepository = new InMemoryOrderRepository();
        var connectorWithOrderLogger = new FuturesConnectorWithOrderLogger(connectorMock.Object, orderRepository, runId);

        connectorMock.Setup(x => x.CreateFuturesOrder(It.IsAny<Pair>(), It.IsAny<FuturesOrder>(), It.IsAny<int>(),
                It.IsAny<PlacementOrder>(), It.IsAny<PlacementOrder>(), It.IsAny<PlacementOrder>()))
            .ReturnsAsync(expectedOrderId);

        // Act
        var result = await connectorWithOrderLogger.CreateFuturesOrder(pair, orderType, 1, entry, take, stop);
        await connectorWithOrderLogger.DisposeAsync();

        // Assert
        result.Should().Be(expectedOrderId);
        connectorMock.Verify(x => x.CreateFuturesOrder(pair, orderType, 1, entry, take, stop), Times.Once);

        (await orderRepository.ReadAsync(runId, pair, TimeFrame.h1, TimeConstants.WholeDataTimeRange))
            .Count()
            .Should()
            .Be(1);
        var loggedOrder = (await orderRepository.ReadAsync(runId, pair, TimeFrame.h1, TimeConstants.WholeDataTimeRange))
            .First();

        loggedOrder.Should().NotBeNull();
        loggedOrder.Pair.Should().Be(pair);
        loggedOrder.OrderId.Should().Be(expectedOrderId.Id);
        loggedOrder.Amount.Should().Be(amount);
        loggedOrder.Type.Should().Be(OrderType.Long);
    }

    [Test]
    public async Task DisposeAsync_DisposeInnerConnector()
    {
        var runId = Guid.NewGuid();

        var connectorMock = new Mock<IFuturesConnector>();
        var orderRepository = new InMemoryOrderRepository();
        var connectorWithOrderLogger = new FuturesConnectorWithOrderLogger(connectorMock.Object, orderRepository, runId);
        
        connectorMock.Verify(x => x.DisposeAsync(), Times.Never);
        await connectorWithOrderLogger.DisposeAsync();
        connectorMock.Verify(x => x.DisposeAsync(), Times.Once());
    }
}