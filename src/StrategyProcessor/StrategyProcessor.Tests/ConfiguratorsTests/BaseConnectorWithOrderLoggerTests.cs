﻿using Domain.Connectors;
using StrategyProcessor.Infrastructure.Connectors.ConnectorWithOrderLogger;
using StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Tests.ConfiguratorsTests;

[Parallelizable(ParallelScope.Fixtures)]
public class BaseConnectorWithOrderLoggerTests
{
    [SetUp]
    public void Setup()
    {
        _connectorMock = new Mock<IConnector>();
    }

    [Test]
    public async Task LogOrder_CallsAddOrdersInTestOfStrategyRepository()
    {
        var order = new OrderDto {Pair = new Pair("BTC", "USDT")};
        var runId = Guid.NewGuid();

        var orderRepositoryMock = new Mock<IOrderRepository>();

        var baseConnectorWithOrderLogger = new BaseConnectorWithOrderLogger<IConnector>(
            _connectorMock.Object, orderRepositoryMock.Object, runId);

        await baseConnectorWithOrderLogger.LogOrder(order);

        orderRepositoryMock.Verify(x => x.AddOrdersAsync(
                It.Is<Guid>(y => y == runId),
                It.Is<List<OrderDto>>(o => o.Contains(order))),
            Times.Once);
    }

    [Test]
    public async Task CancelOrder_CallsCancelOrderInConnector_AndUpdatesOrderStatus()
    {
        var pair = new Pair("BTC", "USDT");
        var orderId = new OrderId("xyz123");

        _connectorMock.Setup(x => x.CancelOrder(pair, orderId)).Returns(Task.CompletedTask);
        var orderRepositoryMock = new Mock<IOrderRepository>();

        var baseConnectorWithOrderLogger = new BaseConnectorWithOrderLogger<IConnector>(
            _connectorMock.Object, orderRepositoryMock.Object, Guid.NewGuid());
        await baseConnectorWithOrderLogger.CancelOrder(pair, orderId);

        _connectorMock.Verify(x => x.CancelOrder(pair, orderId), Times.Once);
        orderRepositoryMock.Verify(x => x.CloseOrderAsync(orderId), Times.Once);
    }

    [Test]
    public async Task DisposeAsync_DisposeInnerConnector()
    {
        _connectorMock.Verify(x => x.DisposeAsync(), Times.Never);
        var orderRepositoryMock = new Mock<IOrderRepository>();

        var baseConnectorWithOrderLogger = new BaseConnectorWithOrderLogger<IConnector>(
            _connectorMock.Object, orderRepositoryMock.Object, Guid.NewGuid());

        await baseConnectorWithOrderLogger.DisposeAsync();
        _connectorMock.Verify(x => x.DisposeAsync(), Times.Once());
    }

    private Mock<IConnector> _connectorMock = null!;
}