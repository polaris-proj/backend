﻿using Domain.StrategyProcessor;
using Infrastructure.Extensions;
using StrategyProcessor.Infrastructure.DAL.Repositories.StrategyArtifacts;
using StrategyProcessor.Shared;

namespace StrategyProcessor.Tests.Common.InMemoryStorages;

public class InMemoryChartRepository : IChartRepository
{
    public Task AddChartsAsync(Guid runId, IEnumerable<ChartDto> chartDtos)
    {
        if (!_charts.ContainsKey(runId))
        {
            _charts[runId] = new List<ChartDto>();
        }

        _charts[runId].AddRange(chartDtos);
        return Task.CompletedTask;
    }

    public Task AddDotToChartAsync(Guid runId, string name, string? description, Pair pair, TimeFrame timeFrame,
        Point point)
    {
        if (_charts.TryGetValue(runId, out var chartList))
        {
            var chart = chartList.FirstOrDefault(c => c.Name == name && c.Pair == pair && c.TimeFrame == timeFrame);
            if (chart != null)
            {
                chart.Chart.Add(point);
            }
            else
            {
                throw new Exception("Chart not found");
            }
        }
        else
        {
            throw new Exception("RunId not found");
        }

        return Task.CompletedTask;
    }

    public Task<IEnumerable<ChartDto>> ReadAsync(Guid runId, Pair pair, TimeFrame timeFrame, DateTimeRange dateTimeRange)
    {
        if (_charts.TryGetValue(runId, out var chartList))
        {
            var charts = chartList.Where(c => c.Pair == pair && c.TimeFrame == timeFrame);
            var pointFilteredCharts = charts.Select(chart => new ChartDto
            {
                Name = chart.Name,
                Pair = chart.Pair,
                TimeFrame = chart.TimeFrame,
                Chart = chart.Chart.Where(point => dateTimeRange.Contains(point.TimeStamp.ToDateTime())).ToList()
            });
            return Task.FromResult(pointFilteredCharts);
        }

        return Task.FromResult(Enumerable.Empty<ChartDto>());
    }

    private readonly Dictionary<Guid, List<ChartDto>> _charts = new();
}