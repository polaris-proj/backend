﻿using Infrastructure.DIExtension;
using IODataModule.Client.Clients;
using StrategyProcessor.Tests.Common.InMemoryStorages;

namespace StrategyProcessor.Tests.Common;

public class MockedScopeFactory<T> : IFactory<T>
{
    internal readonly InMemoryRunStrategyRepository Repository = new();

    private readonly Dictionary<Type, object> _services;

    public MockedScopeFactory()
    {
        _services = new()
        {
            {typeof(ICandleService), new MockedCandleService().GetStorage()}
        };
    }


    public T Create()
    {
        if (_services.ContainsKey(typeof(T)))
            return (T) _services[typeof(T)];

        throw new NotImplementedException(typeof(T).FullName);
    }
}