﻿using Domain.Connectors;
using Domain.Patterns.Objects;
using Domain.StrategyProcessor;
using QuantConnect.Data.Market;
using QuantConnect.Indicators;
using static StrategyProcessor.Strategies.IndicatorDataPointToDomainDataPointMapper;
using MovingAverageType = QuantConnect.Indicators.MovingAverageType;

namespace StrategyProcessor.Strategies;

public record BollingerStrategy : BaseStrategy<IFuturesConnector>
{
    [StrategyParameter("period")] public int Period { get; set; } = 20;

    [StrategyParameter("deviation")] public decimal Deviation { get; set; } = 2;
    [StrategyParameter("takestopDelta")] public decimal TakestopDelta { get; set; } = 0.01m;

    public BollingerBands2 Bollinger = null!;
    private OrderId? _longOrder;
    private OrderId? _shortOrder;

    public override void Start()
    {
        Bollinger = new BollingerBands2(Period, Deviation);
    }

    public override async Task EventsCatalog_NewCandle(NewCandleEvent obj)
    {
        this.Update(Bollinger, "BB2", "Description", obj, OHLCV.Close);
        var total = GetOrderTotal();
        

        if (_longOrder is null || (await Connector.GetOrderInfo(obj.Pair, _longOrder)).OrderStatus != OrderStatus.Close)
        {
            var longPrice = Bollinger.UpperBand;
            var longAmount = total / longPrice;
            
            if(_longOrder is not null)
                await Connector.CancelOrder(obj.Pair, _longOrder);
            
            _longOrder = await Connector.CreateFuturesOrder(obj.Pair, FuturesOrder.Long, 1,
                    new PlacementOrder(longPrice, longAmount),
                    new PlacementOrder(longPrice * (1 + TakestopDelta), longAmount),
                    new PlacementOrder(longPrice * (1 - TakestopDelta), longAmount));
        }

        if (_shortOrder is null || (await Connector.GetOrderInfo(obj.Pair, _shortOrder)).OrderStatus != OrderStatus.Close)
        {
            var shortPrice = Bollinger.LowerBand;
            var shortAmount = total / shortPrice;
            
            if(_shortOrder is not null)
                await Connector.CancelOrder(obj.Pair, _shortOrder);
            
            _shortOrder = await Connector.CreateFuturesOrder(obj.Pair, FuturesOrder.Short, 1,
                    new PlacementOrder(shortPrice, shortAmount),
                    new PlacementOrder(shortPrice * (1 - TakestopDelta), shortAmount),
                    new PlacementOrder(shortPrice * (1 + TakestopDelta), shortAmount)
                );
        }
    }

    private static decimal GetOrderTotal()
    {
        return 100;
    }
}

public class BollingerBands2 : BarIndicator, IIndicatorWarmUpPeriodProvider
{
    public IndicatorBase<IndicatorDataPoint> StandardDeviation { get; }

    public IndicatorBase<IndicatorDataPoint> MiddleBand { get; }

    public IndicatorBase<IndicatorDataPoint> UpperBand { get; }

    public IndicatorBase<IndicatorDataPoint> LowerBand { get; }

    public IndicatorBase<IndicatorDataPoint> BandWidth { get; }

    public IndicatorBase<IndicatorDataPoint> PercentB { get; }

    public IndicatorBase<IndicatorDataPoint> Price { get; }


    public BollingerBands2(int period, decimal k, MovingAverageType movingAverageType = MovingAverageType.Simple)
        : this($"BB({period},{k})", period, k, movingAverageType)
    {
    }

    public BollingerBands2(string name, int period, decimal k,
        MovingAverageType movingAverageType = MovingAverageType.Simple)
        : base(name)
    {
        WarmUpPeriod = period;
        StandardDeviation = new StandardDeviation(name + "_StandardDeviation", period);
        MiddleBand = movingAverageType.AsIndicator(name + "_MiddleBand", period);
        LowerBand = MiddleBand.Minus(StandardDeviation.Times(k), name + "_LowerBand");
        UpperBand = MiddleBand.Plus(StandardDeviation.Times(k), name + "_UpperBand");
        var compositeIndicator = UpperBand.Minus(LowerBand);
        BandWidth = compositeIndicator.Over(MiddleBand)
            .Times(new ConstantIndicator<IndicatorDataPoint>("ct", 100M), name + "_BandWidth");
        Price = new Identity(name + "_Close");
        PercentB = Price.Minus(LowerBand).Over(compositeIndicator, name + "_%B");
    }

    public override bool IsReady => MiddleBand.IsReady && UpperBand.IsReady && LowerBand.IsReady &&
                                    BandWidth.IsReady && PercentB.IsReady;

    public int WarmUpPeriod { get; }

    protected override decimal ComputeNextValue(IBaseDataBar input)
    {
        var value = input.Close;

        StandardDeviation.Update(input.Time, value);
        MiddleBand.Update(input.Time, value);
        Price.Update(input.Time, value);
        return input.Value;
    }

    protected override IndicatorResult ValidateAndComputeNextValue(IBaseDataBar input)
    {
        var nextValue = ComputeNextValue(input);
        return !IsReady || StandardDeviation.Current.Value != 0M
            ? new IndicatorResult(nextValue)
            : new IndicatorResult(nextValue, IndicatorStatus.MathError);
    }

    public override void Reset()
    {
        StandardDeviation.Reset();
        MiddleBand.Reset();
        UpperBand.Reset();
        LowerBand.Reset();
        BandWidth.Reset();
        PercentB.Reset();
        base.Reset();
    }
}