﻿using Domain.Connectors;
using Domain.Patterns.Objects;
using Domain.StrategyProcessor;
using Infrastructure.Extensions;
using QuantConnect.Data;
using QuantConnect.Indicators;

namespace StrategyProcessor.Strategies;

internal static class IndicatorDataPointToDomainDataPointMapper
{
    internal static void Update<TStrategy, T>(
        this BaseStrategy<TStrategy> strategy,
        IndicatorBase<T> indicator,
        string indicatorName,
        string indicatorDescription,
        NewCandleEvent candle,
        OHLCV dataSource
    )
        where T : IBaseData
        where TStrategy : IConnector
    {
        if (indicator.Update(candle.TimeStamp.ToDateTime(), candle.GetValue(dataSource)))
        {
            strategy.ChartDataStorage.AddChartDot(indicatorName, indicatorDescription, indicator.Current.ToModel(),
                candle.Pair, candle.TimeFrame);
        }
    }

    private static Point ToModel(this IBaseData dataPoint)
    {
        return new Point(dataPoint.Time.ToMilliseconds(), dataPoint.Value);
    }

    private static decimal GetValue(this NewCandleEvent candleEvent, OHLCV dataSelector)
    {
        return dataSelector switch
        {
            OHLCV.Open => candleEvent.Candle.Open,
            OHLCV.High => candleEvent.Candle.High,
            OHLCV.Low => candleEvent.Candle.Low,
            OHLCV.Close => candleEvent.Candle.Close,
            OHLCV.Volume => candleEvent.Candle.Volume,
            _ => throw new ArgumentOutOfRangeException(nameof(dataSelector), dataSelector, null)
        };
    }

    internal enum OHLCV
    {
        Open,
        High,
        Low,
        Close,
        Volume
    }
}