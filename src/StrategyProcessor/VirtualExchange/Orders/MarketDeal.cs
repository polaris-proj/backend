using System;
using Domain;
using Domain.Connectors;
using Domain.StrategyProcessor;

namespace VirtualExchange.Orders;

internal sealed record MarketDeal : DealBase
{
    public MarketDeal(OrderType orderType, Account owner, decimal lastPrice, Pair pair, decimal amount)
        : base(owner, lastPrice, pair, new() {new Order(decimal.MaxValue, amount, orderType)})
    {
        FeeFactor = 0.05m / 100m;


        var order = new Order(lastPrice, amount, orderType)
        {
            OrderStatus = OrderStatus.Close,
            FilledAmount = amount
        };

        if (orderType == OrderType.Buy)
        {
            var priceWithFee = order.Amount * LastPrice * (1 + FeeFactor);

            if (Owner.CoinsAmount[Pair.Second].Free < priceWithFee)
                throw new Exception("No money");

            Owner.UpdateCoinsAmountFree(Pair.Second, -priceWithFee);
            Owner.UpdateCoinsAmountFree(Pair.First, order.Amount);
        }
        else
        {
            var price = order.Amount * LastPrice;
            if (Owner.CoinsAmount[Pair.First].Free < order.Amount)
                throw new Exception("Not enought coins to sell");

            Owner.UpdateCoinsAmountFree(Pair.First, -order.Amount);
            Owner.UpdateCoinsAmountFree(Pair.Second, price * (1 - FeeFactor));
        }

        EntryOrders.Add(order);
        DealStatus = DealStatus.Close;
    }
}