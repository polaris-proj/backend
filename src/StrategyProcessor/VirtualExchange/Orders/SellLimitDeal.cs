﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Connectors;
using Domain.StrategyProcessor;

namespace VirtualExchange.Orders;

internal record SellLimitDeal : DealBase
{
    protected sealed override decimal FeeFactor => 0.025m / 100m;

    public SellLimitDeal(OrderType orderType, Account owner, decimal lastPrice, Pair pair, List<Order> entry,
        List<Order>? takes = null, List<Order>? stops = null) : base(owner, lastPrice, pair, entry, takes, stops)
    {
        //todo можно добавить проверку, что если ордер тайп байлимит, то все стопы и тейки это селл лимиты
        // аналогично для селл лимит
        var usedMoney = entry.Select(x => x.Amount * x.Price).Sum();
        var totalAmount = entry.Select(x => x.Amount).Sum();
        Amount = totalAmount;

        if (orderType == OrderType.BuyLimit && owner.CoinsAmount[pair.Second].Free < usedMoney * (1 + FeeFactor)
            || orderType == OrderType.SellLimit && owner.CoinsAmount[pair.First].Free < totalAmount)
            throw new Exception("No money");


        owner.UpdateCoinsAmountFree(pair.Second, -usedMoney * FeeFactor);
        //TODO как-то странно, если у меня нет 2 монеты из пары, то я даже продать не могу

        owner.UpdateCoinsAmountFree(pair.First, -totalAmount);
        owner.UpdateCoinsAmountLock(pair.First, totalAmount);


        //todo зачем все эти ордера снова в ордера переводить?
        EntryOrders.AddRange(entry.Select(x => new Order(x.Price, x.Amount, orderType)));

        if (takes is not null)
            TakeOrders.AddRange(takes.Select(x => new Order(x.Price, x.Amount, OrderType.SellLimit)));

        if (stops is not null)
            StopOrders.AddRange(stops.Select(x => new Order(x.Price, x.Amount, OrderType.SellLimit)));
    }

    protected override void Buy(Order order)
    {
        var amount = order.Amount;
        // AveragePrice = (AveragePrice * Owner.CoinsAmount[Pair] + amount * deal.Price) /
        //                (Owner.CoinsAmount[Pair] + amount);
        Owner.UpdateCoinsAmountLock(Pair.Second, -amount * order.Price);
        Owner.UpdateCoinsAmountFree(Pair.First, amount);

        Balance -= amount * order.Price;
        Amount += amount;

        order.FilledAmount = amount;
        order.OrderStatus = OrderStatus.Close;
    }

    protected override void Sell(Order order)
    {
        var amount = order.Amount;
        // AveragePrice = (AveragePrice * Owner.CoinsAmount[Pair] + amount * deal.Price)
        //                (Owner.CoinsAmount[Pair] + amount);
        Owner.UpdateCoinsAmountLock(Pair.First, -amount);
        Owner.UpdateCoinsAmountFree(Pair.Second, amount * order.Price);

        Amount -= amount;
        Balance += amount * order.Price;

        order.FilledAmount = amount;
        order.OrderStatus = OrderStatus.Close;
    }

    public override void UpdateStatusOfOrder(decimal price)
    {
        if (DealStatus == DealStatus.Close) return;

        if (LastPrice == 0)
        {
            UpdateLastPrice(price);
            return;
        }

        UpdateAllDeals(price);

        if (EntryOrders.All(x => x.OrderStatus == OrderStatus.Close) &&
            StopOrders.All(x => x.OrderStatus == OrderStatus.Close) &&
            TakeOrders.All(x => x.OrderStatus == OrderStatus.Close) &&
            Amount < Eps)
        {
            DealStatus = DealStatus.Close;
            Owner.CloseDeal(Id);
            return;
        }

        UpdateLastPrice(price);
    }

    public override void CloseDeal()
    {
        DealStatus = DealStatus.Close;

        var totalAmount = EntryOrders.Where(x => x.OrderStatus == OrderStatus.Close)
            .Select(x => x.Amount)
            .Sum();

        Owner.UpdateCoinsAmountFree(Pair.First, totalAmount);
        Owner.UpdateCoinsAmountLock(Pair.First, -totalAmount);


        Owner.CloseDeal(Id);
    }
}