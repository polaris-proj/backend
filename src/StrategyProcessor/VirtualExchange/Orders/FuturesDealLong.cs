﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain;
using Domain.Connectors;
using Domain.StrategyProcessor;

namespace VirtualExchange.Orders;

internal sealed record FuturesDealLong : FuturesDealBase
{
    public FuturesDealLong(Account owner, decimal lastPrice, Pair pair, List<Order> entry, int leverage,
        List<Order>? takes = null, List<Order>? stops = null)
        : base(owner, lastPrice, pair, entry, takes, stops)
    {
        if (leverage < 1)
            throw new Exception("Leverage should be more when 1");

        var totalSum = entry.Sum(x => x.Price * x.Amount);
        if (Owner.CoinsAmount[pair.Second].Free * leverage < totalSum * (1 + FeeFactor))
            throw new Exception("Not enough money");

        Leverage = leverage;
        StartBalance = totalSum;
        Balance = totalSum;

        var usedMoney = StartBalance / Leverage;

        Owner.UpdateCoinsAmountFree(pair.Second, -usedMoney * (1 + FeeFactor));

        EntryOrders.AddRange(entry.Select(x => new Order(x.Price, x.Amount, OrderType.BuyLimit)));

        if (takes is not null)
            TakeOrders.AddRange(takes.Select(x => new Order(x.Price, x.Amount, OrderType.SellLimit)));

        if (stops is not null)
            StopOrders.AddRange(stops.Select(x => new Order(x.Price, x.Amount, OrderType.SellLimit)));
    }

    public override void UpdateStatusOfOrder(decimal price)
    {
        if (DealStatus == DealStatus.Close) return;

        if (LastPrice == 0)
        {
            UpdateLastPrice(price);
            return;
        }

        UpdateAllDeals(price);

        if (Math.Abs(Amount) < Eps && EntryOrders.All(x => x.OrderStatus == OrderStatus.Close))
        {
            CloseDeal();
            return;
        }

        if (LiquidationPrice >= price && Amount > 0) //Liquidation
        {
            Amount = 0;
            DealStatus = DealStatus.Liquidated;
            Owner.CloseDeal(Id);
            return;
        }

        UpdateLastPrice(price);
    }

    protected override void Buy(Order order)
    {
        var amount = order.Amount;

        order.FilledAmount = amount;
        AveragePrice = (AveragePrice * Amount + amount * order.Price) / (Amount + amount);
        Amount += amount;
        Balance -= amount * order.Price;

        var totalSpend = Math.Abs(AveragePrice * Amount);

        if (Math.Abs(Amount) < Eps)
            LiquidationPrice = decimal.MinValue;
        else
            LiquidationPrice = (totalSpend - totalSpend / Leverage) / Math.Abs(Amount);
        order.OrderStatus = OrderStatus.Close;
    }

    protected override void Sell(Order order)
    {
        if (Amount <= 0) return;

        var amount = order.Amount <= Amount ? order.Amount : Amount;
        order.FilledAmount = amount;
        Amount -= amount;
        Balance += order.Price * amount;
        order.OrderStatus = OrderStatus.Close;
    }

    public override void CloseDeal()
    {
        Owner.UpdateCoinsAmountFree(Pair.Second, Balance - StartBalance + StartBalance / Leverage);
        DealStatus = DealStatus.Close;
        Owner.CloseDeal(Id);
    }
}