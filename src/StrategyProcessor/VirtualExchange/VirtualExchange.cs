﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Connectors;
using Domain.Patterns.Objects;
using Infrastructure.EventDriven;

namespace VirtualExchange;

public class VirtualExchange
{
    public VirtualExchange(IConnector connector, IDataSource inputDataSource)
    {
        _connector = connector;
        inputDataSource.RegisterEventDestination<NewCandleEvent>(EventsCatalog_NewCandle);
    }

    //todo по умному распределять данные по аккаунтам, только тогда когда он нужен опред аккаунту,

    //т.е между стратегией и аккаунтом должна быть обратная связь

    private Task EventsCatalog_NewCandle(NewCandleEvent obj)
    {
        //todo надо как-то по более умному эмулировать движение цены за этот промежуток
        foreach (var acc in Accounts)
            acc.DataReceiver(obj); //todo много аллоки

        return Task.CompletedTask;
    }

    internal VirtualExchangeConnector CreateConnector(string name, Ticker currency, decimal startBalance)
    {
        var account = new Account(name, currency, startBalance);
        lock (Accounts)
        {
            Accounts.Add(account);
        }

        return new VirtualExchangeConnector(_connector, account);
    }

    public ISpotConnector CreateSpotConnector(string name, IEnumerable<Balance> initialBalance)
    {
        var account = new Account(name, initialBalance);
        lock (Accounts)
        {
            Accounts.Add(account);
        }

        return new VirtualExchangeConnector(_connector, account);
    }

    public IFuturesConnector CreateFuturesConnector(string name, IEnumerable<Balance> initialBalance)
    {
        var account = new Account(name, initialBalance);
        lock (Accounts)
        {
            Accounts.Add(account);
        }

        return new VirtualExchangeConnector(_connector, account);
    }

    private List<Account> Accounts { get; } = new();
    private readonly IConnector _connector;
}