﻿using System.Net;
using System.Net.Http.Json;
using FluentAssertions;
using NUnit.Framework;
using WebApi.Models.InputModel;
using WebApi.Models.ViewModel;

namespace WebApi.FunctionalTests;

public class AuthControllerTests : BaseTests
{
    [Test]
    public async Task Register_ShouldSucceed()
    {
        var content = JsonContent.Create(GenerateRegisterData());

        using var client = NoAuthHttpClient;
        
        var resp = await client.PostAsync("register", content);

        resp.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    
    [TestCase("")]
    [TestCase("4sym")]
    [TestCase("12345678912345678912345678912345")]
    public async Task Register_ShouldFailed422_WhenBadLogin(string login)
    {
        var registerData = GenerateRegisterData();
        registerData.Login = login;

        var content = JsonContent.Create(registerData);
        
        using var client = NoAuthHttpClient;

        var resp = await client.PostAsync("register", content);

        resp.StatusCode.Should().Be(HttpStatusCode.PreconditionFailed);
    }
    
    [TestCase("")]
    [TestCase("just_string")]
    public async Task Register_ShouldFailed422_WhenBadEmail(string email)
    {
        var registerData = GenerateRegisterData();
        registerData.Email = email;

        var content = JsonContent.Create(registerData);
        
        using var client = NoAuthHttpClient;

        var resp = await client.PostAsync("register", content);

        resp.StatusCode.Should().Be(HttpStatusCode.PreconditionFailed);
    }
    
    [TestCase("short")]
    [TestCase("")]
    public async Task Register_ShouldFailed422_WhenBadPassword(string password)
    {
        var registerData = GenerateRegisterData();
        registerData.Password = password;
        
        var content = JsonContent.Create(registerData);

        using var client = NoAuthHttpClient;
        
        var resp = await client.PostAsync("register", content);

        resp.StatusCode.Should().Be(HttpStatusCode.PreconditionFailed);
    }
    
    [Theory]
    public async Task Register_ShouldFailed400_WhenNoField(bool hasPass, bool hasLogin, bool hasEmail)
    {
        if (hasPass && hasEmail && hasLogin)
        {
            return;
        }
        
        var registerData = GenerateRegisterData();

        if (!hasPass)
        {
            registerData.Password = null;
        }
        
        if (!hasEmail)
        {
            registerData.Email = null;
        }
        
        if (!hasLogin)
        {
            registerData.Login = null;
        }
        
        var content = JsonContent.Create(registerData);
        
        using var client = NoAuthHttpClient;

        var resp = await client.PostAsync("register", content);

        resp.StatusCode.Should().Be(HttpStatusCode.BadRequest);
    }
    
    [Theory]
    public async Task Register_ShouldFailed403_WhenUserExists(bool sameLogin, bool sameEmail)
    {
        if (!sameEmail || !sameLogin)
        {
            return;
        }

        var existingRegister = await CreateUser();

        var otherRegister = GenerateRegisterData();

        if (sameEmail)
        {
            otherRegister.Email = existingRegister.Email;
        }

        if (sameLogin)
        {
            otherRegister.Login = existingRegister.Login;
        }

        var otherContent = JsonContent.Create(otherRegister);
        
        using var client = NoAuthHttpClient;
        
        var resp = await client.PostAsync("register", otherContent);

        resp.StatusCode.Should().Be(HttpStatusCode.Forbidden);
    }

    [Test]
    public async Task Login_ShouldSuccess()
    {
        var registerData = await CreateUser();

        var loginData = new LoginData
        {
            Login = registerData.Login!,
            Password = registerData.Password!,
        };

        var content = JsonContent.Create(loginData);
        
        using var client = NoAuthHttpClient;

        var resp = await client.PostAsync("login", content);

        resp.StatusCode.Should().Be(HttpStatusCode.OK);

        var body = await resp.Content.ReadFromJsonAsync<LoginResult>();

        body.Should().NotBeNull();
        body!.Login.Should().Be(loginData.Login);
    }
    
    [Theory]
    public async Task  Login_ShouldFail403_WhenNoField(bool hasLogin, bool hasPass)
    {
        if (hasPass && hasLogin)
        {
            return;
        }
        
        var registerData = await CreateUser();

        var loginData = new LoginData
        {
            Login = hasLogin ? registerData.Login : null,
            Password = hasPass ? registerData.Password : null,
        };

        var content = JsonContent.Create(loginData);
        
        using var client = NoAuthHttpClient;
        
        var resp = await client.PostAsync("login", content);

        resp.StatusCode.Should().Be(HttpStatusCode.Forbidden);
    }
    
    [Test]
    public async Task Login_ShouldFail403_WhenNoUserExists()
    {
        var registerData = GenerateRegisterData();

        var loginData = new LoginData
        {
            Login = registerData.Login,
            Password = registerData.Password,
        };

        var content = JsonContent.Create(loginData);
        
        using var client = NoAuthHttpClient;

        var resp = await client.PostAsync("login", content);

        resp.StatusCode.Should().Be(HttpStatusCode.Forbidden);
    }

    [Test]
    public async Task Logout_ShouldFail401_WhenNoAuth()
    {
        using var client = NoAuthHttpClient;
        
        var resp = await client.PostAsync("logout", null);

        resp.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
    }

    [Test]
    public async Task Logout_ShouldSuccess()
    {
        var (_, login) = await CreateUserAndLogin();

        using var client = GetAuthClient(login.AccessToken);
        
        var resp = await client.PostAsync("logout", null);

        resp.StatusCode.Should().Be(HttpStatusCode.OK);
    }
    
    private async Task<(RegisterData, LoginResult)> CreateUserAndLogin()
    {
        var user = await CreateUser();
        
        var loginData = new LoginData
        {
            Login = user.Login!,
            Password = user.Password!,
        };

        var content = JsonContent.Create(loginData);

        using var client = NoAuthHttpClient;
        
        var resp = await client.PostAsync("login", content);

        resp.StatusCode.Should().Be(HttpStatusCode.OK);

        var body = await resp.Content.ReadFromJsonAsync<LoginResult>();

        body.Should().NotBeNull();
        
        return (user, body!);
    }

    private async Task<RegisterData> CreateUser()
    {
        var registerData = GenerateRegisterData();
        
        var content = JsonContent.Create(registerData);
        
        using var client = NoAuthHttpClient;

        var resp = await client.PostAsync("register", content);

        resp.StatusCode.Should().Be(HttpStatusCode.OK);

        return registerData;
    }
    
    private int UserCount = 0;
    
    private RegisterData GenerateRegisterData()
    {
        var username = $"auth-test{UserCount++}";
        
        return new RegisterData()
        {
            Login = username,
            Email = $"{username}@mail.ru",
            Password = Guid.NewGuid().ToString()
        };
    }
}