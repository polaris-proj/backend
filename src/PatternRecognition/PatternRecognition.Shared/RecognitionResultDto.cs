﻿
// ReSharper disable InconsistentNaming

namespace PatternRecognition.Shared;

public class RecognitionResultDto
{
    public CandleDto[]? used_candles{ get; set; }
    public Dictionary<string, float>? pattern_to_proba { get; set; }
    public string most_probable_pattern { get; set; }
    public bool is_successful { get; set; }
    public string? fail_reason { get; set; }
}