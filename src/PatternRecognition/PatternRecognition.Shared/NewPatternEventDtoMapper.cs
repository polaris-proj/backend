﻿using Domain;
using Domain.Patterns.Objects;

namespace PatternRecognition.Shared;

public static class NewPatternEventDtoMapper
{
    public static NewPatternEvent Map(this NewPatternEventDto newPatternEventDto)
    {
        var patternToProba = newPatternEventDto.recognition_result.pattern_to_proba?
            .ToDictionary(x => Enum.Parse<PatternType>(x.Key), x => x.Value)
            ?? new Dictionary<PatternType, float>();


        return new NewPatternEvent
        {
            Exchange = (Exchange) newPatternEventDto.exchange,
            Pair = new Pair(newPatternEventDto.pair.first, newPatternEventDto.pair.second),
            TimeFrame = (TimeFrames.TimeFrame) newPatternEventDto.timeframe,
            TimeStamp = newPatternEventDto.timestamp,
            RecognitionResult = new RecognitionResult
            {
                UsedCandles = (newPatternEventDto.recognition_result.used_candles ?? []).Select(x=>x.ToDomain()).ToArray(),
                PatternToProba = patternToProba,
                MostProbablePattern = Enum.Parse<PatternType>(newPatternEventDto.recognition_result.most_probable_pattern),
                IsSuccessful = newPatternEventDto.recognition_result.is_successful,
                FailReason = newPatternEventDto.recognition_result.fail_reason,
            },
        };
    }
    
    private static Candle ToDomain(this CandleDto candleDto)
    {
        return new Candle()
        {
            TimeStamp = candleDto.timestamp,
            Open = candleDto.open,
            High = candleDto.high,
            Low = candleDto.low,
            Close = candleDto.close,
            Volume = candleDto.volume
        };
    }
}
