from google.protobuf import empty_pb2 as _empty_pb2
from protobuf_net import bcl_pb2 as _bcl_pb2
from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class Exchange(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = ()
    BinanceSpot: _ClassVar[Exchange]
    BinanceSpotTest: _ClassVar[Exchange]
    BinanceFutures: _ClassVar[Exchange]
    BinanceFuturesTest: _ClassVar[Exchange]
    Poloniex: _ClassVar[Exchange]

class TimeFrame(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = ()
    Unknown: _ClassVar[TimeFrame]
    m1: _ClassVar[TimeFrame]
    m3: _ClassVar[TimeFrame]
    m5: _ClassVar[TimeFrame]
    m15: _ClassVar[TimeFrame]
    m30: _ClassVar[TimeFrame]
    h1: _ClassVar[TimeFrame]
    h4: _ClassVar[TimeFrame]
    h12: _ClassVar[TimeFrame]
    D1: _ClassVar[TimeFrame]
    D3: _ClassVar[TimeFrame]
    W1: _ClassVar[TimeFrame]
    Mo1: _ClassVar[TimeFrame]
BinanceSpot: Exchange
BinanceSpotTest: Exchange
BinanceFutures: Exchange
BinanceFuturesTest: Exchange
Poloniex: Exchange
Unknown: TimeFrame
m1: TimeFrame
m3: TimeFrame
m5: TimeFrame
m15: TimeFrame
m30: TimeFrame
h1: TimeFrame
h4: TimeFrame
h12: TimeFrame
D1: TimeFrame
D3: TimeFrame
W1: TimeFrame
Mo1: TimeFrame

class Candle(_message.Message):
    __slots__ = ("TimeStamp", "Open", "High", "Low", "Close", "Volume")
    TIMESTAMP_FIELD_NUMBER: _ClassVar[int]
    OPEN_FIELD_NUMBER: _ClassVar[int]
    HIGH_FIELD_NUMBER: _ClassVar[int]
    LOW_FIELD_NUMBER: _ClassVar[int]
    CLOSE_FIELD_NUMBER: _ClassVar[int]
    VOLUME_FIELD_NUMBER: _ClassVar[int]
    TimeStamp: int
    Open: _bcl_pb2.Decimal
    High: _bcl_pb2.Decimal
    Low: _bcl_pb2.Decimal
    Close: _bcl_pb2.Decimal
    Volume: _bcl_pb2.Decimal
    def __init__(self, TimeStamp: _Optional[int] = ..., Open: _Optional[_Union[_bcl_pb2.Decimal, _Mapping]] = ..., High: _Optional[_Union[_bcl_pb2.Decimal, _Mapping]] = ..., Low: _Optional[_Union[_bcl_pb2.Decimal, _Mapping]] = ..., Close: _Optional[_Union[_bcl_pb2.Decimal, _Mapping]] = ..., Volume: _Optional[_Union[_bcl_pb2.Decimal, _Mapping]] = ...) -> None: ...

class NewCandleEvent(_message.Message):
    __slots__ = ("Candle", "Pair", "TimeFrame", "Exchange", "TimeStamp")
    CANDLE_FIELD_NUMBER: _ClassVar[int]
    PAIR_FIELD_NUMBER: _ClassVar[int]
    TIMEFRAME_FIELD_NUMBER: _ClassVar[int]
    EXCHANGE_FIELD_NUMBER: _ClassVar[int]
    TIMESTAMP_FIELD_NUMBER: _ClassVar[int]
    Candle: Candle
    Pair: Pair
    TimeFrame: TimeFrame
    Exchange: Exchange
    TimeStamp: int
    def __init__(self, Candle: _Optional[_Union[Candle, _Mapping]] = ..., Pair: _Optional[_Union[Pair, _Mapping]] = ..., TimeFrame: _Optional[_Union[TimeFrame, str]] = ..., Exchange: _Optional[_Union[Exchange, str]] = ..., TimeStamp: _Optional[int] = ...) -> None: ...

class Pair(_message.Message):
    __slots__ = ("First", "Second")
    FIRST_FIELD_NUMBER: _ClassVar[int]
    SECOND_FIELD_NUMBER: _ClassVar[int]
    First: Ticker
    Second: Ticker
    def __init__(self, First: _Optional[_Union[Ticker, _Mapping]] = ..., Second: _Optional[_Union[Ticker, _Mapping]] = ...) -> None: ...

class Ticker(_message.Message):
    __slots__ = ("Name",)
    NAME_FIELD_NUMBER: _ClassVar[int]
    Name: str
    def __init__(self, Name: _Optional[str] = ...) -> None: ...
