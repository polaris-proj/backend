from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class TimeSpan(_message.Message):
    __slots__ = ("value", "scale")
    class TimeSpanScale(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
        __slots__ = ()
        DAYS: _ClassVar[TimeSpan.TimeSpanScale]
        HOURS: _ClassVar[TimeSpan.TimeSpanScale]
        MINUTES: _ClassVar[TimeSpan.TimeSpanScale]
        SECONDS: _ClassVar[TimeSpan.TimeSpanScale]
        MILLISECONDS: _ClassVar[TimeSpan.TimeSpanScale]
        TICKS: _ClassVar[TimeSpan.TimeSpanScale]
        MINMAX: _ClassVar[TimeSpan.TimeSpanScale]
    DAYS: TimeSpan.TimeSpanScale
    HOURS: TimeSpan.TimeSpanScale
    MINUTES: TimeSpan.TimeSpanScale
    SECONDS: TimeSpan.TimeSpanScale
    MILLISECONDS: TimeSpan.TimeSpanScale
    TICKS: TimeSpan.TimeSpanScale
    MINMAX: TimeSpan.TimeSpanScale
    VALUE_FIELD_NUMBER: _ClassVar[int]
    SCALE_FIELD_NUMBER: _ClassVar[int]
    value: int
    scale: TimeSpan.TimeSpanScale
    def __init__(self, value: _Optional[int] = ..., scale: _Optional[_Union[TimeSpan.TimeSpanScale, str]] = ...) -> None: ...

class DateTime(_message.Message):
    __slots__ = ("value", "scale", "kind")
    class TimeSpanScale(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
        __slots__ = ()
        DAYS: _ClassVar[DateTime.TimeSpanScale]
        HOURS: _ClassVar[DateTime.TimeSpanScale]
        MINUTES: _ClassVar[DateTime.TimeSpanScale]
        SECONDS: _ClassVar[DateTime.TimeSpanScale]
        MILLISECONDS: _ClassVar[DateTime.TimeSpanScale]
        TICKS: _ClassVar[DateTime.TimeSpanScale]
        MINMAX: _ClassVar[DateTime.TimeSpanScale]
    DAYS: DateTime.TimeSpanScale
    HOURS: DateTime.TimeSpanScale
    MINUTES: DateTime.TimeSpanScale
    SECONDS: DateTime.TimeSpanScale
    MILLISECONDS: DateTime.TimeSpanScale
    TICKS: DateTime.TimeSpanScale
    MINMAX: DateTime.TimeSpanScale
    class DateTimeKind(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
        __slots__ = ()
        UNSPECIFIED: _ClassVar[DateTime.DateTimeKind]
        UTC: _ClassVar[DateTime.DateTimeKind]
        LOCAL: _ClassVar[DateTime.DateTimeKind]
    UNSPECIFIED: DateTime.DateTimeKind
    UTC: DateTime.DateTimeKind
    LOCAL: DateTime.DateTimeKind
    VALUE_FIELD_NUMBER: _ClassVar[int]
    SCALE_FIELD_NUMBER: _ClassVar[int]
    KIND_FIELD_NUMBER: _ClassVar[int]
    value: int
    scale: DateTime.TimeSpanScale
    kind: DateTime.DateTimeKind
    def __init__(self, value: _Optional[int] = ..., scale: _Optional[_Union[DateTime.TimeSpanScale, str]] = ..., kind: _Optional[_Union[DateTime.DateTimeKind, str]] = ...) -> None: ...

class NetObjectProxy(_message.Message):
    __slots__ = ("existingObjectKey", "newObjectKey", "existingTypeKey", "newTypeKey", "typeName", "payload")
    EXISTINGOBJECTKEY_FIELD_NUMBER: _ClassVar[int]
    NEWOBJECTKEY_FIELD_NUMBER: _ClassVar[int]
    EXISTINGTYPEKEY_FIELD_NUMBER: _ClassVar[int]
    NEWTYPEKEY_FIELD_NUMBER: _ClassVar[int]
    TYPENAME_FIELD_NUMBER: _ClassVar[int]
    PAYLOAD_FIELD_NUMBER: _ClassVar[int]
    existingObjectKey: int
    newObjectKey: int
    existingTypeKey: int
    newTypeKey: int
    typeName: str
    payload: bytes
    def __init__(self, existingObjectKey: _Optional[int] = ..., newObjectKey: _Optional[int] = ..., existingTypeKey: _Optional[int] = ..., newTypeKey: _Optional[int] = ..., typeName: _Optional[str] = ..., payload: _Optional[bytes] = ...) -> None: ...

class Guid(_message.Message):
    __slots__ = ("lo", "hi")
    LO_FIELD_NUMBER: _ClassVar[int]
    HI_FIELD_NUMBER: _ClassVar[int]
    lo: int
    hi: int
    def __init__(self, lo: _Optional[int] = ..., hi: _Optional[int] = ...) -> None: ...

class Decimal(_message.Message):
    __slots__ = ("lo", "hi", "signScale")
    LO_FIELD_NUMBER: _ClassVar[int]
    HI_FIELD_NUMBER: _ClassVar[int]
    SIGNSCALE_FIELD_NUMBER: _ClassVar[int]
    lo: int
    hi: int
    signScale: int
    def __init__(self, lo: _Optional[int] = ..., hi: _Optional[int] = ..., signScale: _Optional[int] = ...) -> None: ...
