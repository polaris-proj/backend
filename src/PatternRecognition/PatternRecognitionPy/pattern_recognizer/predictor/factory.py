import os

import torch

from pattern_recognizer.predictor.network.bp import create_perceptron_classifier
from pattern_recognizer.predictor.network.rbm import RBM
from pattern_recognizer.predictor.recognizer import PatternRecognizer
from resources import RESOURCES_FOLDER

_NUM_POINTS = int(os.getenv("PATTERN_RECOGNITION_NUM_POINTS", "6"))
_EMBEDDING_SIZE = int(os.getenv("PATTERN_RECOGNITION_EMBEDDING_SIZE", "10"))
_NUM_CLASSES = int(os.getenv("PATTERN_RECOGNITION_NUM_CLASSES", "5"))


def create_recognizer(
        num_points: int = _NUM_POINTS,
        num_classes: int = _NUM_CLASSES,
        map_location: str = "cpu",
) -> PatternRecognizer:
    models: list[RBM] = []

    for i in range(2):
        model = RBM(n_vis=12, n_hid=10) if i == 0 else RBM(10, 10)
        with (RESOURCES_FOLDER / f'RBM_layer{i}.pt').open('rb') as f:
            model.load_state_dict(torch.load(f, map_location=map_location))
        models.append(model)

    classifier = create_perceptron_classifier(models, num_classes=num_classes)
    sd = torch.load(RESOURCES_FOLDER / "classifier.pt", map_location=map_location)
    classifier.load_state_dict(sd, strict=False, assign=True)

    return PatternRecognizer(
        classifier=classifier,
        num_points=num_points,
        device=map_location,
    )
