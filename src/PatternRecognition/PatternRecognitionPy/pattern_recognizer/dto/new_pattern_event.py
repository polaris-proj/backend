import dataclasses

from grpc_python_server.ITest_pb2 import Exchange, TimeFrame
from pattern_recognizer.models.pair import Pair
from pattern_recognizer.predictor.data_models import RecognitionResult

# при изменении этого файла, не забудьте обновить его в src/PatternRecognition/PatternRecognition.Shared/...

@dataclasses.dataclass
class NewPatternEvent:
    exchange: Exchange
    pair: Pair
    timeframe: TimeFrame
    timestamp: int
    recognition_result: RecognitionResult
