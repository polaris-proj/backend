import dataclasses

from grpc_python_server.ITest_pb2 import Exchange, TimeFrame
from pattern_recognizer.models.candle import Candle
from pattern_recognizer.models.pair import Pair


@dataclasses.dataclass
class NewCandleEvent:
    exchange: Exchange
    pair: Pair
    timeframe: TimeFrame
    candle: Candle
