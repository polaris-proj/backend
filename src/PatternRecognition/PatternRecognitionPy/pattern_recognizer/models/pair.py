import dataclasses


@dataclasses.dataclass
class Pair:
    first: str
    second: str

    def __str__(self):
        return f"{self.first}/{self.second}"

    def __hash__(self):
        return hash((self.first, self.second))
