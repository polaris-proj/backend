from confluent_kafka import Consumer
from google.protobuf.message import DecodeError

from grpc_python_server.ITest_pb2 import (
    NewCandleEvent as ProtobufNewCandleEvent,
)
from pattern_recognizer.mappers.new_candle_event_mapper import NewCandleEventMapper
from pattern_recognizer.models.new_candle_event import (
    NewCandleEvent as InternalNewCandleEvent,
)


class CandleKafkaReader:
    def __init__(self, conf):
        self.consumer = Consumer(conf)
        self.running = False
        self.handler = None

    def start(self, handler):
        self.running = True
        self.consumer.subscribe(["Candle"]) #CandlesForLoadPatterns
        self.consumer.subscribe(["CandlesForLoadPatterns"])
        self.handler = handler
        self.read_message()

    def stop(self):
        self.running = False
        self.consumer.close()

    def read_message(self):
        try:
            while self.running:
                msg = self.consumer.poll(timeout=1.0)
                if msg is None or msg.error():
                    continue
                else:
                    decoded = self.decode_protobuf_message(msg)
                    if decoded is not None:
                        self.handler(decoded)

        finally:
            self.stop()

    def decode_protobuf_message(self, msg) -> InternalNewCandleEvent | None:
        try:
            candle_event = ProtobufNewCandleEvent()
            candle_event.ParseFromString(msg.value())
            return NewCandleEventMapper.map(candle_event)
        except DecodeError:
            print("Failed to decode Protobuf message")
            return None
