from collections import deque
from typing import NamedTuple, Any

from grpc_python_server.ITest_pb2 import Exchange, TimeFrame
from pattern_recognizer.models.pair import Pair


class EventKey(NamedTuple):
    exchange: Exchange
    pair: Pair
    timeframe: TimeFrame


class EventWindow:
    def __init__(self, window_size: int):
        self.window_size = window_size
        self.data: dict[EventKey, deque] = {}

    def add_event(self, key: EventKey, event: Any) -> None:
        if key not in self.data:
            self.data[key] = deque(maxlen=self.window_size)
        self.data[key].append(event)

    def get_events(self, key: EventKey) -> list:
        return list(self.data.get(key, []))

    def remove_oldest(self, key: EventKey) -> None:
        window = self.data.get(key)
        if window is not None:
            window.popleft()

    def leave_only_latest(self, key: EventKey) -> None:
        window = self.data.get(key)
        if window is not None:
            self.data[key] = deque([window[-1]], maxlen=self.window_size)
