import multiprocessing
import os

from flask import Flask
from pattern_recognizer.server.kafka_consumer import run_consumer

app = Flask(__name__)


@app.route('/health', methods=['GET'])
def health_check():
    return "Service is running", 200


def run_health_check():
    port = int(os.getenv("apiPort", 5000))
    app.run(host='0.0.0.0', port=port)


if __name__ == "__main__":
    print("Starting service")
    # Start Flask app in a separate process
    flask_process = multiprocessing.Process(target=run_health_check)
    flask_process.start()

    # Start Kafka consumer
    run_consumer()

    # Join Flask process
    flask_process.join()
