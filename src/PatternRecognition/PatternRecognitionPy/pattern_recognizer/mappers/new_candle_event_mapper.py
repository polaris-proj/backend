from grpc_python_server.ITest_pb2 import (
    NewCandleEvent as ProtobufNewCandleEvent,
)
from pattern_recognizer.mappers.candle_mapper import CandleMapper
from pattern_recognizer.mappers.pair_mapper import PairMapper
from pattern_recognizer.models.new_candle_event import (
    NewCandleEvent as InternalNewCandleEvent,
)


class NewCandleEventMapper:
    @staticmethod
    def map(event: ProtobufNewCandleEvent) -> InternalNewCandleEvent:
        return InternalNewCandleEvent(
            exchange=event.Exchange,
            pair=PairMapper.map(event.Pair),
            timeframe=event.TimeFrame,
            candle=CandleMapper.map(event.Candle),
        )
