import pytest

from pattern_recognizer.predictor.data_models import ShortCandle
from pattern_recognizer.predictor.factory import create_recognizer
from pattern_recognizer.predictor.recognizer import PatternRecognizer


@pytest.fixture
def pattern_recognizer() -> PatternRecognizer:
    return create_recognizer()


@pytest.mark.parametrize(
    "candles",
    [
        [(0.0, 0.5), (0.1, 0.4), (0.3, 0.0), (0.6, 0.2), (0.7, 0.7), (1.0, 1.0)],
    ]
)
def test_pattern_recognition(pattern_recognizer: PatternRecognizer, candles):
    candles = [ShortCandle(timestamp=c[0], price=c[1]) for c in candles]
    result = pattern_recognizer.recognize(candles)

    probas = list(result.pattern_to_proba.values())
    max_proba = result.pattern_to_proba[result.most_probable_pattern]

    assert max(probas) == pytest.approx(max_proba)
    assert sum(probas) == pytest.approx(1.0)
