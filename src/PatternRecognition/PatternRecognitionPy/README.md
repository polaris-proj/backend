Адрес и порт кафки указывается в переменной окружения
KAFKA_BOOTSTRAP_SERVER

### Запуск
```bash
docker compose up production -d
```

### Разработка/дебаг
```bash
docker compose run --rm development
```