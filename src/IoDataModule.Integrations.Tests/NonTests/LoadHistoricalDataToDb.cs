﻿using System.Diagnostics;
using Domain;
using Domain.Connectors;
using Infrastructure;
using IODataModule.Services;
using IODataModule.Services.ExchangeConnectors;
using Microsoft.Extensions.DependencyInjection;

namespace IoDataModule.Integrations.Tests.NonTests;

public class LoadHistoricalDataToDb : BaseTests
{
    [SetUp]
    public override void Setup()
    {
        base.Setup();
        _candleAccessService = Scope.ServiceProvider.GetRequiredService<ICandleAccessService>();
        _connectorFactory = Scope.ServiceProvider.GetRequiredService<IConnectorFactory>();
    }

    [Test]
    [Ignore("Сервисная тулза")]
    public async Task DownloadData()
    {
        var userId = Guid.Parse("9b257c73-241c-438e-9d2d-46834afcb244");

        var listOfExchanges = new List<(IConnector, Exchange)>
        {
            (await _connectorFactory.GetSpotConnectorAsync(userId, Exchange.BinanceSpot), Exchange.BinanceSpot),
            //  (new BinanceFuturesConnector(key, secret),Exchange.BinanceFutures)
        };

        var startDate = new DateTime(2017, 1, 1);
        var endDate = DateTime.Now.AddDays(-1); //startDate.AddDays(4 * 365);
        var timeFrames = EnumEnumerator.GetEnums<TimeFrame>().OrderBy(x => (int) x).Skip(1).ToArray();

        foreach (var (connector, exchange) in listOfExchanges)
        {
            var pairs = (await connector.GetPrices()).Keys.ToList();
            foreach (var pair in pairs)
            {
                foreach (var tf in timeFrames)
                {
                    Console.WriteLine(pair + " " + tf);
                    var sw = Stopwatch.StartNew();
                    await _candleAccessService.GetRange(userId, exchange, pair, tf, startDate, endDate);
                    //.Select(x => new Candle(x.TimeStamp, x.Open, x.High, x.Low, x.Close))
                    //.ToList();

                    //var zigZag = ZigZag.CalculatePriceStructLight(candles, 0.07f);
                    //var accumulations = SliceAlgorithm.FindBoxes(candles);

                    // var jsonAccumulations = JsonSerializer.Serialize(accumulations, serializeOptions);
                    // var jsonCandles = JsonSerializer.Serialize(candles, serializeOptions);
                    //
                    // var path = "./Data";
                    //
                    // if (!Directory.Exists(Path.Combine(path, pair)))
                    //     Directory.CreateDirectory(Path.Combine(path, pair));
                    //
                    // await File.WriteAllTextAsync(Path.Combine(path, pair, $"{tf}_accums.json"), jsonAccumulations);
                    // await File.WriteAllTextAsync(Path.Combine(path, pair, $"{tf}_candles.json"), jsonCandles);
                    Console.WriteLine("Суммарно: " + sw.ElapsedMilliseconds);
                }
            }
        }
    }

    private ICandleAccessService _candleAccessService = null!;
    private IConnectorFactory _connectorFactory = null!;
}