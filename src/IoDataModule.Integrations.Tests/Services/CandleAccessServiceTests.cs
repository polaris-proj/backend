﻿using Domain;
using IODataModule.Infrastructure.DAL;
using IODataModule.Services;
using Microsoft.Extensions.DependencyInjection;
using MoreLinq.Extensions;

namespace IoDataModule.Integrations.Tests.Services;

public class CandleAccessServiceTests : BaseTests
{
    [SetUp]
    public override void Setup()
    {
        base.Setup();
        DropTables().Wait();
        Scope.ServiceProvider.GetRequiredService<Migrations>().Start().Wait();

        _candleService = Scope.ServiceProvider.GetRequiredService<ICandleAccessService>();
    }

    [TearDown]
    public async Task ClearDataBase()
    {
        await DropTables();
        base.TearDown();
    }

    private static IEnumerable<object[]> GetRangeTestData()
    {
        //код включает в себя граничные точки везде + 1 день
        yield return new object[]
        {
            new Pair("BTC", "USDT"), TimeFrame.h4, new DateTime(2020, 1, 1), new DateTime(2021, 1, 1), 8760 / 4 + 6
        }; //в году 8760 часов
        yield return new object[]
        {
            new Pair("LTC", "USDT"), TimeFrame.D1, new DateTime(2020, 1, 1), new DateTime(2021, 1, 1), 365 + 2
        };
        yield return new object[]
        {
            new Pair("ETH", "USDT"), TimeFrame.D3, new DateTime(2020, 1, 1), new DateTime(2021, 1, 1), 365 / 3 + 2
        };
        yield return new object[]
        {
            new Pair("ETH", "BTC"), TimeFrame.D1, new DateTime(2020, 1, 1), new DateTime(2021, 1, 1), 365 + 2
        };
        yield return new object[]
        {
            new Pair("LTC", "USDT"), TimeFrame.m5, new DateTime(2023, 1, 1), new DateTime(2023, 1, 2), 24 * 12 + 1 //хз почему +1
        };
    }

    [TestCaseSource(nameof(GetRangeTestData))]
    public async Task GetRange_ShouldReturnCandles(Pair pair, TimeFrame tf, DateTime startTime, DateTime endTime,
        int candlesExpectedCount)
    {
        var candles = (await _candleService.GetRange(_userId, Exchange.BinanceSpot, pair, tf, startTime, endTime)).ToArray();

        candles.Should().NotBeEmpty();
        candles.Length.Should().Be(candlesExpectedCount);
        candles.ForEach(x =>
        {
            x.TimeStamp.Should().BePositive();
            x.Close.Should().BePositive();
            x.Volume.Should().BePositive();
            x.High.Should().BePositive();
            x.Low.Should().BePositive();
            x.Open.Should().BePositive();
        });
    }

    [TestCaseSource(nameof(GetRangeTestData))]
    public async Task GetRangeStream_ShouldReturnCandles(Pair pair, TimeFrame tf, DateTime startTime, DateTime endTime,
        int candlesExpectedCount)
    {
        var candles = await _candleService.GetRangeStream(_userId, Exchange.BinanceSpot, pair, tf, startTime, endTime).ToArrayAsync();

        candles.Should().NotBeEmpty();
        candles.Length.Should().Be(candlesExpectedCount);
        candles.ForEach(x =>
        {
            x.TimeStamp.Should().BePositive();
            x.Close.Should().BePositive();
            x.Volume.Should().BePositive();
            x.High.Should().BePositive();
            x.Low.Should().BePositive();
            x.Open.Should().BePositive();
        });
    }

    private ICandleAccessService _candleService = null!;
    private readonly Guid _userId = Guid.Parse("9b257c73-241c-438e-9d2d-46834afcb244");
}