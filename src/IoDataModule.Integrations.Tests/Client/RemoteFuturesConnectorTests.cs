﻿using Domain;
using Domain.Connectors;
using Domain.StrategyProcessor;
using Grpc.Net.Client;
using Infrastructure.FluentAssertions;
using IODataModule.Client;
using IODataModule.Client.Clients;
using IODataModule.Infrastructure.DAL;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MoreLinq.Extensions;

namespace IoDataModule.Integrations.Tests.Client;

public class RemoteFuturesConnectorTests : BaseTests
{
    [SetUp]
    public override void Setup()
    {
        base.Setup();

        DropTables().GetAwaiter().GetResult();
        Scope.ServiceProvider.GetRequiredService<Migrations>().Start().Wait();

        var contextFactory = Scope.ServiceProvider.GetService<IDbContextFactory<IoDataModuleContext>>() ??
                             throw new InvalidOperationException();

        var context = contextFactory.CreateDbContext();
        context.UserExchangeKeys.RemoveRange(context.UserExchangeKeys);
        
        var key = "b93b2970037a9936e4a4844f4d467c6504e68e016bfd205e873fd410ff0b9c91";
        var secret = "651b3ee739bf4a73584813aaffd5650357dc94c97056d6417049836f80735f4f";
        var userId = Guid.NewGuid();

        var channel = GrpcChannel.ForAddress("http://localhost:9999", new GrpcChannelOptions
        {
            MaxReceiveMessageSize = null,
        });
        var wrappedChannel = new IoDataModuleClientChannel {Channel = channel};
        
        var userExchangeKeys = new UserExchangeKeysClient(wrappedChannel);
        userExchangeKeys.AddOrUpdateExchangeKey(userId, Exchange.BinanceFuturesTest, key, secret).GetAwaiter().GetResult();
        _connector = new RemoteFuturesConnector(userId, Exchange.BinanceFuturesTest, wrappedChannel);
    }

    [TearDown]
    public async Task ClearDataBase()
    {
        await _connector.DisposeAsync();
        await DropTables();
        base.TearDown();
    }


    [Test]
    public async Task GetPrices_ReturnSomeValues()
    {
        var priceByTicker = await _connector.GetPrices();
        priceByTicker.Should().NotBeEmpty();
        priceByTicker.Keys.ForEach(x =>
            x.ToString().Should().NotBeNullOrWhiteSpace().And.NotBeEmpty().And.Contain("/"));
        priceByTicker.Values.ForEach(x => x.Should().BePositive());
    }

    private static IEnumerable<object[]> FuturesOrderTestData()
    {
        yield return new object[]
        {
            FuturesOrder.Long, new PlacementOrder(10_000, 0.01m), new PlacementOrder(12_000, 0.01m),
            new PlacementOrder(8_000, 0.01m)
        };
        yield return new object[]
        {
            FuturesOrder.Short, new PlacementOrder(100_000, 0.01m), new PlacementOrder(8_000, 0.01m),
            new PlacementOrder(12_000, 0.01m)
        };
    }

    [TestCaseSource(nameof(FuturesOrderTestData))]
    public async Task CreateFuturesOrder_CreateOrderSuccessfully(FuturesOrder orderType, PlacementOrder placementOrder,
        PlacementOrder take, PlacementOrder stop)
    {
        var order = await _connector.CreateFuturesOrder(_btcUsdt, orderType, 1, placementOrder, take, stop);
        order.Should().NotBeNull();
        order.Id.Should().NotBeNull();
    }

    [TestCaseSource(nameof(FuturesOrderTestData))]
    public async Task GetOrderInfo_ShouldReturnValidDataForFuturesOrder(FuturesOrder orderType,
        PlacementOrder placementOrder, PlacementOrder _, PlacementOrder __)
    {
        var order = await _connector.CreateFuturesOrder(_btcUsdt, orderType, 1, placementOrder);

        var orderInfo = await _connector.GetOrderInfo(_btcUsdt, order);
        orderInfo.OrderType.Should().Be(orderType.ToOrderType());
        orderInfo.Price.Should().BeInRadius(placementOrder.Price);
        orderInfo.Amount.Should().BeInRadius(placementOrder.Amount);
        orderInfo.CreateTime.Should().BeCloseTo(DateTime.UtcNow, TimeSpan.FromHours(1));
    }

    [TestCase(FuturesOrder.Long)]
    [TestCase(FuturesOrder.Short)]
    public async Task CloseOrder_ShouldReturnValidDataForFuturesOrder(FuturesOrder orderType)
    {
        var price = (double) (await _connector.GetPrices()).Single(x => x.Key == _btcUsdt).Value;
        price *= orderType == FuturesOrder.Long ? 0.9 : 1.1;

        var order = await _connector.CreateFuturesOrder(_btcUsdt, orderType, 1,
            new PlacementOrder((int) price, 0.01m));

        var orderInfo = await _connector.GetOrderInfo(_btcUsdt, order);
        orderInfo.OrderStatus.Should().Be(OrderStatus.Open);

        await _connector.CancelOrder(_btcUsdt, order);

        var orderInfoAfterClose = await _connector.GetOrderInfo(_btcUsdt, order);
        orderInfoAfterClose.OrderStatus.Should().Be(OrderStatus.Close);
    }

    [Test]
    public async Task GetOrderInfo_UnknownId_ThrowException()
    {
        var getOrderInfo = async () =>
            await _connector.GetOrderInfo(_btcUsdt, new OrderId(Guid.NewGuid().ToString()));

        await getOrderInfo.Should().ThrowAsync<Exception>("Order Not Found");
    }

    [Test]
    public async Task GetCoinsAmount()
    {
        var coinsAmount = await _connector.GetCoinsAmount();
        coinsAmount.Should().NotBeEmpty();
        coinsAmount.ForEach(x =>
        {
            x.Asset.Name.Should().NotBeNullOrEmpty();
            x.Asset.Name.Length.Should().BeGreaterOrEqualTo(1);
            x.Available.Should().BeGreaterOrEqualTo(0);
            x.Locked.Should().BeGreaterOrEqualTo(0);
            x.Total.Should().BePositive();
        });
    }

    private static IEnumerable<object[]> GetCandlesTestData()
    {
        return new[]
        {
            new object[]
                {new DateTimeRange(DateTime.UtcNow.AddDays(-2), DateTime.UtcNow.AddDays(-1)), TimeFrame.h1, 24},
            new object[] {new DateTimeRange(DateTime.UtcNow.AddDays(-2), DateTime.UtcNow.AddDays(-1)), TimeFrame.h4, 6},
            new object[]
                {new DateTimeRange(DateTime.UtcNow.AddDays(-2), DateTime.UtcNow.AddDays(-1)), TimeFrame.h12, 2},
            new object[] {new DateTimeRange(DateTime.UtcNow.AddDays(-2), DateTime.UtcNow.AddDays(-1)), TimeFrame.D1, 1},
            new object[]
                {new DateTimeRange(DateTime.UtcNow.AddDays(-2), DateTime.UtcNow.AddDays(-1)), TimeFrame.m30, 48},
            new object[]
                {new DateTimeRange(DateTime.UtcNow.AddDays(-2), DateTime.UtcNow.AddDays(-1)), TimeFrame.m15, 96},
        };
    }

    [TestCaseSource(nameof(GetCandlesTestData))]
    public async Task GetCandles_ByPreviousDay_ShouldReturn24H1Candle(DateTimeRange range, TimeFrame timeFrame,
        int candleAmount)
    {
        var candles = await _connector.GetCandles(_btcUsdt, timeFrame, range);

        candles.Count.Should().Be(candleAmount);
    }

    [Test]
    [NonParallelizable]
    public async Task GetOrdersPerPair_ShouldReturnTrades()
    {
        var trades = await _connector.GetOrdersPerPair(_btcUsdt);
        var amount = trades.Count;

        await _connector.CreateFuturesOrder(_btcUsdt, FuturesOrder.Long, 1, new PlacementOrder(20_000, 0.01m));

        var newTrades = await _connector.GetOrdersPerPair(_btcUsdt);
        newTrades.Count.Should().BeGreaterOrEqualTo(amount + 1);
        var lastOrder = newTrades.Last();
        lastOrder.OrderType.Should().Be(OrderType.Long);
        lastOrder.Amount.Should().Be(0.01m);
    }

    private readonly Pair _btcUsdt = new(BtcTicker, UsdtTicker);
    private static readonly Ticker UsdtTicker = new("USDT");
    private static readonly Ticker BtcTicker = new("BTC");


    private RemoteFuturesConnector _connector = null!;
}