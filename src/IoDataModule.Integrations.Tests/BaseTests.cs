﻿using Dapper;
using Infrastructure;
using IODataModule;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Npgsql;

namespace IoDataModule.Integrations.Tests;

public abstract class BaseTests
{
    [SetUp]
    public virtual void Setup()
    {
        Scope = App.Services.CreateScope();
    }

    [TearDown]
    public virtual void TearDown()
    {
        Scope.Dispose();
    }

    [OneTimeSetUp]
    public virtual void OneTimeSetup()
    {
        App = Program.SetUp(Array.Empty<string>());
    }

    [OneTimeTearDown]
    public virtual void OneTimeTearDown()
    {
     //   App.Dispose();
    }

    protected async Task DropTables()
    {
        var postgresParameters = Scope.ServiceProvider.GetRequiredService<PostgresParameters>();
        var connectionString = postgresParameters.FullConnectionString;

        var connection = new NpgsqlConnection(connectionString);
        await connection.ExecuteAsync("DROP SCHEMA public CASCADE; CREATE SCHEMA public;");
        await connection.CloseAsync();
    }

#pragma warning disable NUnit1032
    protected IHost App = null!;
    protected IServiceScope Scope = null!;
}