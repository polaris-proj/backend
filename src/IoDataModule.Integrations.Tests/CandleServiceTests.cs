using Domain;
using Grpc.Net.Client;
using Infrastructure.Extensions;
using IODataModule.Client;
using IODataModule.Client.Clients;
using IODataModule.Infrastructure.DAL;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging.Abstractions;
using MoreLinq.Extensions;

namespace IoDataModule.Integrations.Tests;

public class CandleServiceTests : BaseTests
{
    [SetUp]
    public override void Setup()
    {
        base.Setup();
        DropTables().GetAwaiter().GetResult();
        Scope.ServiceProvider.GetRequiredService<Migrations>().Start().GetAwaiter().GetResult();
        var channel = GrpcChannel.ForAddress("http://localhost:9999", new GrpcChannelOptions
        {
            MaxReceiveMessageSize = null,
        });
        var wrappedChannel = new IoDataModuleClientChannel {Channel = channel};
        _candleService = new CandleService(wrappedChannel, new NullLogger<CandleService>());
    }

    [TearDown]
    public async Task ClearDataBase()
    {
        await DropTables();
        base.TearDown();
    }

    private static IEnumerable<object[]> GetRangeTestData()
    {
        //код включает в себя граничные точки везде + 1 день
        yield return new object[]
        {
            new Pair("BTC", "USDT"), TimeFrame.h4, new DateTime(2020, 1, 1).ToMilliseconds(),
            new DateTime(2021, 1, 1).ToMilliseconds(), 8760 / 4 + 6
        }; //в году 8760 часов
        yield return new object[]
        {
            new Pair("LTC", "USDT"), TimeFrame.D1, new DateTime(2020, 1, 1).ToMilliseconds(),
            new DateTime(2021, 1, 1).ToMilliseconds(), 365 + 2
        };
        yield return new object[]
        {
            new Pair("ETH", "USDT"), TimeFrame.D3, new DateTime(2020, 1, 1).ToMilliseconds(),
            new DateTime(2021, 1, 1).ToMilliseconds(), 365 / 3 + 2
        };
        yield return new object[]
        {
            new Pair("ETH", "BTC"), TimeFrame.D1, new DateTime(2020, 1, 1).ToMilliseconds(),
            new DateTime(2021, 1, 1).ToMilliseconds(), 365 + 2
        };
        yield return new object[]
        {
            new Pair("LTC", "USDT"), TimeFrame.m5, new DateTime(2023, 1, 1).ToMilliseconds(),
            new DateTime(2023, 1, 2).ToMilliseconds(), 24 * 12 + 1 //хз почему +1
        };
    }

    [TestCaseSource(nameof(GetRangeTestData))]
    public async Task GetRange_ShouldReturnCandles(Pair pair, TimeFrame tf, long startTime, long endTime,
        int candlesExpectedCount)
    {
        var candles = (await _candleService.GetRange(_userId, new (Exchange.BinanceSpot, pair, tf), startTime, endTime)
            ).ToArray();

        candles.Should().NotBeEmpty();
        candles.Length.Should().Be(candlesExpectedCount);
        candles.ForEach(x =>
        {
            x.TimeStamp.Should().BePositive();
            x.Close.Should().BePositive();
            x.Volume.Should().BePositive();
            x.High.Should().BePositive();
            x.Low.Should().BePositive();
            x.Open.Should().BePositive();
        });
    }

    [TestCaseSource(nameof(GetRangeTestData))]
    public async Task GetRangeStream_ShouldReturnCandles(Pair pair, TimeFrame tf, long startTime, long endTime,
        int candlesExpectedCount)
    {
        var candles = await _candleService.GetRangeStream(_userId, new (Exchange.BinanceSpot, pair, tf), startTime, endTime)
            .ToArrayAsync();

        candles.Should().NotBeEmpty();
        candles.Length.Should().Be(candlesExpectedCount);
        candles.ForEach(x =>
        {
            x.TimeStamp.Should().BePositive();
            x.Close.Should().BePositive();
            x.Volume.Should().BePositive();
            x.High.Should().BePositive();
            x.Low.Should().BePositive();
            x.Open.Should().BePositive();
        });
    }

    private ICandleService _candleService = null!;
    private Guid _userId = Guid.Parse("9b257c73-241c-438e-9d2d-46834afcb244");
}