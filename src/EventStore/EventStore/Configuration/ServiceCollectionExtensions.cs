﻿using Domain.Patterns.Objects;
using EventStorage.EventsConsuming;
using EventStorage.EventStoraging;
using EventStore.Shared;
using Infrastructure;
using Infrastructure.DAL.Dapper;
using Infrastructure.RTQConnector;

namespace EventStorage.Configuration;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddGrpcControllers(this IServiceCollection services)
    {

        return services.AddScoped<IPatternAccessServiceGrpc, PatternAccessServiceGrpc>();
    }
    
    public static IServiceCollection AddEventConsumers(this IServiceCollection services)
    {
        return services
            .AddSingleton<KafkaParameters>()
            .AddSingleton<EventsConsumer<NewPatternEvent>>();
    }
    
    public static IServiceCollection ConfigureDatabase(this IServiceCollection services)
    {
        services.AddSingleton<PostgresParameters>();
        services.ConfigureNpgSql();
        services.AddSingleton<NewPatternEventRepository>();
        
        return services;
    }
}