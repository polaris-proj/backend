using System.Runtime.Serialization;
using System.Text.Json;
using Confluent.Kafka;
using Domain.Patterns;
using EventStorage.EventStoraging;
using Infrastructure.RTQConnector;
using PatternRecognition.Shared;

namespace EventStorage.EventsConsuming;

internal class EventsConsumer<TEvent>: IDisposable
where TEvent : ITradeEvent
{
    private readonly IConsumer<Ignore, string> _consumer;
    private readonly ILogger<EventsConsumer<TEvent>> _logger;
    private readonly NewPatternEventRepository _eventRepository;

    private readonly string _topicName = Topic.PatternRecognition.ToString();

    public EventsConsumer(
        KafkaParameters kafkaParameters,
        ILogger<EventsConsumer<TEvent>> logger,
        NewPatternEventRepository eventRepository)
    {
        _logger = logger;
        _eventRepository = eventRepository;

        var cc = kafkaParameters.GetClientConfig();
        var config = new ConsumerConfig(cc)
        {
            GroupId = $"{_topicName}_consumer_eventStore",
            AllowAutoCreateTopics = false,
            EnableAutoCommit = true
        };
        _consumer = new ConsumerBuilder<Ignore, string>(config).Build();
        
        ExecuteAsync(CancellationToken.None);
    }

    public void Dispose()
    {
        _consumer.Dispose();
    }

    protected void ExecuteAsync(CancellationToken ct)
    { 
        Task.Run(async () =>
        {
            _logger.LogInformation($"Поднимаю консюмер для {_topicName}");
            _consumer.Subscribe(_topicName);

            while (!ct.IsCancellationRequested)
            {
                await ProcessKafkaMessageAsync(ct);
            }

            _consumer.Close();
        });
    }


    private async Task ProcessKafkaMessageAsync(CancellationToken ct)
    {
        var consumeResult = _consumer.Consume(ct);
     
        var eventJson = consumeResult.Message;
        var eventDto = JsonSerializer.Deserialize<NewPatternEventDto>(eventJson.Value);

        if (eventDto == null)
        {
            throw new SerializationException($"Не смогли десериализовать в {typeof(TEvent).Name} \"{eventJson}\"");
        }

        await _eventRepository.SaveEventsAsync(new[] { eventDto.Map() });
       // _consumer.Commit(consumeResult);
    }
}