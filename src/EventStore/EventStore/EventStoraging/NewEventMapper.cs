﻿using System.Text.Json;
using Domain;
using Domain.Patterns.Objects;

namespace EventStorage.EventStoraging;

public static class NewEventMapper
{
    public static NewPatternEventDbo Map(this NewPatternEvent @event) =>
        new NewPatternEventDbo()
        {
            TimeFrame = (int)@event.TimeFrame,
            TimeStamp = @event.TimeStamp,
            RecognitionResult = JsonSerializer.Serialize<RecognitionResult>(@event.RecognitionResult)
        };
    
    public static NewPatternEvent Map(this NewPatternEventDbo eventDbo, Exchange exchange, Pair pair) =>
        new NewPatternEvent()
        {
            Exchange = exchange,
            Pair = pair,
            TimeFrame = (TimeFrames.TimeFrame)eventDbo.TimeFrame,
            TimeStamp = eventDbo.TimeStamp,
            RecognitionResult = JsonSerializer.Deserialize<RecognitionResult>(eventDbo.RecognitionResult)
        };
}