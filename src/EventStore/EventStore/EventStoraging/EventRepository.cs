﻿
using Domain.Patterns.Objects;
using EventStore.Shared;
using Npgsql;
using PostgreSQLCopyHelper;

namespace EventStorage.EventStoraging;

public abstract class EventRepositoryBase(NpgsqlDataSource dataSource) //: IEventRepository<NewPatternRecognitionDbo>
{
    protected readonly NpgsqlDataSource DataSource = dataSource;

    public async Task SaveEventAsync(IEnumerable<NewPatternEvent> events)
    {
        await using var connection = DataSource.CreateConnection();
        await connection.OpenAsync();
        await EventCopyHelper.SaveAllAsync(connection, events.Select(@event => @event.Map()));
    }

    public abstract Task<IEnumerable<NewPatternEvent>> GetEventAsync(GetRangeArgument rangeArgument);

    protected abstract PostgreSQLCopyHelper<NewPatternEventDbo> EventCopyHelper { get; }
}