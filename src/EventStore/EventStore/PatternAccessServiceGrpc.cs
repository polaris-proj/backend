﻿using Domain.Patterns.Objects;
using EventStorage.EventStoraging;
using EventStore.Shared;
using ProtoBuf.Grpc;

namespace EventStorage;

public class PatternAccessServiceGrpc(NewPatternEventRepository eventRepository) : IPatternAccessServiceGrpc
{
    public async Task<IReadOnlyList<NewPatternEvent>> GetRangeAsync(GetRangeArgument argument, CallContext context = default)
    {
        return (await eventRepository.GetEventAsync(argument)).ToList();
    }
}