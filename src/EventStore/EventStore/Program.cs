using Domain.Patterns.Objects;
using EventStorage.Configuration;
using EventStorage.EventsConsuming;
using EventStore.Shared;
using Infrastructure;
using Infrastructure.Metric;
using Infrastructure.Metric.Grpc;
using Microsoft.AspNetCore.Builder;
using ProtoBuf.Grpc.Server;

var builder = WebApplication.CreateBuilder(args);
builder.ConfigureOpenTelemetry("EventStore");

builder.Services.AddHealthChecks();
builder.Services.AddLogging(x => { x.AddConsole(); });
builder.Services.AddSingleton<IVault, Vault>();

builder.Services.ConfigureDatabase();
builder.Services.AddCodeFirstGrpc(options =>
{
    options.EnableDetailedErrors = true;
    options.MaxReceiveMessageSize = 200 * 1024 * 1024; // 200 MB
    options.MaxSendMessageSize = 500 * 1024 * 1024; // 500 MB
    options.Interceptors.Add<GrpcErrorInterceptor>();
});
builder.Services
    .AddEventConsumers()
    .AddGrpcControllers();




var host = builder.Build();

host.UseRouting();
host.UseEndpoints(endpoints =>
{
    endpoints.MapGrpcService<IPatternAccessServiceGrpc>();
    endpoints.MapHealthChecks("health");
});


var t =host.Services.GetRequiredService<EventsConsumer<NewPatternEvent>>();


await host.RunAsync();




t = null;


