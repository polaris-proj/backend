﻿using System.Runtime.Serialization;
using Domain;

namespace EventStore.Shared;

[DataContract]
public class GetPairsArgument
{
    public GetPairsArgument(Guid userId, Exchange exchange)
    {
        Exchange = exchange;
        UserId = userId;
    }

    [DataMember(Order = 1)] public Exchange Exchange { get; set; }
    [DataMember(Order = 2)] public Guid UserId { get; set; }
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    private GetPairsArgument()
    {
    } //for protobuf don't delete
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
}