﻿using Grpc.Net.Client;
using Infrastructure.Grpc;

namespace EventStore.Client;

public class EventStoreClientChannel : INamedGrpcChannelWrapper
{
    public GrpcChannel Channel { get; init; } = null!;
}