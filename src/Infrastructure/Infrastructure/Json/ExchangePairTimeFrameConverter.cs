﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;
using Domain;
using Domain.StrategyProcessor;
using static System.Enum;

namespace Infrastructure.Json;

public class ExchangePairTimeFrameConverter : JsonConverter<ExchangePairTimeFrame>
{
    public override ExchangePairTimeFrame Read(ref Utf8JsonReader reader, Type typeToConvert,
        JsonSerializerOptions options) => ReadLocal(ref reader, options);

    public override void Write(Utf8JsonWriter writer, ExchangePairTimeFrame value, JsonSerializerOptions options) =>
        writer.WriteStringValue(GetConvertedValue(value));

    public override ExchangePairTimeFrame ReadAsPropertyName(ref Utf8JsonReader reader, Type typeToConvert,
        JsonSerializerOptions options) => ReadLocal(ref reader, options);

    public override void WriteAsPropertyName(Utf8JsonWriter writer, ExchangePairTimeFrame value,
        JsonSerializerOptions options) => writer.WritePropertyName(GetConvertedValue(value));

    private static ExchangePairTimeFrame ReadLocal(ref Utf8JsonReader reader, JsonSerializerOptions _)
    {
        var values = reader.GetString()?.Split();

        if (values is null || values.Length != 3)
            throw new ArgumentException("Invalid ExchangePairTimeFrame format");

        var successParseExchange = TryParse<Exchange>(values[0], out var exchange);
        var pair = new Pair(values[1]);
        var successParseTimeFrame = TryParse<TimeFrame>(values[2], out var timeFrame);

        if (!successParseExchange || !successParseTimeFrame)
        {
            throw new ArgumentException("Invalid ExchangePairTimeFrame format");
        }

        return new ExchangePairTimeFrame(exchange, pair, timeFrame);
    }

    private static string GetConvertedValue(ExchangePairTimeFrame value)
    {
        return $"{value.Exchange} {value.Pair} {value.TimeFrame}";
    }
}