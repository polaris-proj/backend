﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace Infrastructure.Json;

public static class JsonOptions
{
    public static JsonSerializerOptions JsonSerializerOptions { get; } = new(JsonSerializerDefaults.Web);

    static JsonOptions()
    {
        Setup(JsonSerializerOptions);
    }

    public static void Setup(JsonSerializerOptions options)
    {
        options.Converters.Add(new DateOnlyJsonConverter());
        options.Converters.Add(new TimeOnlyJsonConverter());
        options.Converters.Add(new TypeJsonConverter());
        options.Converters.Add(new ExchangePairTimeFrameConverter());
        options.Converters.Add(new JsonStringEnumConverter());
        options.ReadCommentHandling = JsonCommentHandling.Skip;
    }
}