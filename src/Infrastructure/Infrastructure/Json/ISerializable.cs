﻿using System.Text.Json;

namespace Infrastructure.Json;

public interface ISerializable
{
    virtual JsonDocument Serialize()
    {
        return JsonSerializer.SerializeToDocument(this, JsonOptions.JsonSerializerOptions);
    }
}