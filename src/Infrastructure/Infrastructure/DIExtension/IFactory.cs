namespace Infrastructure.DIExtension;

public interface IFactory<out T>
{
    public T Create();
}