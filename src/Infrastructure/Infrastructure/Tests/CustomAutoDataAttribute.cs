﻿using System;
using AutoFixture;
using AutoFixture.Kernel;
using AutoFixture.NUnit3;
using Domain;
using Domain.Connectors;

namespace Infrastructure.Tests;

[AttributeUsage(AttributeTargets.Method)]
public class CustomAutoDataAttribute() : AutoDataAttribute(CreateFixture)
{
    public static IFixture CreateFixture()
    {
        return new Fixture().Customize(new CompositeCustomization(
            new PairCustomization(),
            new DateTimeRangeCustomization(),
            new RandomEnumSequenceGenerator().ToCustomization()
        ));
    }

    private class PairCustomization : ICustomization
    {
        private static readonly Ticker[] Tickers =
            {"BTC", "USDT", "BUSD", "ETH", "LTC", "EOS", "XLM", "LINK", "XRP", "BNB", "ADA", "XMR"};

        public void Customize(IFixture fixture)
        {
            fixture.Customize<Pair>(x =>
                x.FromFactory(() =>
                {
                    var index = Random.Shared.Next(0, Tickers.Length - 2);
                    return new Pair(Tickers[index], Tickers[index + 1]);
                }));
        }
    }

    private class RandomEnumSequenceGenerator : ISpecimenBuilder
    {
        private static readonly Random Random = new();

        public object Create(object request, ISpecimenContext context)
        {
            var seededRequest = request as SeededRequest;
            var type = seededRequest?.Request as Type;
            if (type == null || !type.IsEnum)
            {
                return new NoSpecimen();
            }

            var values = Enum.GetValues(type);

            var minValue = type == typeof(TimeFrame) ? 1 : 0;


            var index = Random.Next(minValue, values.Length);
            return values.GetValue(index) ?? throw new IndexOutOfRangeException();
        }
    }
    
    private class DateTimeRangeCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            fixture.Customize<DateTimeRange>(x =>
                x.FromFactory(() =>
                {
                    var offset = Random.Shared.Next(1, 10000);
                    var startDateTime = fixture.Create<DateTime>();
                    return new DateTimeRange(startDateTime, startDateTime.AddHours(offset));
                }));
        }
    }
}