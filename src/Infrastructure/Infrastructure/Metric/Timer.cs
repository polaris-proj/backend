﻿using System;
using System.Reflection;

namespace Infrastructure.Metric;

public static class MethodTimeLogger
{
    public static void Log(MethodBase methodBase, long milliseconds, string? message)
    {
        Console.WriteLine($"{methodBase.Name} {milliseconds} {message}");
    }
}