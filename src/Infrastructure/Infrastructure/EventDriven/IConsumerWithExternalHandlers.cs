﻿using System;
using System.Threading.Tasks;

namespace Infrastructure.EventDriven;

public interface IConsumerWithExternalHandlers<out T>
{
    void RegisterHandler(Func<T, Task> action);
}