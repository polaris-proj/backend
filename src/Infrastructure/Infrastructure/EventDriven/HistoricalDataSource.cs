﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.EventDriven;

public class HistoricalDataSource : BaseDataSource
{
    public async Task SendEvents<T>(IEnumerable<T> collection)
    {
        await Task.WhenAll(collection.Select(SendEvent));
    }

    public async Task SendEvent<T>(T @event)
    {
       await SendDataToHandlers(@event);
    }
}