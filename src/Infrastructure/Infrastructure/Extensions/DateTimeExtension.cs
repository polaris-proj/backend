﻿using System;

namespace Infrastructure.Extensions;

public static class DateTimeExtension
{
    public static long ToMilliseconds(this DateTime dt)
    {
        if (dt < TimeConstants.Jan1St1970)
        {
            throw new ArgumentOutOfRangeException($"{dt} should be more {TimeConstants.Jan1St1970}");
        }

        return (dt - TimeConstants.Jan1St1970).Ticks / TimeSpan.TicksPerMillisecond;
    }

    public static DateTime ToDateTime(this long milliseconds)
    {
        return TimeConstants.Jan1St1970.AddMilliseconds(milliseconds);
    }
}