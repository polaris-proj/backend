﻿namespace Infrastructure;

public class PostgresParameters : IParameters
{
    public PostgresParameters(IVault vault)
    {
        Host = vault.GetValue("Settings:PostgresSettings:Host");
        Port = int.Parse(vault.GetValue("Settings:PostgresSettings:Port"));
        DbName = vault.GetValue("Settings:PostgresSettings:Name");
        Username = vault.GetValue("Settings:PostgresSettings:Username");
        Password = vault.GetValue("Settings:PostgresSettings:Password");
        IncludeErrorDetail = bool.Parse(vault.GetValue("Settings:PostgresSettings:IncludeErrorDetail"));
        Pooling = bool.Parse(vault.GetValue("Settings:PostgresSettings:Pooling"));
        Timeout = int.Parse(vault.GetValue("Settings:PostgresSettings:Timeout"));
        CommandTimeout = int.Parse(vault.GetValue("Settings:PostgresSettings:CommandTimeout"));
        
        ConnectionStringWithoutDatabaseName = $"Host={Host};Port={Port};Username={Username};Password={Password};Include Error Detail={IncludeErrorDetail};Pooling={Pooling};Timeout={Timeout};Command Timeout={CommandTimeout}";
        FullConnectionString = $"{ConnectionStringWithoutDatabaseName};Database={DbName}";
    }

    public string DbName { get; }
    public string Host { get; }
    public int Port { get; }
    public string Username { get; }
    public string Password { get; }
    public bool IncludeErrorDetail { get; }
    public bool Pooling { get; }
    public int Timeout { get; }
    public int CommandTimeout { get; }
    public string ConnectionStringWithoutDatabaseName { get; }
    public string FullConnectionString { get; }
}