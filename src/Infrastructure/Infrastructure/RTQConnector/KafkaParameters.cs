﻿using System;
using Confluent.Kafka;

namespace Infrastructure.RTQConnector;

public class KafkaParameters(IVault vault) : IParameters
{
    public string BootstrapServers { get; } = vault.GetValue("Settings:Kafka:BootstrapServers");
    public string? SecurityProtocol { get; } = vault.GetValueOrNull("Settings:Kafka:SecurityProtocol");
    public string? SaslUsername { get; } = vault.GetValueOrNull("Settings:Kafka:SaslUsername");
    public string? SaslPassword { get; } = vault.GetValueOrNull("Settings:Kafka:SaslPassword");
    public string? SaslMechanism { get; } = vault.GetValueOrNull("Settings:Kafka:SaslMechanism");

    public ClientConfig GetClientConfig()
    {
        var cc = new ClientConfig();
        if (SaslMechanism != null) cc.SaslMechanism = Enum.Parse<SaslMechanism>(SaslMechanism);
        if (SaslUsername != null) cc.SaslUsername = SaslUsername;
        if (SaslPassword != null) cc.SaslPassword = SaslPassword;
        if (SecurityProtocol != null) cc.SecurityProtocol = Enum.Parse<SecurityProtocol>(SecurityProtocol);

        if (BootstrapServers.Length < 1)
        {
            throw new ArgumentException("BootstrapServers is empty");
        }

        cc.BootstrapServers = BootstrapServers;

        return cc;
    }
}