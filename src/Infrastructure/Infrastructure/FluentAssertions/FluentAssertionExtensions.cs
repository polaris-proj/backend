﻿using FluentAssertions;
using FluentAssertions.Numeric;

namespace Infrastructure.FluentAssertions;

public static class FluentAssertionExtensions
{
    public static AndConstraint<NumericAssertions<decimal>> BeInRadius(
        this NumericAssertions<decimal> assertions, decimal number, decimal radius = 0.0000000001m)
        => assertions.BeLessThanOrEqualTo(number + radius).And.BeGreaterOrEqualTo(number - radius);
}