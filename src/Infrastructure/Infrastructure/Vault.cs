﻿using System;
using Microsoft.Extensions.Configuration;

namespace Infrastructure;

public class Vault(IConfiguration config) : IVault
{
    public string GetValue(string key)
    {
        var configValue = config[key];

        if (configValue != null)
        {
            return configValue;
        }

        throw new VaultKeyNotFoundException($"Key: {key} not found");
    }

    public string? GetValueOrNull(string key)
    {
        return config[key];
    }
}

public interface IVault
{
    string GetValue(string key);
    string? GetValueOrNull(string key);
}

public class VaultKeyNotFoundException : Exception
{
    public VaultKeyNotFoundException(string message)
        : base(message)
    {
    }
}