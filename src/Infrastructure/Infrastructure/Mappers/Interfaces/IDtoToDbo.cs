﻿namespace Infrastructure.Mappers.Interfaces;

public interface IDtoToDbo<in TIn, out TOut> : IMap
{
    TOut MapDtoToDbo(TIn candle);
}