﻿namespace Infrastructure.Mappers.Interfaces;

public interface IModelToDbo<in TIn, out TOut> : IMap
{
    TOut MapModelToDbo(TIn strategyStartEventDto);
}