﻿namespace Infrastructure.Mappers.Interfaces;

public interface IDboToDto<in TIn, out TOut> : IMap
{
    TOut MapDboToDto(TIn parameterDbo);
}