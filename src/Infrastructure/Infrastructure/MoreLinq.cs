﻿using System;
using System.Collections.Generic;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace Infrastructure;

public static class MoreLinq
{
    public static T RandomElement<T>(this IReadOnlyList<T> collection)
    {
        if (collection == null)
        {
            throw new ArgumentNullException(nameof(collection));
        }

        var random = new Random();
        var count = collection.Count;
        var index = random.Next(0, count);

        return collection[index];
    }
    
    public static async IAsyncEnumerable<T> Buffer<T>(this IAsyncEnumerable<T> asyncEnumerable, int size)
    {
        var options = new BoundedChannelOptions(size)
        {
            SingleReader = true,
            SingleWriter = true,
            AllowSynchronousContinuations = false,
        };

        var chan = Channel.CreateBounded<T>(options);
        var reader = chan.Reader;
        var writer = chan.Writer;

        var asyncEnumerator = asyncEnumerable.GetAsyncEnumerator();

        async Task<bool> FillAsync()
        {
            var more = true;
            while (more && reader.Count < size)
            {
                try
                {
                    more = await asyncEnumerator.MoveNextAsync();

                    if (more)
                        await writer.WriteAsync(asyncEnumerator.Current);
                    else
                        writer.Complete();
                }
                catch (Exception ex)
                {
                    more = false;
                    writer.Complete(ex);
                }
            }

            return more;
        }

        var fillTask = Task.FromResult(true);
        var keepFilling = true;

        var continu = true;
        while (continu)
        {
            if (keepFilling && fillTask.IsCompleted && reader.Count < size)
            {
                keepFilling = await fillTask;

                if (keepFilling)
                    fillTask = Task.Run(FillAsync);
            }

            continu = await reader.WaitToReadAsync();

            if (continu)
            {
                var ret = await reader.ReadAsync();
                yield return ret;
            }
        }
    }

    public static IEnumerable<IEnumerable<TSource>> Batch<TSource>(this IEnumerable<TSource> source, int size)
    {
        return Batch(source, size, x => x);
    }

    /// <summary>
    /// Batches the source sequence into sized buckets and applies a projection to each bucket.
    /// </summary>
    /// <typeparam name="TSource">Type of elements in <paramref name="source"/> sequence.</typeparam>
    /// <typeparam name="TResult">Type of result returned by <paramref name="resultSelector"/>.</typeparam>
    /// <param name="source">The source sequence.</param>
    /// <param name="size">Size of buckets.</param>
    /// <param name="resultSelector">The projection to apply to each bucket.</param>
    /// <returns>A sequence of projections on equally sized buckets containing elements of the source collection.</returns>
    /// <remarks>
    /// <para>
    /// This operator uses deferred execution and streams its results
    /// (buckets are streamed but their content buffered).</para>
    /// <para>
    /// <para>
    /// When more than one bucket is streamed, all buckets except the last
    /// is guaranteed to have <paramref name="size"/> elements. The last
    /// bucket may be smaller depending on the remaining elements in the
    /// <paramref name="source"/> sequence.</para>
    /// Each bucket is pre-allocated to <paramref name="size"/> elements.
    /// If <paramref name="size"/> is set to a very large value, e.g.
    /// <see cref="int.MaxValue"/> to effectively disable batching by just
    /// hoping for a single bucket, then it can lead to memory exhaustion
    /// (<see cref="OutOfMemoryException"/>).
    /// </para>
    /// </remarks>
    public static IEnumerable<TResult> Batch<TSource, TResult>(this IEnumerable<TSource> source, int size,
        Func<IEnumerable<TSource>, TResult> resultSelector)
    {
        if (source == null) throw new ArgumentNullException(nameof(source));
        if (size <= 0) throw new ArgumentOutOfRangeException(nameof(size));
        if (resultSelector == null) throw new ArgumentNullException(nameof(resultSelector));

        return _();

        IEnumerable<TResult> _()
        {
            switch (source)
            {
                case ICollection<TSource> {Count: 0}:
                {
                    break;
                }
                case ICollection<TSource> collection when collection.Count <= size:
                {
                    var bucket = new TSource[collection.Count];
                    collection.CopyTo(bucket, 0);
                    yield return resultSelector(bucket);
                    break;
                }
                case IReadOnlyCollection<TSource> {Count: 0}:
                {
                    break;
                }
                case IReadOnlyList<TSource> list when list.Count <= size:
                {
                    var bucket = new TSource[list.Count];
                    for (var i = 0; i < list.Count; i++)
                        bucket[i] = list[i];
                    yield return resultSelector(bucket);
                    break;
                }
                case IReadOnlyCollection<TSource> collection when collection.Count <= size:
                {
                    size = collection.Count;
                    goto default;
                }
                default:
                {
                    TSource[]? bucket = null;
                    var count = 0;

                    foreach (var item in source)
                    {
                        bucket ??= new TSource[size];
                        bucket[count++] = item;

                        // The bucket is fully buffered before it's yielded
                        if (count != size)
                            continue;

                        yield return resultSelector(bucket);

                        bucket = null;
                        count = 0;
                    }

                    // Return the last bucket with all remaining elements
                    if (count > 0)
                    {
                        Array.Resize(ref bucket, count);
                        yield return resultSelector(bucket);
                    }

                    break;
                }
            }
        }
        
        
    }
}