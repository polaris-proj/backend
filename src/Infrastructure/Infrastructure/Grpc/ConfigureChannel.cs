﻿using System;
using Grpc.Core;
using Grpc.Net.Client;
using Grpc.Net.Client.Balancer;
using Grpc.Net.Client.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Infrastructure.Grpc;

public static class ConfigureChannelDependencyInjection
{
    public static IServiceCollection ConfigureGrpcChannel<TClientChannel, TGrpcServiceParameters>(this IServiceCollection services, IWebHostEnvironment env)
    where TClientChannel: class, INamedGrpcChannelWrapper, new()
    where TGrpcServiceParameters: IGrpcServiceParameters
    {
        services.AddSingleton<ResolverFactory>(_ => new DnsResolverFactory(TimeSpan.FromSeconds(30)));
        services.AddSingleton<TClientChannel>(x =>
        {
            var address = x.GetRequiredService<TGrpcServiceParameters>().Address;
            GrpcChannel channel;
            if (env.IsProduction())
            {
                var methodConfig = new MethodConfig
                {
                    Names = { MethodName.Default },
                    RetryPolicy = new RetryPolicy
                    {
                        MaxAttempts = 5,
                        InitialBackoff = TimeSpan.FromMilliseconds(100),
                        MaxBackoff = TimeSpan.FromSeconds(5),
                        BackoffMultiplier = 1.5,
                        RetryableStatusCodes = {StatusCode.Unavailable, StatusCode.DeadlineExceeded,
                                                StatusCode.DataLoss, StatusCode.Internal}
                    }
                };
                
                channel = GrpcChannel.ForAddress(
                    address, //dns address
                    new GrpcChannelOptions
                    {
                        Credentials = ChannelCredentials.Insecure,
                        ServiceConfig = new ServiceConfig
                        {
                            LoadBalancingConfigs = {new RoundRobinConfig()},
                            MethodConfigs = {  methodConfig}
                        },
                        MaxReceiveMessageSize = null,
                    });
            }
            else
            {
                channel = GrpcChannel.ForAddress(address, new GrpcChannelOptions
                {
                    MaxReceiveMessageSize = null,
                    MaxSendMessageSize = null,
                });
            }

            return new TClientChannel {Channel = channel};
        });
        return services;
    }
}