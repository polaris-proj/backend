﻿using System.Runtime.Serialization;

namespace Infrastructure.Grpc;

[DataContract]
public class Answer<T>
{
    public Answer(T? value)
    {
        Value = value;
    }
    
    [DataMember(Order = 1)] public T? Value { get; set; }

    private Answer() { }//for protobuf don't delete
}