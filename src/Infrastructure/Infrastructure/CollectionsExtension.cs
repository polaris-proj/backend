﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure;

public static class CollectionsExtension
{
    public static async Task ProcessInParallel<T>(this IEnumerable<T> items, Func<T, Task> taskFactory,
        int maxDegreeOfParallelism)
    {
        var tasks = new List<Task>();
        using var semaphore = new SemaphoreSlim(maxDegreeOfParallelism);

        foreach (var item in items)
        {
            await semaphore.WaitAsync();

            var task = Task.Run(async () =>
            {
                try
                {
                    await taskFactory(item);
                }
                finally
                {
                    semaphore.Release();
                }
            });

            tasks.Add(task);
        }

        await Task.WhenAll(tasks);
    }
}