﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Infrastructure.DAL.Dbo;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.DAL.Storage;

public abstract class RepositoryWithGuid<T, TDbContext> : IRepository<T, Guid>
    where T : class, IEntity<Guid>
    where TDbContext : DbContext
{
    protected readonly TDbContext Context;

    protected RepositoryWithGuid(TDbContext context)
    {
        Context = context;
    }

    public virtual IEnumerable<T> ReadAll()
    {
        return Context.Set<T>().ToArray();
    }

    public virtual async Task<List<T>> ReadAllAsync()
    {
        return await Context.Set<T>().ToListAsync();
    }

    public virtual T? Read(Guid id)
    {
        return Context.Set<T>().FirstOrDefault(u => u.Id == id);
    }

    public virtual async Task<T?> ReadAsync(Guid id)
    {
        return await Context.Set<T>().FirstOrDefaultAsync(u => u.Id == id);
    }

    public virtual IEnumerable<T> Read(Expression<Func<T, bool>> lambda)
    {
        return Context.Set<T>().Where(lambda).ToArray();
    }

    public virtual async Task<IEnumerable<T>> ReadAsync(Expression<Func<T, bool>> lambda)
    {
        return await Context.Set<T>().Where(lambda).ToArrayAsync();
    }

    public virtual Guid Create(T item)
    {
        Context.Set<T>().Add(item);
        return item.Id;
    }

    public virtual async Task<Guid> CreateAsync(T item)
    {
        await Context.Set<T>().AddAsync(item);
        return item.Id;
    }


    public virtual void Update(T item)
    {
        Context.Set<T>().Update(item).State = EntityState.Modified;
    }

    public virtual Task UpdateAsync(T item)
    {
        Context.Set<T>().Update(item).State = EntityState.Modified;
        return Task.CompletedTask;
    }

    public virtual void Delete(Guid id)
    {
        var user = Read(id);
        if (user != null)
        {
            Context.Set<T>().Remove(user);
        }
    }

    public virtual async Task DeleteAsync(Guid id)
    {
        var user = await ReadAsync(id);
        if (user != null)
        {
            Context.Set<T>().Remove(user);
        }
    }


    public virtual void Save()
    {
        Context.SaveChanges();
    }

    public virtual async Task SaveAsync()
    {
        await Context.SaveChangesAsync();
    }
}