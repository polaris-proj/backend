﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Dapper;

//source https://github.com/Faithlife/DapperUtility/tree/master

namespace Infrastructure.DAL.Dapper;

/// <summary>
/// Methods for bulk insert with Dapper.
/// </summary>
public static partial class BulkInsertUtility
{
    /// <summary>
    /// Efficiently inserts multiple rows, in batches as necessary.
    /// </summary>
    public static int BulkInsert<TInsert>(this IDbConnection connection, string sql,
        IEnumerable<TInsert> insertParams, IDbTransaction? transaction = null, int? batchSize = null)
    {
        return connection.BulkInsert(sql, (object) null!, insertParams, transaction, batchSize);
    }

    /// <summary>
    /// Efficiently inserts multiple rows, in batches as necessary.
    /// </summary>
    public static int BulkInsert<TCommon, TInsert>(this IDbConnection connection, string sql, TCommon commonParam,
        IEnumerable<TInsert> insertParams, IDbTransaction? transaction = null, int? batchSize = null)
    {
        var rowCount = 0;
        foreach (var commandDefinition in GetBulkInsertCommands(sql, commonParam, insertParams, transaction,
                     batchSize))
            rowCount += connection.Execute(commandDefinition);
        return rowCount;
    }

    /// <summary>
    /// Efficiently inserts multiple rows, in batches as necessary.
    /// </summary>
    public static Task<int> BulkInsertAsync<TInsert>(this IDbConnection connection, string sql,
        IEnumerable<TInsert> insertParams, IDbTransaction? transaction = null, int? batchSize = null,
        CancellationToken cancellationToken = default)
    {
        return connection.BulkInsertAsync(sql, (object) null!, insertParams, transaction, batchSize,
            cancellationToken);
    }

    /// <summary>
    /// Efficiently inserts multiple rows, in batches as necessary.
    /// </summary>
    public static async Task<int> BulkInsertAsync<TCommon, TInsert>(this IDbConnection connection, string sql,
        TCommon commonParam, IEnumerable<TInsert> insertParams, IDbTransaction? transaction = null,
        int? batchSize = null, CancellationToken cancellationToken = default)
    {
        var rowCount = 0;
        foreach (var commandDefinition in GetBulkInsertCommands(sql, commonParam, insertParams, transaction,
                     batchSize, cancellationToken))
            rowCount += await connection.ExecuteAsync(commandDefinition);
        return rowCount;
    }

    /// <summary>
    /// Gets the Dapper <c>CommandDefinition</c>s used by <c>BulkInsert</c> and <c>BulkInsertAsync</c>.
    /// </summary>
    public static IEnumerable<CommandDefinition> GetBulkInsertCommands<TInsert>(string sql,
        IEnumerable<TInsert> insertParams, IDbTransaction? transaction = null, int? batchSize = null,
        CancellationToken cancellationToken = default)
    {
        return GetBulkInsertCommands(sql, (object) null!, insertParams, transaction, batchSize, cancellationToken);
    }

    /// <summary>
    /// Gets the Dapper <c>CommandDefinition</c>s used by <c>BulkInsert</c> and <c>BulkInsertAsync</c>.
    /// </summary>
    public static IEnumerable<CommandDefinition> GetBulkInsertCommands<TCommon, TInsert>(string sql,
        TCommon commonParam, IEnumerable<TInsert> insertParams, IDbTransaction? transaction = null,
        int? batchSize = null, CancellationToken cancellationToken = default)
    {
        if (sql == null)
            throw new ArgumentNullException(nameof(sql));
        if (insertParams == null)
            throw new ArgumentNullException(nameof(insertParams));
        if (batchSize < 1)
            throw new ArgumentOutOfRangeException(nameof(batchSize), batchSize, "Batch size must be positive.");

        // find VALUES clause
        var valuesClauseMatches = SValuesClauseRegex.Matches(sql);
        if (valuesClauseMatches.Count == 0)
            throw new ArgumentException("SQL does not contain 'VALUES (' followed by ')...'.", nameof(sql));
        if (valuesClauseMatches.Count > 1)
            throw new ArgumentException("SQL contains more than one 'VALUES (' followed by ')...'.", nameof(sql));

        return YieldBulkInsertCommands(valuesClauseMatches[0], sql, commonParam, insertParams, transaction,
            batchSize, cancellationToken);
    }

    private static IEnumerable<CommandDefinition> YieldBulkInsertCommands<TCommon, TInsert>(Match valuesClauseMatch,
        string sql, TCommon commonParam, IEnumerable<TInsert> insertParams, IDbTransaction? transaction = null,
        int? batchSize = null, CancellationToken cancellationToken = default)
    {
        // identify SQL parts
        var tupleMatch = valuesClauseMatch.Groups[1];
        var sqlPrefixLength = tupleMatch.Index;
        var sqlSuffixIndex = valuesClauseMatch.Index + valuesClauseMatch.Length;
        var tupleSql = tupleMatch.Value;

        // get common names and values
        var commonNames = ParamExtractor<TCommon>.GetNames();
        var commonValues = ParamExtractor<TCommon>.GetValues(commonParam);

        // get insert names and find insert parameters in tuple
        var insertNames = ParamExtractor<TInsert>.GetNames();
        var pastEndTupleSqlIndices = SParameterRegex.Matches(tupleSql)
            .Where(match => insertNames.Any(name =>
                string.Compare(match.Value, 1, name, 0, match.Value.Length, StringComparison.OrdinalIgnoreCase) ==
                0))
            .Select(match => match.Index + match.Length)
            .ToList();

        // calculate batch size (999 is SQLite's maximum and works well with MySql.Data)
        const int maxParamsPerBatch = 999;
        var actualBatchSize = batchSize ??
                              Math.Max(1,
                                  (maxParamsPerBatch - commonNames.Length) / Math.Max(1, insertNames.Length));

        // insert one batch at a time
        string? batchSql = null;
        var lastBatchCount = 0;
        StringBuilder? batchSqlBuilder = null;
        foreach (var insertParamBatch in insertParams.Batch(actualBatchSize).Select(x=>x.ToArray()))
        {
            // build the SQL for the batch
            var batchCount = insertParamBatch.Length;
            if (batchSql == null || batchCount != lastBatchCount)
            {
                if (batchSqlBuilder == null)
                    batchSqlBuilder = new StringBuilder(sqlPrefixLength +
                                                        batchCount * (1 + tupleSql.Length +
                                                                      pastEndTupleSqlIndices.Count * 4) +
                                                        (sql.Length - sqlSuffixIndex));
                else
                    batchSqlBuilder.Clear();

                batchSqlBuilder.Append(sql, 0, sqlPrefixLength);

                for (var rowIndex = 0; rowIndex < batchCount; rowIndex++)
                {
                    if (rowIndex != 0)
                        batchSqlBuilder.Append(',');

                    var tupleSqlIndex = 0;
                    foreach (var pastEndTupleSqlIndex in pastEndTupleSqlIndices)
                    {
                        batchSqlBuilder.Append(tupleSql, tupleSqlIndex, pastEndTupleSqlIndex - tupleSqlIndex);
                        batchSqlBuilder.Append('_');
                        batchSqlBuilder.Append(rowIndex.ToString(CultureInfo.InvariantCulture));
                        tupleSqlIndex = pastEndTupleSqlIndex;
                    }

                    batchSqlBuilder.Append(tupleSql, tupleSqlIndex, tupleSql.Length - tupleSqlIndex);
                }

                batchSqlBuilder.Append(sql, sqlSuffixIndex, sql.Length - sqlSuffixIndex);
                batchSql = batchSqlBuilder.ToString();
                lastBatchCount = batchCount;
            }

            // add the parameters for the batch
            var batchParameters = new DynamicParameters();
            for (int commonIndex = 0; commonIndex < commonNames.Length; commonIndex++)
                batchParameters.Add(commonNames[commonIndex], commonValues[commonIndex]);

            // enumerate rows to insert
            for (int rowIndex = 0; rowIndex < insertParamBatch.Length; rowIndex++)
            {
                var insertParam = insertParamBatch[rowIndex];
                var insertValues = ParamExtractor<TInsert>.GetValues(insertParam);
                for (int insertIndex = 0; insertIndex < insertNames.Length; insertIndex++)
                    batchParameters.Add(
                        insertNames[insertIndex] + "_" + rowIndex.ToString(CultureInfo.InvariantCulture),
                        insertValues[insertIndex]);
            }

            // return command definition
            yield return new CommandDefinition(batchSql, batchParameters, transaction, default,
                default, CommandFlags.Buffered, cancellationToken);
        }
    }

    // cache property names and getters for each type
    private static class ParamExtractor<T>
    {
        public static string[] GetNames() => SNames;

        public static object[] GetValues(T param)
        {
            var values = new object[SGetters.Length];
            if (param != null)
            {
                for (int index = 0; index < values.Length; index++)
                    values[index] = SGetters[index](param);
            }

            return values;
        }

        static ParamExtractor()
        {
            var names = new List<string>();
            var getters = new List<Func<T, object>>();
            foreach (var property in typeof(T).GetProperties(BindingFlags.Instance | BindingFlags.Public))
            {
                var getter = TryCreateGetter(property);
                if (getter != null)
                {
                    names.Add(property.Name);
                    getters.Add(getter);
                }
            }

            SNames = names.ToArray();
            SGetters = getters.ToArray();
        }

        private static Func<T, object>? TryCreateGetter(PropertyInfo property)
        {
            var getMethod = property.GetGetMethod();
            var ownerType = property.DeclaringType;
            if (getMethod == null || ownerType == null)
                return null;

            var dynamicGetMethod = new DynamicMethod(name: $"_Get{property.Name}_",
                returnType: typeof(T), parameterTypes: new[] {typeof(object)}, owner: ownerType);
            var generator = dynamicGetMethod.GetILGenerator();
            generator.DeclareLocal(typeof(object));
            generator.Emit(OpCodes.Ldarg_0);
            generator.Emit(OpCodes.Castclass, ownerType);
            generator.EmitCall(OpCodes.Callvirt, getMethod, null);
            if (!property.PropertyType.GetTypeInfo().IsClass)
                generator.Emit(OpCodes.Box, property.PropertyType);
            generator.Emit(OpCodes.Ret);

            return (Func<T, object>) dynamicGetMethod.CreateDelegate(typeof(Func<T, object>));
        }

        private static readonly string[] SNames;
        private static readonly Func<T, object>[] SGetters;
    }

    static readonly Regex SValuesClauseRegex = MyRegex();

    static readonly Regex SParameterRegex = MyRegex1();

    [GeneratedRegex("\\b[vV][aA][lL][uU][eE][sS]\\s*(\\(.*?\\))\\s*\\.\\.\\.", RegexOptions.Singleline | RegexOptions.RightToLeft | RegexOptions.CultureInvariant)]
    private static partial Regex MyRegex();
    [GeneratedRegex("[@:?]\\w+\\b", RegexOptions.CultureInvariant)]
    private static partial Regex MyRegex1();
}