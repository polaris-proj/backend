﻿namespace Infrastructure.Tests;

[TestFixture]
public class CircularHashSetTests
{
    [Test]
    public void Add_AddsNewElement()
    {
        var capacity = 3;
        var set = new CircularHashSet<int>(capacity);

        var result1 = set.Add(1);
        var result2 = set.Add(2);
        var result3 = set.Add(3);

        result1.Should().BeTrue();
        result2.Should().BeTrue();
        result3.Should().BeTrue();
    }

    [Test]
    public void Add_ReplacesExistingElement()
    {
        var capacity = 3;
        var set = new CircularHashSet<int>(capacity);

        set.Add(1);
        set.Add(2);
        set.Add(3);

        var result = set.Add(4);

        result.Should().BeTrue();
        set.Contains(1).Should().BeFalse();
        set.Contains(2).Should().BeTrue();
        set.Contains(3).Should().BeTrue();
        set.Contains(4).Should().BeTrue();
    }

    [Test]
    public void Add_ThrowsArgumentException_ForInvalidCapacity()
    {
        var act = () =>
        {
            // ReSharper disable once ObjectCreationAsStatement
            new CircularHashSet<int>(-1);
        };

        act.Should().Throw<ArgumentException>().WithMessage("Capacity should be positive");
    }

    [Test]
    public void Contains_ReturnsTrue_ForExistingElement()
    {
        int capacity = 3;
        var set = new CircularHashSet<int>(capacity);

        set.Add(1);
        set.Add(2);
        set.Add(3);

        var result = set.Contains(2);

        result.Should().BeTrue();
    }

    [Test]
    public void Contains_ReturnsFalse_ForNonExistingElement()
    {
        var capacity = 3;
        var set = new CircularHashSet<int>(capacity);

        set.Add(1);
        set.Add(2);
        set.Add(3);

        var result = set.Contains(4);

        result.Should().BeFalse();
    }
}