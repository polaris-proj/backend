﻿namespace Infrastructure.Tests;

[TestFixture]
public class MoreLinqTests
{
    [Test]
    public void RandomElement_ReturnsRandomElement_FromNonEmptyList()
    {
        var list = new List<int> { 1, 2, 3, 4, 5 };
        var randomElement = list.RandomElement();

        Assert.Contains(randomElement, list);
    }

    [Test]
    public void RandomElement_ThrowsException_WhenListIsNull()
    {
        List<int>? list = null;

        Assert.Throws<ArgumentNullException>(() => list!.RandomElement());
    }

    [Test]
    public void RandomElement_ThrowsException_WhenListIsEmpty()
    {
        var list = Array.Empty<int>();

        Assert.Throws<ArgumentOutOfRangeException>(() => list.RandomElement());
    }
        
    [Test]
    public void BatchWithSelector_ReturnsBatchesWithSpecifiedSize()
    {
        var source = Enumerable.Range(1, 10).ToArray();
        var size = 3;

        var expected = new List<int[]>
        {
            new[] {1, 2, 3},
            new[] {4, 5, 6},
            new[] {7, 8, 9},
            new[] {10}
        };

        var actual = source.Batch(size, batch => batch.ToArray()).ToList();

        actual.Should().BeEquivalentTo(expected);
    }

    [Test]
    public void Batch_ReturnsEmptyBatches_WhenSourceIsEmpty()
    {
        var source = Array.Empty<int>();
        var size = 3;

        var expected = Array.Empty<int[]>();

        var actual = source.Batch(size, batch => batch.ToArray()).ToList();

        actual.Should().BeEquivalentTo(expected);
    }

    [Test]
    public void Batch_ReturnsSingleBatch_WhenSizeIsGreaterThanSource()
    {
        int[] source = {1, 2, 3};
        var size = 5;

        var expected = new List<int[]>
        {
            new[] {1, 2, 3}
        };

        var actual = source.Batch(size, batch => batch.ToArray()).ToList();


        actual.Should().BeEquivalentTo(expected);
    }

    [Test]
    public void Batch_ReturnsBatchesWithSpecifiedSize()
    {
        var source = Enumerable.Range(1, 10).ToArray();
        var size = 3;

        var expected = new List<IEnumerable<int>>
        {
            new List<int> {1, 2, 3},
            new List<int> {4, 5, 6},
            new List<int> {7, 8, 9},
            new List<int> {10}
        };

        var actual = source.Batch(size).ToList();

        actual.Should().BeEquivalentTo(expected);
    }

    [Test]
    public async Task Buffer_ReturnsBufferedItems()
    {
        var source = Enumerable.Range(1, 10).ToArray();

        var actual = new List<int>();

        await foreach (var buffer in source.ToAsyncEnumerable().Buffer(3))
        {
            actual.Add(buffer);
        }

        actual.Should().BeEquivalentTo(source);
    }
}

public static class EnumerableExtensions
{
    public static async IAsyncEnumerable<T> ToAsyncEnumerable<T>(this IEnumerable<T> source)
    {
        foreach (var item in source)
        {
            await Task.Yield();
            yield return item;
        }
    }
}