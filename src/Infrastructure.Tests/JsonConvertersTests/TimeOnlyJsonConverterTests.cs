﻿using System.Text;
using System.Text.Json;
using Infrastructure.Json;

namespace Infrastructure.Tests.JsonConvertersTests
{
    [TestFixture]
    public class TimeOnlyJsonConverterTests
    {
        [Test]
        public void Read_ConvertsJsonStringToTimeOnly()
        {
            var converter = new TimeOnlyJsonConverter();
            var reader = new Utf8JsonReader("\"12:34:56\""u8);

            reader.Read(); // Move to the Value token
            var result = converter.Read(ref reader, typeof(TimeOnly), new JsonSerializerOptions());

            result.Should().Be(new TimeOnly(12, 34, 56));
        }

        [Test]
        public void Write_ConvertsTimeOnlyToJsonString()
        {
            var converter = new TimeOnlyJsonConverter();
            var memWriter = new MemoryStream();
            var writer = new Utf8JsonWriter(memWriter);

            converter.Write(writer, new TimeOnly(23, 45, 12, 500), new JsonSerializerOptions());
            writer.Flush();

            var json = Encoding.UTF8.GetString(memWriter.ToArray());
            json.Should().Be("\"23:45:12.500\"");
        }

        [Test]
        public void Roundtrip_ConvertsTimeOnlyToJsonStringAndBack()
        {
            var converter = new TimeOnlyJsonConverter();
            var options = new JsonSerializerOptions();
            options.Converters.Add(converter);

            var time = new TimeOnly(12, 34, 56, 789);

            var json = JsonSerializer.Serialize(time, options);
            var result = JsonSerializer.Deserialize<TimeOnly>(json, options);

            result.Should().Be(time);
        }
    }
}