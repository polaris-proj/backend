﻿using Infrastructure.DIExtension;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.Tests;

[TestFixture]
public class DiExtensionsTests
{

    [Test]
    public void AddFactory_AddServiceAndImplementation_RegisterServicesSuccessfully()
    {
        // Arrange
        var services = new ServiceCollection();

        // Act
        services.AddFactory<IService, ServiceImplementation>();
        var container = services.BuildServiceProvider();

        // Assert
        container.GetServices<IService>().Should().NotBeEmpty();
        container.GetServices<Func<IService>>().Should().NotBeEmpty();
        container.GetService<IFactory<IService>>().Should().NotBeNull();
    }

    [Test]
    public void AddFactory_FactoryUsage_ResolvesInstances()
    {
        // Arrange
        var services = new ServiceCollection();
        services.AddFactory<IService, ServiceImplementation>();
        var container = services.BuildServiceProvider();
        var factory = container.GetRequiredService<IFactory<IService>>();

        // Act
        var instance1 = factory.Create();
        var instance2 = factory.Create();

        // Assert
        instance1.Should().BeOfType<ServiceImplementation>();
        instance2.Should().BeOfType<ServiceImplementation>();
        instance1.Should().NotBeSameAs(instance2);
    }

    private interface IService;

    private class ServiceImplementation : IService;
}